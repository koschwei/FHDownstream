# Instructions and notes for running the RunII2017 Analysis

## 1 Running the nTupler

### 1.1 Running datasets on crab

All scripst and configuration are in `$CMSSW_BASE/src/TTH/MEAnalysis/crab_nano`. The dataset are defined multiple `.json` files. For this analysis all files with the `CMSSW_9X` prefix are important.      
The crab submission is done with `multicrab_94X.py`. The scirpt required to flag to be set in the command line options:
- `--tag` : This will be the name used for the folder in crab_projects, as identifier in the task name and subfolder on the SE
- `--workflow` : In the script there are different workflows hardcoded (setting the FW confiuration and the run datasets). The important ones for the FH analyisis are `hadronic` and `hadronic_nome`

For bulk interaction with the crab jobs the `FHDownstream` repository provides the srcipt `crabTools.py` which is linked to the `crab_nano` folder in the setup process.

## 2 Cross sections
For some samples no theory prediction is available. The get the cross sections for such samples. Either consult the [XSDB](https://cms-gen-dev.cern.ch/xsdb/) or run the calculation yourself using the [GenXSecAnalyzer](https://twiki.cern.ch/twiki/bin/viewauth/CMS/HowToGenXSecAnalyzer#Automated_scripts_to_compute_the)

One examples (on lxplus) would be

```bash
cern-get-sso-cookie -u https://cms-pdmv-dev.cern.ch/mcm/ -o ~/private/dev-cookie.txt --krb --reprocess
cern-get-sso-cookie -u https://cms-pdmv.cern.ch/mcm/ -o ~/private/prod-cookie.txt --krb --reprocess
source /afs/cern.ch/cms/PPD/PdmV/tools/McM/getCookie.sh

voms-proxy-init -voms cms

cmsrel CMSSW_9_4_9
cd CMSSW_9_4_9/src
cmsenv
scram b -j8
cd ../../

# download the genproduction reposotory
git clone https://github.com/cms-sw/genproductions.git
cd genproductions/test/calculateXSectionAndFilterEfficiency

./calculateXSectionAndFilterEfficiency.sh -f datasets.txt -c RunIIFall17 -d MINIAODSIM -n 1000000
```

The `datasets.txt` file contains a list of DAS request names that should be run.

## 3 Procuding the skims
Use the `gc/confs/projectSkimFH.conf`. Might be necessary to update `gc/scripts/projectSkimFH.py` (if variables changed) or `gc/executables/projectSkimFH.sh` (if groups have to be disabled/enabled).

The GC jobs are configured to write the Skim to the T3SE. In order to merge the job output files, please use `utils/haddSE.py`:

| option        | description                                                                                                                                                               |
| ------        | -----------                                                                                                                                                               |
| `--type`      | Can be *Single* or *Multi*. Use *Multi* if `--dir` is pointed to full GC output folder (`path/to/se/GCXXXXXX`). Use *Single* if pointed to a single dataset output folder |
| `--dir`       | Point to GC output folder on SE                                                                                                                                           |
| `--altOutput` | Point to desired output folder on SE. Without this the output is written to the GC output folder that also containes the folders for the datasets                         |
| `--skip`      | Define multiple substring (separated by whitespace)  that will be used to exclude dataset                                                                                 |


Furthermore there is the `--split` option which will cause total hadd to be split in multiple steps (defined by `--splitting`). This can be necessary if the dataset contains so many files that the command will be longer than the `subprocess.Popen()` can handle
Also keep in mind that it is not possible to produce a root file with hadd that is larger than 100GB. There is currently no solution to this implemented. It issuggested to skim more variable to get an overall smaller output.

To create the final data skim (*JetHT* and *BTagCSV* skims are set up in a way so there is no double counting if the trigger selection in specified) use
```bash
cd /scratch/$USER
export PATHTOSKIM=/pnfs/psi.ch/cms/trivcat/store/user/path/to/skims #Set this according to your output
hadd data.root root://t3dcachedb.psi.ch/$PATHTOSKIM/JetHT.root root://t3dcachedb.psi.ch/$PATHTOSKIM/BTagCSV.root
xrdcp data.root root://t3dcachedb.psi.ch/$PATHTOSKIM/data.root 
```

**IMPORTANT**: In the current version new b-tagging scale factors and the corresponding normalization will be added to each skim. This is configured in `FHDownstream/utils/addbTagWeights.py`. If this is not desired in can be commented out in the shell script (in `FHDownstream/gc/scripts/`) corresponding to the skim you are running.

## 4 Corrections and reweighting
This sections details all necessary steps for calculating corrections and reweightings from the finished nTuples. Remember to update all GC configs to the correct set of dataset filess

### 4.1 b-tagging normalizations
In order to make sure the b-tag reweighting does not chnage the normaization a nJets dependent noramlization factor is derived (before anaylsis cuts are apply). All jets with `pT > 30 GeV` and an `abs(eta) < 2.4` are considered.

The systeamtics and the input .csv is currently hardcoded in `gc/scripts/getWeightNormalizations.py` and `gc/scripts/classes.h`. Update and change if needed.

```bash
cd gc/
./go.py -c confs/getWeightNormalizations.conf
#Hadd all output files. Use utiles/haddAllFiles.sh -> copy to gc output directors and run 
#cd back to utils/
python calcbTagNorm.py --input /path/to/GC/output/ --folder some/folder/name/ #Input is defined in gc-config, folder will be created 
```

### 4.2 nGen in current dataset
Use the GC script `gc/conf/countFH.conf`. The gen cound will be generated considering `genWeight` and `puWeight` 

```bash
cd gc/
./go.py -c confs/countFH.conf
#Hadd all output files. Use utiles/haddAllFiles.sh -> copy to gc output directors and run 
#cd back to utils/
python getReCalcCounts.py /path/to/GC/output/
```

This will print a Markdown table in the terminal

### 4.3 Trigger Scale factor
For the trigger scale factor nTuples with a SL selection are required. The MEAnalysis config for this is `MEAnalysis/python/cfg_FH_val.py`.

```bash
cd gc/
./go.py -c confs/MEAnalysis_TriggerSF.conf # Or MEAnalysis_SLValidation.conf if you also want the minor backgrounds
#What for GC to finish
# cd to utils
python getFilesCrab.py --outpath . --path /pnfs/path/to/output --onlytthbb # --onlytthbb will disable the counting done on the nanoAOD files
# cd back to gc. We need skims of those files
# update the dataset in confs/projectSkimTriggerSF.conf
./go.py -c confs/projectSkimTriggerSF.conf
#wait... go to utils once finished
python haddSE.py .....
```

#### 4.3.1 Getting the SF
The scripts required for the Trigger SF are located in `FHDownstream/Plotting`.

Use the `Trigger_SFs.py` scripts. Update the nGen numbers and dataset paths in the `TriggerSF()` function. Set a nice name with the `--tag` flag.

1. Run the script with w/o any other flags to get efficiency plots with offline selection
2. Run with `--looseOfflinecuts` to get the SF with a looser selection. This will basically add one additional bin below the usual offline cut. The SF form this step will be used for the actual SF calculation in further steps.
3. Update the path to the SF file in `classes.h` and run with `--closure` to get closure check plots with the new SF

With the `plotSFslices.py` you can plot 2D histograms (x: HT, y: 6th pt) for each bin in CSV of the 3D SF. Update the `SFFile` variable and set `afterEdit = False` (if not updated. see next paragraph).

If the errors are weird or there is some value that is obviouly wrong due to statistics (usually this happens in the bins with large nBCSV --> use output of `plotSFslices.py` to checkt this!) it cna be editied with the `editSFFile.py` script. It is an interactiv scripts. Just follow what it asks :D

**Copy the stable version to `MEAnalysis/data/` and update the name in `MEAnalysis/python/MEAnalysis_cfg_heppy.py`.**

### 4.4 Update the Helper functions
Before going further `FFHDownstream/Plotting/Helper.py` has to be update with nGen, xsec, path etc.

Use the scripts `HTReweighting/getQCDNormalization.py` to get the scalefactor to fix the normalization change cause by the *qglWeight*

### 4.5 CR Correction
Cacluation of the CR correction factor for the QCD estimate. Check definition of low b-tag region! Check sample dependent values. **xsec and ngen** set in script!

Run `get_function_withDR.py` in `Plotting/CRcorrections` for data and MC by changing the `tag` variable to contain `data` or `TT`respectively. Also run once with `redoEta` for both to get **up** systematic of the CR correction (needed for sparsinator).

If both are run, create `btagCorrections.h` in `Plotting/CRcorrections` and copy the correction functions from the output folder into that file with names `btagCorrMC`, `btagCorrData`.

For the sideband validation an additional correction has be calculated. This is done by adding `bML` to the tag. This will chnage the btag High and Low values so that the corrected jets are not between loose and medium but between loose and mediumloose. This new cut is estimated from skims by splitting the number of jets between loose and medium info half. The correction from this is used in the QCD_estimate.py script.

### 4.6 HT reweighting
This is necessary because we observe larger differences in the HT disctribution after estimating the ddQCD than for other control variables. To get the reweighting run `HTReweighting/QCD_estimate.py` w/o the reweighting (by setting `HTreweight = False`, which is also the standard way to run this script) in the VR with the `6jHT` variable for 

1. `jets = ["ge7j"]`,`bjets = ["3b"]` (for the 3b weight) and 
2. `jets = ["ge7j"]`,`bjets = ["4b"]`(for the 4b weight).

Update the files in `HTReweighting/get_ddQCD_HTweights.py` and run the script. This will printout the weights in a c++ like form (`forCpp = True`) or a python like form (`forCpp = False`).

Update

```cpp
float HTWeight(float HT, int threeb){
  if(HT<500.0)......
}
```

Run `HTReweighting/QCD_estimate.py` again with `HTreweight = True` for closure plots. Also make MEM and HT6 plots with enabled `HTreweight = True` (with pulls and Data/MC) for validation.

### 4.7 Reweighting for PDF, muF and muR
The scalefactors were provided by KIT since they are not dependent on the framework. If we need to calculate them for MEAnalysis we need to add calculation of the final up and down PDF weight to nanoAOD or run the sparsinator on samples with passAll (without MEM!).

These weights are only needed for 

- ttHbb
- ttHnonbb
- TTTo*

## 5 Validation
### 5.1 QCD estimation
The script for producing the ddQCD valiadtion plots from skims is `HTReweighting/QCD_estimate.py`. Set the categories and variables in the script. Also make sure that 

- all necessary weights (TriggerWeight, CRCorrection) are set in c macros,
- QGL weight fix is set properly in the `Helper.py`
- Category definitions are correct

Use 

```
regions = ["SR","CR","VR","CR2"]
validation =True
unblind = False
```

### 5.2 Sideband QCD Estimation
For the sideband validation CR2+CR (xRegions) and CR+VR (yRegions) are split in a similar way to the general strategya

####  5.2.1 yRegions
Set `regions = ["SRy","CRy","VRy","CR2y"]` (the categories are defined in `FHDownstream/Plotting/Helper.py`) in `HTReweighting/QCD_estimate.py` and with desired variable and category definition.
Run with:

- `validation =True` and `unblind = True` for VRy
- `validation =False` and `unblind = True` for SRy

#### 5.2.2 xRegions
**Make sure the functions for the sideband correction (as described in 4.5) are set properly.**

Set `regions = ["SRx","CRx","VRx","CR2x"]` (the categories are defined in `FHDownstream/Plotting/Helper.py`) in `HTReweighting/QCD_estimate.py` and with desired variable and category definition.
Run with:

- `validation =True` and `unblind = True` for VRx
- `validation =False` and `unblind = True` for SRx

### 5.2.3 HTReweighting in SR
We also produce a SR validation plot (blinded) for the HTReweighting. Run `HTReweighting/QCD_estimate.py` 
with

```
regions = ["SR","CR","VR","CR2"]
validation =False
unblind = False
```

in all categories. Update paths and filenames in `HTReweighting/plotEffHTRewighting.py` and run it.

### 5.3 QCD estimation for VR fitDiagnostics

Run as normal datacards (see next section) but with `validation = true` in `Add_ddQCDv2.py` config. Use `LaunchAll_FH_VR.py` to produce datacards. Plots can be obtained with `Plotting/FitDiagnosticsValidation.py`.

## 6 Sparsinator and getting the final files for making datacards
### 6.1 Code preparation
Make sure the setting in the sparsinator code are correct.
Check:

- that bTag Normalization weights are correct for current version of DeepCSV SF
- the CRCorrection functions `btagCorrData`, `btagCorrMC`, `btagCorrDataUp` and `btagCorrMCUp` are set according to the results from 4.5
- WP (loose and medium) are correct in `get_CRCorr`
- if new weights for the QCD MC nJets reweighting in `QCDReweighting` are needed (only necessary for preselection and atm 2017 MC)
- Factors in `getNormFactor` for PDF, muF and muR weights
- HTReweighing factors are according to new estimation in 4.6
- if spltting of FH trigger by sample still correct in `pass_HLT_fh` (in 2018 they should be in the same dataset again)
- if all necessary weights are defined weight lists for data (only HTReweighting and CRCorrection in CR and CR2) and mc

### 6.2 Prepping the configs
This is the first time most of the config actually matters. Make sure that the **dataset paths**, **xsec** and **ngen** are set correctly. Also it is important to put all processes you want to run in the `base_processes_fh` list. Otherwise the output files will be empty even if you run the sparsinator over the files. 

For the FH anylsis some options are set in `MEAnalysis/data/config_FH.cfg` (it overwrites the `default.cfg`). Check in both if you are unsur
e.
There are also some options directly effecting the sparsinor in the `[sparsinator]` section. Important for FH are:

- `do_hadronic` : Has to be `true`. Otherwise the "general" EventModel will be used.
- For using the most up-to-date b-tag SF: `reCalcBTagSF`, `reCalcBTagSFType`, `reCalcBTagSFFile`
	-  Currently it is not set up to read the new SF even if they are rerun in MEAnalysis. The EventModel only looks at the nanoAOD ones which are not setup to deal with the special SF of ttH
-  `doCRCorr` : This will completely disable the CRCorrtions functionality. Only really needed for Preselection since it is be construction only applied in CR and CR2
-  `useJESbTagWeights` : If `true` the JES variied b-tag SF will be used in the corresponding shifted distributon instead of the nominal
-  `addJESbTagTemples` : If `true` the JES variied b-tag SF will be saved as templates. Only really needed for debugging or validation

For the VR, CR and CR2 region separate configs are present in the `MEANalysis/data` folder. They overwrite settings in the main `MEAnalysis/data/config_FH.cfg` config. Basically they only change the category selection.

There is also a config for the Preselection region `MEAnalysis/data/config_FH_preSel.cfg` which works essentially the same.

At this point also the systemtics that are considered are set. Check the `[systematics]` section in `default.cfg`

### 6.3 Running the sparsinator
The sparsinator need to run over all samples for all four analysis regions. Use the GC configs `sparse_SR.conf`, `sparse_CR.conf` , `sparse_CR2.conf` and `sparse_VR.conf`.
Depending on the batch system and the filesize etc. All jobs should run on the `all.q`. Maybe some tuning is required depending on your lumisplitting on crab

The outputfiles have to be hadded for each samples (as usual with `haddAllFiles.sh`) and then merged into four files. One for each region: `merged_SR.root`, `merged_CR.root`, `merged_VR.root` and `merged_CR2.root`

### 6.4 Postprocessing the templates files
#### 6.4.1 Fixing the normalization with qglWeight
Use the script `FHDownstream/Datacards/QGLNorm.py`. Update the `folder` and the `fileName*` variable. This will create a new files (with appended `_new`) with the QGL normalization

#### 6.4.2 Getting the rate parameter for HDAMP and UE
For this you need the sparsinator output of the HDAMP and UE variied samples. Use the script `FHDownstream/Datacards/getPSrates.py`. Updates files and run once for HDAMP and once for UE
	
#### 6.4.3 Adding the ddQCD templates and rate templates 
Additional the ddQCD histograms have the be added to the SR and VR files. This is done, using the script `FHDownstream/Datacards/Add_ddQCDv2.py`. The script is configured by a config file. Use one of the `.cfg` present in the directory and save you chnages under a new name (for bookkeeping). In the config, the inputfiles (use the `*_new.root` files from step 6.4.1) and the rate systamtics + values are set (there are some other options).

Run the script by passing the config with the `--config` argument.

This will produce a output file (same directory as imput) with an appended `_ddQCD_added` to the name. This file will contain the ddQCD templates as well as the templates for the rate variations in the SR.

Hadd the `_new` file and the `_ddQCD_added` file to get the final template file required for the datacard builder.

### 6.5 Making datacards
For historical reasons the scripts for producing the datacards are located in `TTH/Plotting/python/Datacards`. 

Use the script `Launchall_FH.py`. Update the inputfile (the file you have at the end of 6.4.3) and set the variables on top of the files according to you likeing.

Run the script..... Now you have datacards :D

## 7 Final plots, yields, etc.
### 7.1 Pulls
Run `diffNuisances.py` from Higgs Combine repo (Setup Combine CMSSW version. Located in `$CMSSW_BASE/src/HiggsAnalysis/CombinedLimit/test/diffNuisances.py`) and pipe output into a file.

Run in Analysis CMSSW version: 
`python $CMSSW_BASE/src/TTH/FHDownstream/Datacards/plotDiffNuisances.py --inSplusB diffNuisance.txt --inBOnly diffNuisance.txt --output pulls`. This will produce horizontal pull plots split into three groups

### 7.2 Discriminator plots
All discriminator plots were created with the common [datacard/limit framework](https://gitlab.cern.ch/ttH/datacards/tree/master) of the ttHbb groups. The plot script is `datacards/PreFitPostFitPlots/PrePostFitPlots.py`.

