import ROOT
#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.INFO,
)
#############################################################
#############################################################


def doFile(fileName):
    rFile = ROOT.TFile.Open(fileName)

    histos2Save = []
    
    for key in rFile.GetListOfKeys():
        if key.GetName().endswith("_off"):
            logging.info("Adding %s", key.GetName()+"Up")
            histo = rFile.Get(key.GetName()).Clone(key.GetName()+"Up")
            histo.SetTitle(key.GetName()+"Up")
            histos2Save.append(histo)

    
    filePath = rFile.GetName()
    newFile = ROOT.TFile(filePath.replace(".root","_offRename.root"),"recreate")
    newFile.cd()
    for h in histos2Save:
        h.Write()
    newFile.Close()
    rFile.Close()

    logging.info("Done")
    ROOT.gROOT.GetListOfFiles().Remove(newFile)
    ROOT.gROOT.GetListOfFiles().Remove(rFile)
    
if __name__ == "__main__":
    inFile_SR = "/t3home/koschwei/scratch/ttH/sparse/sparsev5_final_v2/merged_SR_new.root"
    inFile_CR = "/t3home/koschwei/scratch/ttH/sparse/sparsev5_final_v2/merged_CR_new.root"
    doFile(inFile_SR)
    doFile(inFile_CR)
