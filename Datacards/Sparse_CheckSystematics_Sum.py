import sys, os
sys.path.insert(0, os.path.abspath(os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/classes/"))
from PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos
from ratios import RatioPlot
import ROOT
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)
#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.DEBUG,
)
#############################################################
#############################################################
baseDir = "/t3home/koschwei/scratch/ttH/sparse/sparsev5_final_v2"
plot4process = ["ddQCD","ttbarPlusBBbar","ttbarOther","ttbarPlusCCbar","ttbarPlusB","ttbarPlus2B","singlet","wjets","zjets","ttbarW","ttbarZ","diboson"]
plot4process = ["ddQCD","ttbarPlusBBbar","ttbarOther","ttbarPlusCCbar","ttbarPlusB","ttbarPlus2B","singlet","wjets","zjets","ttbarW","ttbarZ","diboson","ttH_hcc","ttH_htt","ttH_hgluglu","ttH_hgg","ttH_hww","ttH_hzz","ttH_hzg"]
preSel = False
tag = baseDir.split("/")[-1]
identifier = "newBSF"
if not preSel:
    inputFile = {
        "fh_j9_t4" : baseDir+"/merged_SR_ddQCD.root",
        "fh_j8_t4" : baseDir+"/merged_SR_ddQCD.root",
        "fh_j7_t4" : baseDir+"/merged_SR_ddQCD.root",
        "fh_j9_t3" : baseDir+"/merged_SR_ddQCD.root",
        "fh_j8_t3" : baseDir+"/merged_SR_ddQCD.root",
        "fh_j7_t3" : baseDir+"/merged_SR_ddQCD.root"
    }
    plot4cat = ["fh_j9_t4","fh_j8_t4","fh_j7_t4", "fh_j9_t3","fh_j8_t3","fh_j7_t3"]
    #plot4cat = ["fh_j7_t3"]
    plot4disc = {
        "fh_j9_t4" : "mem_FH_4w2h2t_p",
        "fh_j8_t4" : "mem_FH_3w2h2t_p",
        "fh_j7_t4" : "mem_FH_3w2h2t_p",
        "fh_j9_t3" : "mem_FH_4w2h1t_p",
        "fh_j8_t3" : "mem_FH_4w2h1t_p",
        "fh_j7_t3" : "mem_FH_4w2h1t_p"
    }
else:
    inputFile = {
        "fh_presel" : baseDir+"/merged_PreSel_new_QCDScaled.root",
    }
    plot4cat = ["fh_presel"]
    plot4disc = {
        "fh_presel" : "numJets",
    }

logging.info("Getting files")
rFiles = {}
for cat in plot4cat:
    rFiles[cat] = ROOT.TFile.Open(inputFile[cat])

    

logging.info("Getting systematics")
systematics = []
for proc in plot4process:
    logging.info("Processing: %s", proc)
    for cat in plot4cat:
        rFile = rFiles[cat]
        allHistos = []
        for key in rFile.GetListOfKeys():
            allHistos.append(key.GetName())
        allHistos = set(allHistos)
        #logging.info("Processing: %s", cat)
        allCanvases = []
        hnames = []
        for key in rFile.GetListOfKeys():
            keyName = key.GetName()
            if proc in keyName and cat in keyName:
                hnames.append(keyName)
        for hname in hnames:
            #only look at systeamtics
            if len(hname.split("__")) == 4:
                systName = hname.split("__")[-1]
                if systName.endswith("Down"):
                    systematics.append(systName[:-len("Down")])
                if systName.endswith("Up"):
                    systematics.append(systName[:-len("Up")])
systematics = set(systematics)

logging.info("Found systematics: %s", systematics)

for cat in plot4cat:
    logging.info("Processing: %s", cat)
    rFile = rFiles[cat]
    hnames = []
    for key in rFile.GetListOfKeys():
        keyName = key.GetName()
        if proc in keyName and cat in keyName:
            hnames.append(keyName)
    allHistos = []
    for key in rFile.GetListOfKeys():
        allHistos.append(key.GetName())
    allHistos = set(allHistos)
    allCanvases = []    
    for systematic in systematics:
        logging.info("Processing: %s", systematic)
        for iproc, proc in enumerate(plot4process):
            

            
            upSys = "{0}__{1}__{2}__{3}Up".format(proc, cat, plot4disc[cat], systematic)
            downSys = "{0}__{1}__{2}__{3}Down".format(proc, cat, plot4disc[cat], systematic)
            nom = "{0}__{1}__{2}".format(proc, cat, plot4disc[cat])
            if upSys not in allHistos:
                logging.warning("Did not fine Up systematic for %s", systematic)
                logging.debug("Query for: %s",upSys)
                logging.info("Replacing with nominal: %s", nom)
                upSys = nom
            if downSys not in allHistos:
                logging.warning("Did not fine Down systematic for %s", systematic)
                logging.debug("Query for: %s",downSys)
                logging.info("Replacing with nominal: %s", nom)
                downSys = nom

            if iproc == 0:
                hSum = rFile.Get(nom).Clone("sum_nom_{0}_{1}_{2}".format(proc, cat, systematic))
                hSum_Up = rFile.Get(upSys).Clone("sum_up_{0}_{1}_{2}".format(proc, cat, systematic))
                hSum_Down = rFile.Get(downSys).Clone("sum_down_{0}_{1}_{2}".format(proc, cat, systematic))
            else:
                hSum.Add(rFile.Get(nom))
                hSum_Up.Add(rFile.Get(upSys))
                hSum_Down.Add(rFile.Get(downSys))

        histoList = [
            hSum,
            hSum_Up,
            hSum_Down
        ]
        
        histoList[0].SetLineColor(ROOT.kBlack)
        histoList[1].SetLineColor(ROOT.kRed)
        histoList[2].SetLineColor(ROOT.kBlue)
        thisRatio = RatioPlot(systematic)
        thisRatio.ratioRange = (0.994, 1.016)
        thisRatio.legendSize = (0.3,0.5,0.7,0.95)
        thisRatio.passHistos(histoList)
        allCanvases.append(
            thisRatio.drawPlot(
                ["Nominal", "{0}Up".format(systematic), "{0}Down".format(systematic)],
                xTitle =  plot4disc[cat],
                noErr = True
            )
        )
        # allCanvases.append(makeCanvasOfHistos(systematic, plot4disc[cat], histoList,
        #                                       legendText = ["Nominal", "{0}Up".format(systematic), "{0}Down".format(systematic)],
        #                                       colorList = [ROOT.kBlack, ROOT.kBlue, ROOT.kRed], lineWidth = 2))
    saveCanvasListAsPDF(allCanvases, "sparseSysComp_{0}_{1}_{2}_{3}".format(tag, identifier, "bgSumWSignal", cat), ".")



        
for cat in plot4cat:
    ROOT.gROOT.GetListOfFiles().Remove(rFiles[cat]);
