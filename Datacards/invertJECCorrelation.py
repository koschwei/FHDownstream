import ROOT
import sys
from copy import deepcopy

inFile = sys.argv[1]
outfile = sys.argv[2]

rFile = ROOT.TFile.Open(inFile)
keys = rFile.GetListOfKeys()

outHistos = []


rOutFile = ROOT.TFile(outfile, "RECREATE")
rOutFile.cd()
for ikey, key in enumerate(keys):
    histo = key.ReadObj()
    key = key.GetName()
    if ikey % 1000 == 0:
        print "{0}/{1}".format(ikey, len(keys))
    if "CMS_scale" in key:
        if key.endswith("2017Up") or key.endswith("2017Down"):
            histo.SetTitle(key.replace("_2017",""))
            histo.SetName(key.replace("_2017",""))
        elif not "2017" in key:
            if key.endswith("Up"):
                histo.SetTitle(key.replace("Up","_2017Up"))
                histo.SetName(key.replace("Up","_2017Up"))
            elif key.endswith("Down"):
                histo.SetTitle(key.replace("Down","_2017Down"))
                histo.SetName(key.replace("Down","_2017Down"))
                print "Adding _2017 - {0} {1}".format(key, histo)
            else:
                print "There is a scale unc that does not end on up or down"
                exit()
        else:
            print "There is a scale systematic that does not contain 2017 and does not end with 2017Up or 2017Down"
            exit()
    #print "Writing {0} as {1}".format(key, histo)
    histo.Write()

print "INput keys {0}".format(len(keys))
print "Output keys {0}".format(len(rOutFile.GetListOfKeys()))
        
rOutFile.Close()
