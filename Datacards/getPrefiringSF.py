import ROOT

onlyUseUp = True

addSignalProcs = [
    "ttH_hcc",
    "ttH_htt",
    "ttH_hgluglu",
    "ttH_hgg",
    "ttH_hww",
    "ttH_hzz",
    "ttH_hzg",
]
addBkgProcs = [
    "singlet",
    "wjets",
    "zjets",
    "ttbarW",
    "ttbarZ",
    "diboson",

]
files = {
    "SR" : "/t3home/koschwei/scratch/ttH/sparse/sparsev6/Prefiring/merged_SR.root",
    "VR" : "/t3home/koschwei/scratch/ttH/sparse/sparsev6/Prefiring/merged_VR.root",
    "CR" : "/t3home/koschwei/scratch/ttH/sparse/sparsev6/Prefiring/merged_CR.root",
    "CR2" : "/t3home/koschwei/scratch/ttH/sparse/sparsev6/Prefiring/merged_CR2.root"
}

variable = "ht30"

processes = {
    "ttH_hbb" : ["ttH_hbb"],
    "ttbar" : ["ttbarPlusBBbar","ttbarPlusB","ttbarPlus2B","ttbarPlusCCbar","ttbarOther"],
    }

cats = [
    "fh_j7_t3",
    "fh_j8_t3",
    "fh_j9_t3",
    "fh_j7_t4",
    "fh_j8_t4",
    "fh_j9_t4",
]

print "Opening files"
rFile = {}
for region in files:
    rFile[region] = ROOT.TFile.Open(files[region])

"""
SF = {}
Var = {}
for region in files:
    print "++++++++++++++++++++++++++++++++++"
    print "++++++++++++++ Getting SF for %s"%region
    SF[region] = {}
    Var[region] = {}
    for cat in cats:
        SF[region][cat] = {}
        Var[region][cat] = {}
        for proc in processes:
            #print "Making SF for %s"%proc
            yieldOld = 0
            yieldNew = 0
            yieldUp = 0
            yieldDown = 0
            for subproc in processes[proc]:
                #print "Running subprocess %s"%subproc
                yieldOld += rFile[region].Get("{0}__{1}__{2}".format(subproc, cat, variable)).Integral()
                yieldNew += rFile[region].Get("{0}__{1}__{2}__{3}".format(subproc, cat, variable, "CMS_ttHbb_L1PreFiring")).Integral()
                yieldUp += rFile[region].Get("{0}__{1}__{2}__{3}".format(subproc, cat, variable, "CMS_ttHbb_L1PreFiringUp")).Integral()
                yieldDown += rFile[region].Get("{0}__{1}__{2}__{3}".format(subproc, cat, variable, "CMS_ttHbb_L1PreFiringDown")).Integral()

            print "==========================================="
            print "============ {0} ============".format(cat)
            print "== {0} ====================".format(proc)
            print " Yield nom = {0}".format(yieldOld)
            print "  Yield L1 = {0}".format(yieldNew)
            print "  Yield Up = {0}".format(yieldUp)
            print "Yield Down = {0}".format(yieldDown)
            print "SF nom = {0}".format(yieldNew/yieldOld)
            print "SF up = {0}".format(yieldUp/yieldNew)
            print "SF down = {0}".format(yieldDown/yieldNew)

            SF[region][cat][proc] = str(yieldNew/yieldOld)
            if onlyUseUp:
                Var[region][cat][proc] = str(yieldUp/yieldNew)
            else:
                 Var[region][cat][proc] = "{0}/{1}".format(yieldUp/yieldNew, yieldDown/yieldNew)
                
            
# General SF
for region in files:
    print "[GeneralSF_{0}]".format(region)
    for cat in cats:
        print "{0}:".format(cat)
        for proc in processes:
            for subproc in processes[proc]:
                print "  {0} {1}".format(subproc, SF[region][cat][proc])
        for proc in addSignalProcs:
            print "  {0} {1}".format(proc, SF[region][cat]["ttH_hbb"])
        for proc in addBkgProcs:
            print "  {0} {1}".format(proc, SF[region][cat]["ttbar"])

for region in files:
    for cat in cats:
        print "[{0}__{1}__{2}]".format("Systematics",cat,region)
        print "{0}:".format("CMS_ttHbb_L1PreFiring")
        for proc in processes:
            for subproc in processes[proc]:
                print "  {0} {1}".format(subproc, Var[region][cat][proc])
        for proc in addSignalProcs:
            print "  {0} {1}".format(proc, Var[region][cat]["ttH_hbb"])
        for proc in addBkgProcs:
            print "  {0} {1}".format(proc, Var[region][cat]["ttbar"])

"""
        
histoNom = {}
histoL1 = {}
histoL1Up = {}
histoL1Down = {}
for iregion, region in enumerate(files):
    for cat in cats:
        if cat not in histoNom.keys():
            histoNom[cat] = {}
        if cat not in histoL1.keys():
            histoL1[cat] = {}
        if cat not in histoL1Up.keys():
            histoL1Up[cat] = {}
        if cat not in histoL1Down.keys():
            histoL1Down[cat] = {}
        for proc in processes:
            for isubproc, subproc in enumerate(processes[proc]):
                if iregion == 0 and isubproc == 0:
                    histoNom[cat][proc] = rFile[region].Get("{0}__{1}__{2}".format(subproc, cat, variable)).Clone("histoNom")
                    histoL1[cat][proc] = rFile[region].Get("{0}__{1}__{2}__{3}".format(subproc, cat, variable, "CMS_ttHbb_L1PreFiring")).Clone("histoL1")
                    histoL1Up[cat][proc] = rFile[region].Get("{0}__{1}__{2}__{3}".format(subproc, cat, variable, "CMS_ttHbb_L1PreFiringUp")).Clone("histoL1Up")
                    histoL1Down[cat][proc] = rFile[region].Get("{0}__{1}__{2}__{3}".format(subproc, cat, variable, "CMS_ttHbb_L1PreFiringDown")).Clone("histoL1Down")
                else:
                    histoNom[cat][proc].Add(rFile[region].Get("{0}__{1}__{2}".format(subproc, cat, variable)))
                    histoL1[cat][proc].Add(rFile[region].Get("{0}__{1}__{2}__{3}".format(subproc, cat, variable, "CMS_ttHbb_L1PreFiring")))
                    histoL1Up[cat][proc].Add(rFile[region].Get("{0}__{1}__{2}__{3}".format(subproc, cat, variable, "CMS_ttHbb_L1PreFiringUp")))
                    histoL1Down[cat][proc].Add(rFile[region].Get("{0}__{1}__{2}__{3}".format(subproc, cat, variable, "CMS_ttHbb_L1PreFiringDown")))


SF = {}
Var = {}
for cat in cats:
    SF[cat] = {}
    Var[cat] = {}
    for proc in processes:
        SF[cat][proc] =  str(histoL1[cat][proc].Integral()/histoNom[cat][proc].Integral())
        if onlyUseUp:
            Var[cat][proc] = str(histoL1Up[cat][proc].Integral()/histoL1[cat][proc].Integral())
        else:
            Var[cat][proc] = "{0}/{1}".format(str(histoL1Up[cat][proc].Integral()/histoL1[cat][proc].Integral()), str(histoL1Down[cat][proc].Integral()/histoL1[cat][proc].Integral()))


print "[GeneralSF]"
for cat in cats:
    print "{0}:".format(cat)
    for proc in processes:
        for subproc in processes[proc]:
            print "  {0} {1}".format(subproc, SF[cat][proc])
    for proc in addSignalProcs:
        print "  {0} {1}".format(proc, SF[cat]["ttH_hbb"])
    for proc in addBkgProcs:
        print "  {0} {1}".format(proc, SF[cat]["ttbar"])


for cat in cats:
    print "[{0}__{1}]".format("Systematics",cat)
    print "{0}:".format("CMS_ttHbb_L1PreFiring")
    for proc in processes:
        for subproc in processes[proc]:
            print "  {0} {1}".format(subproc, Var[cat][proc])
    for proc in addSignalProcs:
        print "  {0} {1}".format(proc, Var[cat]["ttH_hbb"])
    for proc in addBkgProcs:
        print "  {0} {1}".format(proc, Var[cat]["ttbar"])
