# Scripts for processing sparse histograms for datacard purposes 

| Script | Description | Relevant for results |
|--------|-------------|----------|
| Add_ddQCD.py | Old version of script for claculating thw ddQCD templates | no |
| Add_ddQCDv2.py |  NEW version of script for claculating thw ddQCD templates | yes |
| PiePlots.py | FH-only version of the PiePlot script (see `FitDiagnosticsPiePlots.py` in Plotting) | no |
| QGLnorm.py | Script for normalizing all histograms to have the same post qgWeight integral as before | yes |
| Sparse_CheckSystematics.py | Scrpit for plotting the up/down/nominal for each systemtic from a sparse file for a given process | no |
| Sparse_CheckSystematics_Sum.py | Scrpit for plotting the up/down/nominal for each systemtic from a sparse file. This script will produce the total background template | no |
| checkSystematics.py | same as `Sparse_CheckSystematics.py` but from datacard template file | no |
| getPSrates.py | Get Rate uncertainty parameters from sample based uncertainties with large fluctuations (HDAMP, TUNE in 2017)  | yes |
| getPrefiringSF.py | Get Rate uncertainty parameters for L1 Prefiring | yes |
| invertJECCorrelation.py | INversts which JEC systeamtics are correalted between 2016 and 2017 | no |
| mergeHistosSparse.p | Script for merging ttV and VJets from ttZ, ttW, WJets and ZJets in sparse | no |
| plotDiffNuisances.p | PLot the output of combien diffnuisance for AN style | yes |
| printDatacardYield.py | Get yield for a given process | no |
| renameSys.p | Rename the `*off` systematics so they have correct format in sparse | no (ARC Comment) |
| replaceNominal.py | Script foir replacing the nominal with the unweighted systematic (only data) | no |
| scaleQCDToDataYield.py | Script fior scaling the QCD MC to the datayield in preselection sparse files | yes |
