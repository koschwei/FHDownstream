#Script for extracting the ddQCD histograms from rootfiles with final discriminators 
import ROOT
from copy import deepcopy
from time import sleep
import os
import sys
#sys.path.insert(0, os.path.abspath('../Plotting'))
#from classes.PlotHelpers import saveCanvasListAsPDF

debugging = False
blindDebug = False
nSyst = 9999 # For debugging 
binuncert = 0.0 #0.05 => 5% of bin content added to "statistical" uncertainity
VRstatuncert = False
SRFile = "/mnt/t3nfs01/data01/shome/koschwei/tth/gc/sparsev5_final_v1/merged_SR_new.root"
CRFile = "/mnt/t3nfs01/data01/shome/koschwei/tth/gc/sparsev5_final_v1/merged_CR_new.root"
VRFile = "/mnt/t3nfs01/data01/shome/koschwei/tth/gc/sparsev5_final_v1/merged_VR_new.root"
CR2File ="/mnt/t3nfs01/data01/shome/koschwei/tth/gc/sparsev5_final_v1/merged_CR2_new.root"
method = "alphaCR" #"alphaCR", "alphaVR" or "ABCD"
scaleToData = False #for ABCD method
useMinorBkgs = True
useSignal = True
scaleToNom = False #temp
onlyDoMEM = True

#ddQCD systematics
ddQCDsys = {
    #"CMS_ttHbb_ddQCD_3b_2017": 0.01, #first bin uncertainty
    #"CMS_ttHbb_ddQCD_4b_2017": 0.03
}
ddQCDbins = [2,10]

#normalisation systematics
normsys = {"lumi_13TeV_2017": {"allMC":[1.023]},
           "pdf_Higgs_ttH": {"ttH_hbb":[1.036], "ttH_hcc":[1.036], "ttH_hww":[1.036], "ttH_hzz":[1.036], "ttH_htt":[1.036], "ttH_hgg":[1.036], "ttH_hgluglu":[1.036], "ttH_hzg":[1.036]},
           "pdf_gg": {"ttbarPlus2B":[1.04], "ttbarPlusB":[1.04], "ttbarPlusBBbar":[1.04], "ttbarPlusCCbar":[1.04], "ttbarOther":[1.04], "ttbarZ":[1.03]},
           "pdf_qqbar": {"ttbarW":[1.02], "wjets":[1.04], "zjets":[1.04], "diboson":[1.02]},
           "pdf_qg": {"singlet":[1.03]},
           "QCDscale_ttH": {"ttH_hbb":[1.058,0.908], "ttH_hcc":[1.058,0.908], "ttH_hww":[1.058,0.908], "ttH_hzz":[1.058,0.908], "ttH_htt":[1.058,0.908], "ttH_hgg":[1.058,0.908], "ttH_hgluglu":[1.058,0.908], "ttH_hzg":[1.058,0.908]},
           "QCDscale_ttbar": {"ttbarPlus2B":[1.02,0.96], "ttbarPlusB":[1.02,0.96], "ttbarPlusBBbar":[1.02,0.96], "ttbarPlusCCbar":[1.02,0.96], "ttbarOther":[1.02,0.96], "ttbarW":[1.13,0.88], "ttbarZ":[1.10,0.88]}, #"ttv":[1.13,0.88]}
           "QCDscale_singlet": {"singlet":[1.03,0.98]},
           "QCDscale_V": {"wjets":[1.01],"zjets":[1.01]},
           "QCDscale_VV": {"diboson":[1.02]},

           "fh_j7_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.044,0.958], },
           "fh_j7_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlus2B_2017": { "ttbarPlus2B":[1.049,0.954], },
           "fh_j7_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlusB_2017": { "ttbarPlusB":[1.045,0.957], },
           "fh_j7_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.073,0.932], },
           "fh_j7_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarOther_2017": { "ttbarOther":[1.045,0.957], },
           "fh_j8_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.084,0.922], },
           "fh_j8_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlus2B_2017": { "ttbarPlus2B":[1.125,0.889], },
           "fh_j8_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlusB_2017": { "ttbarPlusB":[1.093,0.915], },
           "fh_j8_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.092,0.916], },
           "fh_j8_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarOther_2017": { "ttbarOther":[1.076,0.929], },
           "fh_j9_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.104,0.906], },
           "fh_j9_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlus2B_2017": { "ttbarPlus2B":[1.123,0.891], },
           "fh_j9_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlusB_2017": { "ttbarPlusB":[1.059,0.944], },
           "fh_j9_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.099,0.91], },
           "fh_j9_t3__mem_FH_4w2h1t_p__CMS_ttHbb_HDAMP_ttbarOther_2017": { "ttbarOther":[1.103,0.907], },
           "fh_j7_t4__mem_FH_3w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.055,0.948], },
           "fh_j7_t4__mem_FH_3w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlus2B_2017": { "ttbarPlus2B":[1.127,0.887], },
           "fh_j7_t4__mem_FH_3w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlusB_2017": { "ttbarPlusB":[1.188,0.842], },
           "fh_j7_t4__mem_FH_3w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.086,0.921], },
           "fh_j7_t4__mem_FH_3w2h2t_p__CMS_ttHbb_HDAMP_ttbarOther_2017": { "ttbarOther":[1.105,0.905], },
           "fh_j8_t4__mem_FH_3w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.117,0.895], },
           "fh_j8_t4__mem_FH_3w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlus2B_2017": { "ttbarPlus2B":[1.106,0.904], },
           "fh_j8_t4__mem_FH_3w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlusB_2017": { "ttbarPlusB":[1.204,0.83], },
           "fh_j8_t4__mem_FH_3w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.068,0.936], },
           "fh_j8_t4__mem_FH_3w2h2t_p__CMS_ttHbb_HDAMP_ttbarOther_2017": { "ttbarOther":[1.053,0.95], },
           "fh_j9_t4__mem_FH_4w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.109,0.901], },
           "fh_j9_t4__mem_FH_4w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlus2B_2017": { "ttbarPlus2B":[1.132,0.883], },
           "fh_j9_t4__mem_FH_4w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlusB_2017": { "ttbarPlusB":[1.022,0.979], },
           "fh_j9_t4__mem_FH_4w2h2t_p__CMS_ttHbb_HDAMP_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.103,0.907], },
           "fh_j9_t4__mem_FH_4w2h2t_p__CMS_ttHbb_HDAMP_ttbarOther_2017": { "ttbarOther":[1.051,0.951], },

           "fh_j7_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.03,0.971], },
           "fh_j7_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlus2B_2017": { "ttbarPlus2B":[1.027,0.973], },
           "fh_j7_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlusB_2017": { "ttbarPlusB":[1.019,0.981], },
           "fh_j7_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.014,0.986], },
           "fh_j7_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarOther_2017": { "ttbarOther":[1.019,0.982], },
           "fh_j8_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.018,0.982], },
           "fh_j8_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlus2B_2017": { "ttbarPlus2B":[1.008,0.992], },
           "fh_j8_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlusB_2017": { "ttbarPlusB":[1.023,0.978], },
           "fh_j8_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.01,0.99], },
           "fh_j8_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarOther_2017": { "ttbarOther":[1.028,0.973], },
           "fh_j9_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.01,0.99], },
           "fh_j9_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlus2B_2017": { "ttbarPlus2B":[1.05,0.952], },
           "fh_j9_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlusB_2017": { "ttbarPlusB":[1.02,0.981], },
           "fh_j9_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.016,0.984], },
           "fh_j9_t3__mem_FH_4w2h1t_p__CMS_ttHbb_UE_ttbarOther_2017": { "ttbarOther":[1.033,0.968], },
           "fh_j7_t4__mem_FH_3w2h2t_p__CMS_ttHbb_UE_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.014,0.986], },
           "fh_j7_t4__mem_FH_3w2h2t_p__CMS_ttHbb_UE_ttbarPlus2B_2017": { "ttbarPlus2B":[1.136,0.88], },
           "fh_j7_t4__mem_FH_3w2h2t_p__CMS_ttHbb_UE_ttbarPlusB_2017": { "ttbarPlusB":[1.156,0.865], },
           "fh_j7_t4__mem_FH_3w2h2t_p__CMS_ttHbb_UE_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.061,0.943], },
           "fh_j7_t4__mem_FH_3w2h2t_p__CMS_ttHbb_UE_ttbarOther_2017": { "ttbarOther":[1.171,0.854], },
           "fh_j8_t4__mem_FH_3w2h2t_p__CMS_ttHbb_UE_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.038,0.963], },
           "fh_j8_t4__mem_FH_3w2h2t_p__CMS_ttHbb_UE_ttbarPlus2B_2017": { "ttbarPlus2B":[1.021,0.98], },
           "fh_j8_t4__mem_FH_3w2h2t_p__CMS_ttHbb_UE_ttbarPlusB_2017": { "ttbarPlusB":[1.097,0.912], },
           "fh_j8_t4__mem_FH_3w2h2t_p__CMS_ttHbb_UE_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.054,0.948], },
           "fh_j8_t4__mem_FH_3w2h2t_p__CMS_ttHbb_UE_ttbarOther_2017": { "ttbarOther":[1.1,0.909], },
           "fh_j9_t4__mem_FH_4w2h2t_p__CMS_ttHbb_UE_ttbarPlusBBbar_2017": { "ttbarPlusBBbar":[1.003,0.997], },
           "fh_j9_t4__mem_FH_4w2h2t_p__CMS_ttHbb_UE_ttbarPlus2B_2017": { "ttbarPlus2B":[1.066,0.938], },
           "fh_j9_t4__mem_FH_4w2h2t_p__CMS_ttHbb_UE_ttbarPlusB_2017": { "ttbarPlusB":[1.042,0.96], },
           "fh_j9_t4__mem_FH_4w2h2t_p__CMS_ttHbb_UE_ttbarPlusCCbar_2017": { "ttbarPlusCCbar":[1.043,0.959], },
           "fh_j9_t4__mem_FH_4w2h2t_p__CMS_ttHbb_UE_ttbarOther_2017": { "ttbarOther":[1.128,0.887], },
           # "CMS_ttHbb_bgnorm_ttbarPlus2B": {"ttbarPlus2B":[1.5]},#TEMP was 1.5
           # "CMS_ttHbb_bgnorm_ttbarPlusB": {"ttbarPlusB":[1.5]}, #TEMP
           # "CMS_ttHbb_bgnorm_ttbarPlusBBbar": {"ttbarPlusBBbar":[1.5]}, #TEMP
           # "CMS_ttHbb_bgnorm_ttbarPlusCCbar": {"ttbarPlusCCbar":[1.5]}
        
           "CMS_ttHbb_bgnorm_ttbarPlus2B_2017": {"ttbarPlus2B":[1.5]},#TEMP was 1.5
           "CMS_ttHbb_bgnorm_ttbarPlusB_2017": {"ttbarPlusB":[1.5]}, #TEMP
           "CMS_ttHbb_bgnorm_ttbarPlusBBbar_2017": {"ttbarPlusBBbar":[1.5]}, #TEMP
           "CMS_ttHbb_bgnorm_ttbarPlusCCbar_2017": {"ttbarPlusCCbar":[1.5]}

           
}

def MakeControlplots( datahisto, ttbarhistos, qcdhisto, canvas , scale=1, blind = False):
    hData = datahisto
    hData.SetLineColor(1) 
    hData.SetMarkerColor(1) 
    hData.SetMarkerStyle(20)
    hData.SetMarkerSize(1)
    stack = ROOT.THStack()
    qcdhisto.SetLineColor(ROOT.kBlack)
    qcdhisto.SetFillStyle(1001) 
    qcdhisto.SetFillColor(ROOT.kYellow)
    qcdhisto.Scale(scale)
    stack.Add(qcdhisto)
    n = 0
    for histo in ttbarhistos:
        histo.SetLineColor(ROOT.kBlack)
        histo.SetFillStyle(1001)
        histo.SetFillColor(ROOT.kRed+n)
        n += 1
        stack.Add(histo)
    stack.Draw("histo")
    if not blind:
        hData.Draw("same p")
    c1.Update()


def checkifttSys(systematic, allttProc):
    for tt in allttProc:
        if tt in systematic:
            return True
    return False

print "\nSR  file:",SRFile
print "CR  file:",CRFile
print "VR  file:",VRFile
print "CR2 file:",CR2File
print "method:", method
if method=="ABCD": print "scaleToData:", scaleToData
print "useMinorBkgs:", useMinorBkgs
print "useSignal:", useSignal
print "scaleToNom:", scaleToNom
print "additional bin-by-bin uncertainty:", binuncert
print "add data stat. uncertainty from VR:", VRstatuncert
#a = raw_input("\nPress return to continue, q to quit ")
#if a == "q": quit()
print "\n Sleep for 20 sec to confirm..."
#sleep(20)

#Get Histos
fCR = None
fVR = None
fCR2 = None
if method=="alphaCR" or method=="ABCD":
    fCR = ROOT.TFile.Open(CRFile)
    CRHistos = {}
    for key in fCR.GetListOfKeys():
        CRHistos.update({key.GetName() : key.ReadObj()})
if method=="alphaVR" or method=="ABCD":
    fVR = ROOT.TFile.Open(VRFile)
    VRHistos = {}
    for key in fVR.GetListOfKeys():
        VRHistos.update({key.GetName() : key.ReadObj()})
if method=="ABCD":
    fCR2 = ROOT.TFile.Open(CR2File)
    CR2Histos = {}
    for key in fCR2.GetListOfKeys():
        CR2Histos.update({key.GetName() : key.ReadObj()})

fSR = ROOT.TFile(SRFile,"READ") #temp? until tthnon is split in CR

#Get Categories, variables, systematics
catvars = set()
systematics = set()
ttbartypes = set()
backgrounds = set()
signals = set()
montecarlo = set()
for key in fCR.GetListOfKeys():
#for key in fSR.GetListOfKeys(): #temp
    parts = key.GetName().split("__")
    if not "data" in parts[0]:
        montecarlo.add( parts[0] )
    if "ttbar" in parts[0] and not parts[0].endswith("Z") and not parts[0].endswith("W"):
        ttbartypes.add( parts[0] )
        if "mem" in parts[2]:#or "ht30" in parts[2]:
            catvars.add( parts[1]+"__"+parts[2] )
        if len(parts)>3:
            systematics.add( "__"+parts[3] )
        else:
            systematics.add( "" )
    elif not "ttH" in parts[0] and not "data" in parts[0] and not "qcd" in parts[0]:
        backgrounds.add( parts[0] )
    elif "ttH" in parts[0]:
        signals.add( parts[0] )


#catvars = list(catvars)[0]
        
print catvars
print "N_systematics:", len(systematics)
print "Minor backgrounds:", backgrounds
print "ttbar samples:", ttbartypes
print "Signals", signals
print "All MC samples:", montecarlo
raw_input("Waiting for confirmation. Press ret")

if debugging:
    c1 = ROOT.TCanvas() #RM
    c1.cd() #RM
    canvasList = []
#Get QCD histogram from CR
#DataHistosCR = {}
#TTbarHistosCR = {}
#BkgHistosCR = {}
if method=="alphaCR" or method=="ABCD": 
    QCDHistosCR = {}
    nQCD_CR = {}
    print "\nGetting ddQCD histogram from CR"
    for catvar in catvars:
        for isys, sys in enumerate(systematics):
            #print isys, nSyst
            if debugging and isys > nSyst:
                break
            #print ".",
            nData = 0
            nTTbar = 0
            nBkg = 0.0
            datakey = "data__"+catvar
            if "data__"+catvar+sys in CRHistos: #ddQCD HTreweight applied to data in CR
                if "HTreweight3b" in sys  and "t4" in catvar:
                    pass
                elif "HTreweight4b" in sys  and "t3" in catvar:
                    pass
                else:
                    datakey = "data__"+catvar+sys
            hQCD = CRHistos[datakey].Clone()
            hQCD.SetName("ddQCD__"+catvar+sys)
            hQCD.SetTitle("ddQCD__"+catvar+sys)
            nData = hQCD.Integral()
            hDataCR = CRHistos[datakey].Clone() ##need?
            ttbarlist = []
            #bkglist = []
            for tt in ttbartypes:
                ttbarkey = tt+"__"+catvar+sys
                if "HTreweight3b" in sys  and "t4" in catvar:
                    print "Replacing",sys,"in 4tag cat with nominal"
                    ttbarkey = tt+"__"+catvar
                if "HTreweight4b" in sys  and "t3" in catvar:
                    print "Replacing",sys,"in 3tag cat with nominal"
                    ttbarkey = tt+"__"+catvar
                if checkifttSys(sys, ttbartypes) and tt+"_" not in sys:
                    print "Replacing",sys,"in",tt,"with nominal"
                    ttbarkey = tt+"__"+catvar
                if not ttbarkey in CRHistos:
                    print "missing", ttbarkey, "use nominal"
                    ttbarkey = tt+"__"+catvar
                hQCD = hQCD - CRHistos[ttbarkey]
                nTTbar += CRHistos[ttbarkey].Integral()
                ttbarlist.append(deepcopy(CRHistos[ttbarkey])) ###need?
            if useMinorBkgs: 
                for bkg in backgrounds:
                    if "ttH" in bkg: continue #exclude signal from control region (small and mem not calculated)
                    bkgkey = bkg+"__"+catvar+sys
                    if "HTreweight3b" in sys  and "t4" in catvar:
                        bkgkey = bkg+"__"+catvar
                    if "HTreweight4b" in sys  and "t3" in catvar:
                        bkgkey = bkg+"__"+catvar
                    if "scaleMu" in sys:
                        bkgkey = bkg+"__"+catvar
                    if "CMS_ttHbb_PDF" in sys:
                        bkgkey = bkg+"__"+catvar
                    if checkifttSys(sys, ttbartypes):
                        print "Replacing ttbar systematic",sys," in bkg",bkg,"with nominal"
                        bkgkey = bkg+"__"+catvar
                    if not bkgkey in CRHistos:
                        print "missing", bkgkey, "use nominal"
                        bkgkey = bkg+"__"+catvar
                    hQCD = hQCD - CRHistos[bkgkey]
                    nBkg += CRHistos[bkgkey].Integral()
                    #bkglist.append(deepcopy(CRHistos[bkgkey]))

            if debugging:
                print "DEBUG CR: (catvar, sys):(",catvar,sys,"):",nData,nTTbar,nData-nTTbar
            QCDHistosCR.update({catvar+sys : deepcopy(hQCD)})
            nQCD_CR.update({catvar+sys : nData - nTTbar - nBkg})
            #DataHistosCR.update({catvar+sys : deepcopy(hDataCR)})
            #TTbarHistosCR.update({catvar+sys : deepcopy(ttbarlist)})
            #if useMinorBkgs:
            #    BkgHistosCR.update({catvar+sys : deepcopy(bkglist)})
            #Make controlplots
            if debugging:
                MakeControlplots(hDataCR, ttbarlist, hQCD, c1)
                #raw_input("DEBUG CR: Press ret for next category")
                sleep(0.033)
            del hQCD, hDataCR, ttbarlist#, bkglist
        for sys, dic in normsys.items():
            samples = []
            if "allMC" in dic:
                samples = ttbartypes.union(backgrounds if useMinorBkgs else [])
            else:
                samples = [ s for s in dic ]
            if sys.startswith("fh_j") and not catvar in sys:
                continue
            for sdir in ["Up","Down"]: 
                nData = 0
                nMC = 0.0
                datakey = "data__"+catvar
                hQCD = CRHistos[datakey].Clone("ddQCD__"+catvar+"__"+sys+sdir)
                hQCD.SetTitle("ddQCD__"+catvar+"__"+sys+sdir)
                nData = hQCD.Integral()
                for proc in ttbartypes.union(backgrounds if useMinorBkgs else []):
                    if "ttH" in proc: continue
                    mckey = proc+"__"+catvar
                    hMC = CRHistos[mckey].Clone(mckey+"__"+sys+sdir)
                    if proc in samples:
                        key = "allMC" if "allMC" in dic else proc
                        up = dic[key][0]
                        down = dic[key][1] if len(dic[key])==2 else 1/dic[key][0]
                        hMC.Scale(up if sdir=="Up" else down)
                    hQCD = hQCD - hMC
                    nMC += hMC.Integral()
                QCDHistosCR.update({catvar+"__"+sys+sdir : deepcopy(hQCD)})
                nQCD_CR.update({catvar+"__"+sys+sdir : nData - nMC})
                del hQCD, hMC

if method == "ABCD":
    #Get QCD histogram from CR2
    QCDHistosCR2 = {}
    #DataHistosCR2 = {}
    #TTbarHistosCR2 = {}
    print "\nGetting ddQCD histogram from CR2"
    for catvar in catvars:
        print "doing", catvar
        for sys in systematics:
            #print ".",
            datakey = "data__"+catvar
            hQCDCR2 = CR2Histos[datakey].Clone("ddQCD__"+catvar+sys)
            #hDataCR2 = CR2Histos[datakey].Clone()
            #ttbarlistCR2 = []
            for tt in ttbartypes:
                ttbarkey = tt+"__"+catvar+sys
                hQCDCR2 = hQCDCR2 - CR2Histos[ttbarkey]
                #ttbarlistCR2.append(deepcopy(CR2Histos[ttbarkey]))
            if useMinorBkgs:
                for bkg in backgrounds:
                    if "ttH" in bkg: continue
                    bkgkey = bkg+"__"+catvar+sys
                    hQCDCR2 = hQCDCR2 - CR2Histos[bkgkey]
            QCDHistosCR2.update({catvar+sys : deepcopy(hQCDCR2)})
            #DataHistosCR2.update({catvar+sys : deepcopy(hDataCR2)})
            #TTbarHistosCR2.update({catvar+sys : deepcopy(ttbarlistCR2)})
            del hQCDCR2 #hDataCR2, ttbarlistCR2
        for sys, dic in normsys.items():
            samples = []
            if "allMC" in dic:
                samples = ttbartypes.union(backgrounds if useMinorBkgs else [])
            else:
                samples = [ s for s in dic ]
            if sys.startswith("fh_j") and not catvar in sys:
                continue
            for sdir in ["Up","Down"]: 
                datakey = "data__"+catvar
                hQCDCR2 = CR2Histos[datakey].Clone("ddQCD__"+catvar+"__"+sys+sdir)
                for proc in ttbartypes.union(backgrounds if useMinorBkgs else []):
                    if "ttH" in proc: continue
                    mckey = proc+"__"+catvar
                    hMC = CR2Histos[mckey].Clone(mckey+"__"+sys+sdir)
                    if proc in samples:
                        key = "allMC" if "allMC" in dic else proc
                        up = dic[key][0]
                        down = dic[key][1] if len(dic[key])==2 else 1/dic[key][0]
                        hMC.Scale(up if sdir=="Up" else down)
                    hQCDCR2 = hQCDCR2 - hMC
                QCDHistosCR2.update({catvar+"__"+sys+sdir : deepcopy(hQCDCR2)})
                del hQCDCR2, hMC

if method=="alphaVR" or method=="ABCD":
    #Get QCD histogram from VR
    QCDHistosVR = {}
    nQCD_VR = {}
    print "\nGetting ddQCD histogram from VR"
    for catvar in catvars:
        print "doing", catvar
        for sys in systematics:
            #print ".",
            nData = 0
            nTTbar = 0
            nBkg = 0.0
            datakey = "data__"+catvar
            hQCDVR = VRHistos[datakey].Clone("ddQCD__"+catvar+sys)
            hQCDVR.SetTitle("ddQCD__"+catvar+sys)
            nData = hQCDVR.Integral()
            for tt in ttbartypes:
                ttbarkey = tt+"__"+catvar+sys
                hQCDVR = hQCDVR - VRHistos[ttbarkey]
                nTTbar += VRHistos[ttbarkey].Integral()
            if useMinorBkgs:
                for bkg in backgrounds:
                    if "ttH" in bkg: continue
                    bkgkey = bkg+"__"+catvar+sys
                    hQCDVR = hQCDVR - VRHistos[bkgkey]
                    nBkg += VRHistos[bkgkey].Integral()
            QCDHistosVR.update({catvar+sys : deepcopy(hQCDVR)})
            nQCD_VR.update({catvar+sys : nData - nTTbar - nBkg})
            del hQCDVR
        for sys, dic in normsys.items():
            samples = []
            if "allMC" in dic:
                samples = ttbartypes.union(backgrounds if useMinorBkgs else [])
            else:
                samples = [ s for s in dic ]
            if sys.startswith("fh_j") and not catvar in sys:
                continue
            for sdir in ["Up","Down"]: 
                nData = 0
                nMC = 0.0
                datakey = "data__"+catvar
                hQCDVR = VRHistos[datakey].Clone("ddQCD__"+catvar+"__"+sys+sdir)
                hQCDVR.SetTitle("ddQCD__"+catvar+"__"+sys+sdir)
                nData = hQCDVR.Integral()
                for proc in ttbartypes.union(backgrounds if useMinorBkgs else []):
                    if "ttH" in proc: continue
                    mckey = proc+"__"+catvar
                    hMC = VRHistos[mckey].Clone(mckey+"__"+sys+sdir)
                    if proc in samples:
                        key = "allMC" if "allMC" in dic else proc
                        up = dic[key][0]
                        down = dic[key][1] if len(dic[key])==2 else 1/dic[key][0]
                        hMC.Scale(up if sdir=="Up" else down)
                    hQCDVR = hQCDVR - hMC
                    nMC += hMC.Integral()
                QCDHistosVR.update({catvar+"__"+sys+sdir : deepcopy(hQCDVR)})
                nQCD_VR.update({catvar+"__"+sys+sdir : nData - nMC})
                del hQCDVR, hMC

print "\nGetting histograms from the SR sparse file"
#Add to SR ROOT File
#fSR = ROOT.TFile(SRFile,"update")
fSR.cd()
SRHistos = {}
for key in fSR.GetListOfKeys():
    if len(key.GetName().split("__")) == 4: #KS Temp
        proc, cat, var, sys = key.GetName().split("__") #KS Temp
        if "HTreweight3b" in sys and "t4" in cat: #KS Temp
            if "mem" in var:
                print "skipping key",key,"in SR"
            continue #KS Temp
        if "HTreweight4b" in  sys and "t3" in cat: #KS Temp
            if "mem" in var:
                print "skipping key",key,"in SR"
            continue #KS Temp
        if "scaleMu" in sys and proc not in ttbartypes:
            if "mem" in var:
                print "skipping key",key,"in SR"
            continue #KS Temp
        if "CMS_ttHbb_PDF" in sys and proc in backgrounds:
            if "mem" in var:
                print "skipping key",key,"in SR"
            continue #KS Temp
        if checkifttSys(sys, ttbartypes) and proc+"_" not in sys:
            if "mem" in var:
                print "skipping key",key,"in SR for sys",sys
            continue
    SRHistos.update({key.GetName() : key.ReadObj()})

NewHistosSR = {}
nQCD_SR = {}
for catvar in catvars:
    print "doing", catvar
    for isys, sys in enumerate(systematics):
        if debugging and isys > nSyst:
            break
        #print ".",
        nData = 0
        nTTbar = 0
        nBkg = 0.0
        datakey = "data__"+catvar
        nData = SRHistos[datakey].Integral()
        hData = SRHistos[datakey].Clone()
        ttbarlist = []
        for tt in ttbartypes:
            if tt+"__"+catvar+sys in SRHistos:
                ttbarkey = tt+"__"+catvar+sys
            else:
                print tt+"__"+catvar+sys, "does not exist in SR. using nominal"
                ttbarkey = tt+"__"+catvar
            nTTbar += SRHistos[ttbarkey].Integral()
            ttbarlist.append(SRHistos[ttbarkey].Clone()) ##need?
        if useMinorBkgs:
            for bkg in backgrounds:
                if bkg+"__"+catvar+sys in SRHistos:
                    bkgkey = bkg+"__"+catvar+sys
                else:
                    print bkg+"__"+catvar+sys, "does not exist in SR. using nominal"
                    bkgkey = bkg+"__"+catvar
                nBkg += SRHistos[bkgkey].Integral()
        if useSignal: #include signal in yield calculation only
            for sig in signals:
                if  sig+"__"+catvar+sys in SRHistos:
                    sigkey = sig+"__"+catvar+sys
                else:
                    print sig+"__"+catvar+sys, "does not exist in SR. using nominal"
                    sigkey = sig+"__"+catvar
                nBkg += SRHistos[sigkey].Integral()
        if debugging:
            print "DEBUG SR: (catvar, sys):(",catvar,sys,"):",nData,nTTbar,nData-nTTbar
        nQCD_SR.update({catvar+sys : nData - nTTbar - nBkg})
        if debugging:
            qcdkey = catvar+sys
            MakeControlplots(hData,ttbarlist, QCDHistosCR[qcdkey],c1,nQCD_SR[qcdkey]/nQCD_CR[qcdkey], blindDebug)
            raw_input("DEBUG SR: Press ret for next category")
            sleep(0.033)
        del hData, ttbarlist
    #add normalisation uncertainties as shape uncertainties
    for sys, dic in normsys.items():
        samples = []
        if "allMC" in dic:
            samples = ttbartypes.union(backgrounds if useMinorBkgs else []).union(signals if useSignal else [])
        else:
            samples = [ s for s in dic ]
        if sys.startswith("fh_j") and not catvar in sys:
            continue
        for sdir in ["Up","Down"]: 
            nData = 0
            nMC = 0.0
            datakey = "data__"+catvar
            nData = SRHistos[datakey].Integral()
            for proc in montecarlo:
                mckey = proc+"__"+catvar
                hMC = SRHistos[mckey].Clone(mckey+"__"+sys+sdir)
                hMC.SetTitle(mckey+"__"+sys+sdir)
                if proc in samples:
                    key = "allMC" if "allMC" in dic else proc
                    up = dic[key][0]
                    down = dic[key][1] if len(dic[key])==2 else 1/dic[key][0]
                    hMC.Scale(up if sdir=="Up" else down)
                    NewHistosSR.update({mckey+"__"+sys+sdir : deepcopy(hMC)}) #uncomment to only save affected histos
                if proc in ttbartypes.union(backgrounds if useMinorBkgs else []).union(signals if useSignal else []):
                    nMC += hMC.Integral()
                #NewHistosSR.update({mckey+"__"+sys+sdir : deepcopy(hMC)}) #comment to only save affected histos
            nQCD_SR.update({catvar+"__"+sys+sdir : nData - nMC})
            del hMC
    # #add ddQCD systematics (try to apply only to ddQCD)
    # for sys, val in ddQCDsys.item():
    #     for sdir in ["Up","Down"]: 
            


QCDHistosSR = {}
print "\nWriting data driven QCD background histos to SR ROOT file"

outfile = ROOT.TFile(SRFile[0:-5]+"_updated.root", "RECREATE")

if binuncert > 0:
    print "adding {0}% bin-by-bin uncertainty".format(binuncert*100)
if VRstatuncert:
    print "adding VRdatastat bin-by-bin uncertainty"
    fileVR = ROOT.TFile.Open(VRFile)
    VRdataHistos = {}
    for key in fileVR.GetListOfKeys():
        if"data__" in key.GetName():
            print "key from VRfile:", key #temp
            VRdataHistos.update({key.GetName() : key.ReadObj()})
    fSR.cd()
    
outfile.cd()
for key in (QCDHistosVR if method=="alphaVR" else QCDHistosCR):
    #print ".",
    if method=="alphaCR":
        kparts = key.split("__")
        if len(kparts)<3:
            print "less than 3 parts:", key, nQCD_SR[key], QCDHistosCR[key].GetName()
        nomkey = kparts[0]+"__"+kparts[1]
        if scaleToNom: #fix all ddQCD yields to nominal
            QCDHistosCR[key].Scale(nQCD_SR[nomkey]/nQCD_CR[key])
        else:
            QCDHistosCR[key].Scale(nQCD_SR[key]/nQCD_CR[key])
        QCDHistosSR.update({key : deepcopy(QCDHistosCR[key])})
    elif method=="alphaVR":
        QCDHistosVR[key].Scale(nQCD_SR[key]/nQCD_VR[key])
        QCDHistosSR.update({key : deepcopy(QCDHistosVR[key])})
    elif method=="ABCD":
        hqcd_sr = QCDHistosCR[key].Clone()
        for i in range(QCDHistosCR[key].GetXaxis().GetNbins()+2):
            cr = QCDHistosCR[key].GetBinContent(i)
            cr2 = QCDHistosCR2[key].GetBinContent(i)
            vr = QCDHistosVR[key].GetBinContent(i)
            sr = cr*vr/cr2 if cr2>0 else 0.0

            err_cr = QCDHistosCR[key].GetBinError(i)
            err_cr2 = QCDHistosCR2[key].GetBinError(i)
            err_vr = QCDHistosVR[key].GetBinError(i)
            err_sr = sr * ( ( (err_vr/vr)**2 if vr>0 else 0 ) +
                            ( (err_cr/cr)**2 if cr>0 else 0 ) +
                            ( (err_cr2/cr2)**2 if cr2>0 else 0 ) )**0.5
            
            hqcd_sr.SetBinContent(i, sr)
            hqcd_sr.SetBinError(i, err_sr)
        if scaleToData:
            hqcd_sr.Scale(nQCD_SR[key]/hqcd_sr.Integral())
        QCDHistosSR.update({key : deepcopy(hqcd_sr)})
        
    if binuncert > 0:
        for bin in range(QCDHistosSR[key].GetNbinsX()+2):
            err = QCDHistosSR[key].GetBinError(bin)
            add = QCDHistosSR[key].GetBinContent(bin)*binuncert
            err = (err**2 + add**2)**0.5
            QCDHistosSR[key].SetBinError(bin,err)
    if VRstatuncert:
        dkey = "data__"+key.split("__")[0]+"__"+key.split("__")[1]
        for bin in range(QCDHistosSR[key].GetNbinsX()+2):
            err = QCDHistosSR[key].GetBinError(bin)
            add = VRdataHistos[dkey].GetBinError(bin)
            err = (err**2 + add**2)**0.5
            QCDHistosSR[key].SetBinError(bin,err)
            #print add, #temp
        #print "" #temp

    #apply ddQCD systematics to mem, first get nominal
    if len(key.split("__"))==2 and "mem_FH" in key:
        #print "applying ddQCDsys to", key
        for sys, val in ddQCDsys.items():
            for sdir in ["Up","Down"]:
                print "making", key+"__"+sys+sdir
                hqcd = QCDHistosCR[key].Clone("ddQCD__"+key+"__"+sys+sdir)
                hqcd.SetTitle("ddQCD__"+key+"__"+sys+sdir)
                if ("3b" in sys and "t3" in key) or ("4b" in sys and "t4" in key):
                    if sdir=="Up":
                        diff = val*hqcd.GetBinContent(1)
                    else:
                        diff = -val*hqcd.GetBinContent(1)
                    newbin = hqcd.GetBinContent(1)+diff
                    integral = hqcd.Integral(ddQCDbins[0],ddQCDbins[1])
                    hqcd.Scale( (integral-diff)/integral)
                    hqcd.SetBinContent(1,newbin)
                QCDHistosSR.update({key+"__"+sys+sdir : deepcopy(hqcd)})
                if not debugging:
                    QCDHistosSR[key+"__"+sys+sdir].Write()
                else:
                    print key+"__"+sys+sdir,"not written"

    if not debugging:
        QCDHistosSR[key].Write()
    else:
        print "DEBUG: Scaling",key,":",nQCD_SR[key]/nQCD_CR[key]
        print "DEBUG: QCD histo for",key,"is *NOT* written to ROOT file"

print "\nWriting normalisation systematic histos to SR ROOT file"
for key in NewHistosSR:
    if not debugging:
        NewHistosSR[key].Write()
    else:
        print key

print "Closing output ROOT file"
outfile.Close()
ROOT.gROOT.GetListOfFiles().Remove(fSR);
ROOT.gROOT.GetListOfFiles().Remove(fVR);
ROOT.gROOT.GetListOfFiles().Remove(fCR);
ROOT.gROOT.GetListOfFiles().Remove(fCR2);
print "done!"
exit()
