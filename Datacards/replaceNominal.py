import ROOT
import sys
from copy import deepcopy


infile = sys.argv[1]

rFile = ROOT.TFile.Open(infile)

keys = rFile.GetListOfKeys()

histos = []
for key in keys:
    keyName = key.GetName()
    histos.append(deepcopy(rFile.Get(keyName)))


rFile.Close()
outfile = ROOT.TFile(infile.replace(".root","_replaced.root"),"RECREATE")
outfile.cd()
"""
for histo in histos:
    if histo.GetName().split("__") == 3:
        continue
    if "CRCorr_off" in histo.GetName():
        print "Setting",histo.GetName(),"to",histo.GetName()[0:-len("__CRCorr_off")]
        histo.SetName(histo.GetName()[0:-len("__CRCorr_off")])
    histo.Write()
"""

for histo in histos:
    if not "data" in histo.GetName():
        histo.Write()
        continue
    if histo.GetName().split("__") == 3:
        continue
    if "unweighted" in histo.GetName():
        print "Setting",histo.GetName(),"to",histo.GetName()[0:-len("__unweighted")]
        histo.SetName(histo.GetName()[0:-len("__unweighted")])
    histo.Write()
