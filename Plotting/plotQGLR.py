import ROOT
from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos
import Helper
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.INFO,
)
#############################################################
#############################################################

def main():
    SignalInput = {
        "names" : ["ttH"],
        "files" : ["root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v4/pre/v3/ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8.root"],
        "nGens" : [Helper.nGen["ttH_hbb"]],
        "xsecs" : [Helper.xsecs["ttH_hbb"]],
        "color" : ROOT.kBlue
        }
    BackgroundInput = {
        "names" : ["ttAH", "ttDL", "ttSL"],
        "files" : ["root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v4/pre/v3/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root",
                   "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v4/pre/v3/TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root",
                   "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v4/pre/v3/TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root"],
        "nGens" : [Helper.nGen["TTbar_fh"], Helper.nGen["TTbar_dl"], Helper.nGen["TTbar_sl"]],
        "xsecs" : [Helper.xsecs["TTbar_fh"], Helper.xsecs["TTbar_dl"], Helper.xsecs["TTbar_sl"]],
        "color" : ROOT.kRed
    }
    DataInput = {
        "names" : ["data"],
        "files" : ["root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v4/pre/v3/data.root"],
        "nGens" : [1],
        "xsecs" : [1],
        "color" : ROOT.kBlack
    }
    rfiles = {}
    trees = {}
    allFiles = SignalInput["files"]+BackgroundInput["files"]+DataInput["files"]
    allNames = SignalInput["names"]+BackgroundInput["names"]+DataInput["names"]
    logging.info("Creating TFiles and TTrees")
    for i in range(len(allFiles)):
        #rfiles[allNames[i]] = ROOT.TFile(allFiles[i], "READ")
        rfiles[allNames[i]] = ROOT.TFile.Open(allFiles[i])
        trees[allNames[i]] = rfiles[allNames[i]].Get("tree")

    
    mcWeight = "puWeight * (sign(genWeight)) *  btagWeight_shape * triggerFHWeight * qgWeight"
    #cuts = Helper.get_cuts()
    preSel2017 = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1) && ht30>500 && jets_pt[5]>40 && nBDeepCSVM>=2  && Flag_METFilters == 1"
    categories = {
        "3b" : "{0} && ({1} || {2})".format(preSel2017, "nBDeepCSVM==3", "Sum$(jets_btagDeepCSV>0.1522)==3 && nBDeepCSVM==2"),
        "4b" : "{0} && ({1} || {2})".format(preSel2017, "nBDeepCSVM>=4", "Sum$(jets_btagDeepCSV>0.1522)>=4 && nBDeepCSVM==2")
    }
    histos = [ROOT.TH1F("hBaseQGLR3b","hBaseQGLR3b", 40,0,1),
              ROOT.TH1F("hBaseQGLR4b","hBaseQGLR4b", 40,0,1)]
    variables = [
        "qg_LR_3b_flavour_5q_0q",
        "qg_LR_4b_flavour_5q_0q"
    ]
    niceNames = {
        "qg_LR_4b_flavour_5q_0q": "QGLR (4b)",
        "qg_LR_3b_flavour_5q_0q": "QGLR (3b)"
    }
    for icut, cat in enumerate(categories):
        logging.info("Processing cut: %s",str(icut))
        allCanvases = []
        for ivar, variable in enumerate(variables):
            logging.info("Processing variables: %s", variable)
            hSignal = histos[ivar].Clone("hSignal_"+variable+"_"+cat)
            hSignal.SetLineColor(SignalInput["color"])
            for iname, name in enumerate(SignalInput["names"]):
                tName = hSignal.GetName()+"_"+str(iname)
                hSigTmp = hSignal.Clone(tName)
                weight = "((1000 * {0})/{1}) * {2}".format(SignalInput["xsecs"][iname], SignalInput["nGens"][iname], mcWeight)
                trees[name].Project(tName, variable, "({0}) * ({1})".format(categories[cat], weight))
                hSignal.Add(hSigTmp) 
            hBackground = histos[ivar].Clone("hBackground_"+variable+"_"+str(icut))
            hBackground.SetLineColor(BackgroundInput["color"])
            for iname, name in enumerate(BackgroundInput["names"]):
                tName = hBackground.GetName()+"_"+str(iname)
                hBkgTmp = hBackground.Clone(tName)
                weight = "((1000 * {0})/{1}) * {2}".format(BackgroundInput["xsecs"][iname], BackgroundInput["nGens"][iname], mcWeight)
                trees[name].Project(tName, variable, "({0}) * ({1})".format(categories[cat], weight))
                hBackground.Add(hBkgTmp)
                
            hData = histos[ivar].Clone("hData_"+variable+"_"+str(icut))
            hData.SetLineColor(DataInput["color"])
            for iname, name in enumerate(DataInput["names"]):
                tName = hData.GetName()+"_"+str(iname)
                hBkgTmp = hData.Clone(tName)
                weight = "1"
                trees[name].Project(tName, variable, "({0}) * ({1})".format(categories[cat], weight))
                hData.Add(hBkgTmp)
                
            allCanvases.append(makeCanvasOfHistos("blubb"+str(ivar),niceNames[variable],[hSignal, hBackground, hData], legendText = ["ttH","ttbar","Data"], normalized = True, legendSize=(0.2,0.5,0.5,0.8), lineWidth = 2))
        saveCanvasListAsPDF(allCanvases, "QGLR_"+cat,".")

    
if __name__ == "__main__":
    main()
