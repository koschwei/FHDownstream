#include<algorithm>

float bMLCorrMC(float pt, float eta, float minDeltaRb, float csv){
    if(csv<0.152200 || csv>0.255000) return 1;
    else return ((0.947136-0.139038*minDeltaRb+0.542246*pow(minDeltaRb,2)-0.514814*pow(minDeltaRb,3)+0.211383*pow(minDeltaRb,4)-0.0401395*pow(minDeltaRb,5)+0.00288099*pow(minDeltaRb,6)) )*((1.12083-0.0290764*eta-0.190702*pow(eta,2)+0.166495*pow(eta,3)+0.268487*pow(eta,4)-0.30672*pow(eta,5)-0.40047*pow(eta,6)+0.255482*pow(eta,7)+0.312587*pow(eta,8)-0.111259*pow(eta,9)-0.128823*pow(eta,10)+0.0262953*pow(eta,11)+0.028745*pow(eta,12)-0.00319164*pow(eta,13)-0.00329277*pow(eta,14)+0.000155653*pow(eta,15)+0.000151851*pow(eta,16)) )*( (68.4285-0.000287597*pt)*erf((pt+751.054)*0.00340275)-67.394);
}

float bMLCorrData(float pt, float eta, float minDeltaRb, float csv){
    if(csv<0.152200 || csv>0.255000) return 1;
    else return ((1.62359-1.67368*minDeltaRb+1.94544*pow(minDeltaRb,2)-1.18727*pow(minDeltaRb,3)+0.386353*pow(minDeltaRb,4)-0.0633036*pow(minDeltaRb,5)+0.0041006*pow(minDeltaRb,6)) )*((1.10065-0.00442692*eta-0.0335053*pow(eta,2)+0.0478638*pow(eta,3)-0.0256095*pow(eta,4)-0.121206*pow(eta,5)-0.0956982*pow(eta,6)+0.123193*pow(eta,7)+0.125825*pow(eta,8)-0.0609718*pow(eta,9)-0.0608276*pow(eta,10)+0.0156551*pow(eta,11)+0.0144636*pow(eta,12)-0.00200347*pow(eta,13)-0.00170288*pow(eta,14)+0.000100959*pow(eta,15)+7.94039e-05*pow(eta,16)) )*( (56.7474-3.5734e-05*pt)*erf((pt+2377.74)*0.00126049)-55.7451);
}
