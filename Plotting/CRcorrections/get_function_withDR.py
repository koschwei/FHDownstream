import ROOT, os
from copy import deepcopy
#from getChain import get_vhbb_chain, get_vhbb_counts
ROOT.gStyle.SetOptStat(0)
#### Comment me! ###
ROOT.gROOT.SetBatch(1)
ROOT.Math.MinimizerOptions.SetDefaultMinimizer("Minuit2","Minimize");
#####################

"""
rank not implemented for DeepCSV atm! Be substituted Max$ and MaxIf$

jets_CSVrank>=2

== 

( jets_btagDeepCSV < Max$(jets_btagDeepCSV) && jets_btagDeepCSV < MaxIf$(jets_btagDeepCSV, jets_btagDeepCSV < Max$(jets_btagDeepCSV)) )



"""

ROOT.gROOT.LoadMacro("../classes.h")
ROOT.gInterpreter.ProcessLine("TriggerSF3D* internalTriggerSF3D = new TriggerSF3D()")
ROOT.gROOT.LoadMacro("../functions.h") #inlcude functions that can be used in TTree::Draw

version = "v5/"
useVHBB = False #otherwise use skims
redoEta = False #correct pT, eta, dRmin, eta again
doCSVL = False
saveFitHistos = True
useFitHistos = False


tag = "_data_bML_v5" #_data _ddQCD or _TT_bML ##replace "ave" with "min"

QGLRlo = 0.0
QGLRhi = 1.0

if QGLRhi>0 and (QGLRlo>0 or QGLRhi<1):
    cut3b = "((nBDeepCSVM==3 || (nBDeepCSVM==2 && nBDeepCSVL<4) ) && qg_LR_3b_flavour_5q_0q>{0} && qg_LR_3b_flavour_5q_0q<{1})".format(QGLRlo, QGLRhi)
    cut4b = "((nBDeepCSVM>=4 || (nBDeepCSVM==2 && nBDeepCSVL>=4) ) && qg_LR_4b_flavour_5q_0q>{0} && qg_LR_4b_flavour_5q_0q<{1})".format(QGLRlo, QGLRhi)
else:
    cut3b = ""
    cut4b = ""

if useVHBB:
    jet = "Jet"
else:
    jet = "jets"  

btag_low = 0.1522
btag_high = 0.4941 # Medium for SR/CR/... and ML for SRx/CR --> DeepCSV > 0.255 splits jets for estimation in half
btag_csv_high = 0.8838


btag_max = 1.0
if "bML" in tag:
    btag_high = 0.255
    btag_max = 0.4941


destination = "./"+version+"/plots"
    
if not os.path.exists(destination):
    os.makedirs(destination)
else:
    print "WARNING: directory already exists. Will overwrite existing files..."

if redoEta:
    tag += "_reEta"

ofname = "btagCorrection"+tag+".h"

def getRatioPt(histoHighCSV,histoLowCSV):
    histoHighCSV_pt = histoHighCSV.ProjectionZ()
    histoLowCSV_pt = histoLowCSV.ProjectionZ()
    histoHighCSV_pt.Scale(1/histoHighCSV_pt.Integral())
    histoLowCSV_pt.Scale(1/histoLowCSV_pt.Integral())
    ratio_pt = histoHighCSV_pt.Clone("ratio_pt")
    ratio_pt.Divide(histoHighCSV_pt,histoLowCSV_pt)
    return ratio_pt

def getRatioEta(histoHighCSV,histoLowCSV):
    histoHighCSV_eta = histoHighCSV.ProjectionY()
    histoLowCSV_eta = histoLowCSV.ProjectionY()
    histoHighCSV_eta.Scale(1/histoHighCSV_eta.Integral())
    histoLowCSV_eta.Scale(1/histoLowCSV_eta.Integral())
    ratio_eta = histoHighCSV_eta.Clone("ratio_eta")
    ratio_eta.Divide(histoHighCSV_eta,histoLowCSV_eta)
    return ratio_eta

def getRatioDR(histoHighCSV,histoLowCSV):
    histoHighCSV_dR = histoHighCSV.ProjectionX()
    histoLowCSV_dR = histoLowCSV.ProjectionX()
    histoHighCSV_dR.Scale(1/histoHighCSV_dR.Integral())
    histoLowCSV_dR.Scale(1/histoLowCSV_dR.Integral())
    ratio_dR = histoHighCSV_dR.Clone("ratio_dR")
    ratio_dR.Divide(histoHighCSV_dR,histoLowCSV_dR)
    return ratio_dR


def getDR(histoHighCSV,histoLowCSV, intHigh, intLow):
    histoHighCSV_dR = histoHighCSV.ProjectionX()
    histoLowCSV_dR = histoLowCSV.ProjectionX()
    histoHighCSV_dR.Scale(1/histoHighCSV_dR.Integral())
    histoLowCSV_dR.Scale(1/histoLowCSV_dR.Integral())
    return deepcopy(histoHighCSV_dR), deepcopy(histoLowCSV_dR)

def getEta(histoHighCSV,histoLowCSV, intHigh, intLow):
    histoHighCSV_dR = histoHighCSV.ProjectionY()
    histoLowCSV_dR = histoLowCSV.ProjectionY()
    histoHighCSV_dR.Scale(1/histoHighCSV_dR.Integral())
    histoLowCSV_dR.Scale(1/histoLowCSV_dR.Integral())
    return deepcopy(histoHighCSV_dR), deepcopy(histoLowCSV_dR)

def getPt(histoHighCSV,histoLowCSV, intHigh, intLow):
    histoHighCSV_dR = histoHighCSV.ProjectionZ()
    histoLowCSV_dR = histoLowCSV.ProjectionZ()
    histoHighCSV_dR.Scale(1/histoHighCSV_dR.Integral())
    histoLowCSV_dR.Scale(1/histoLowCSV_dR.Integral())
    return deepcopy(histoHighCSV_dR), deepcopy(histoLowCSV_dR)


basePath = "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/"
datasetData = {
    "data" : basePath+version+"/data.root",
}

datasetTT = {
    "SL" : basePath+version+"bSF/TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root",
    "DL" : basePath+version+"bSF/TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root",
    "FH" : basePath+version+"bSF/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root",
}
trigger = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1)"
luminosity = 41.527540362872
xsec = {
    "SL" : 365.46,
    "DL" : 88.34,
    "FH" : 377.96
}

nGen = {
    "SL" : 85394144.0,
    "DL" : 62658272.0,
    "FH" : 120589264.0
}

print "Getting files and trees"
files = {}
trees = {}
if not useFitHistos:
    for ds in datasetTT:
        print "Loading:",datasetTT[ds]
        files[ds] = ROOT.TFile.Open(datasetTT[ds])
        trees[ds] = files[ds].Get("tree")
    for ds in datasetData:
        print "Loading:",datasetData[ds]
        files[ds] = ROOT.TFile.Open(datasetData[ds])
        trees[ds] = files[ds].Get("tree")

######### vars definition ######
pt_nbins = 70
pt_min  = 30.
pt_max  = 500

eta_nbins = 42*2
eta_min  = -2.4 #was 2.6
eta_max  = +2.4

dR_nbins = 55
dR_min  = 0.5
dR_max  = 4.5#was 5.5

print "Setting fit functions"
#fit_pt = ROOT.TF1("fit_pt","(87.4929-0.000487594*x)*erf((x+303.403)*0.00605046)-86.3071",pt_min,pt_max) #dat
if "TT" in tag:
    ####### v4 #########
    # p0 = -61.8816#0.5#-80
    # p1 = 0.000977138#0.01#0.01
    # p2 = 1164.39#-2-05#200 
    # p3 = 69.9348#1e-08#80
    # p4 = -0.0144098#1#-0.01
    ####################
    ####### v5 #########
    p0 = -67.2219
    p1 = 0.00301669
    p2 = 600
    p3 = 68.6018
    p4 = -0.00151358
    ####################
else:
    #( (87.4929-0.000487594*pt)*math.erf((pt+303.403)*0.00605046)-86.3071)
    #(87.8526-0.00329547*z)*erf((z+536.512)*0.00310985)-85.9452
    ####################
    ####### v4 #########
    p0 = -85.9452#0.5#-80
    p1 = 0.00310985#0.01#0.01
    p2 = 536.512#-2-05#200 
    p3 = 87.8526#1e-08#80
    p4 = -0.000487594#1#-0.01
    ####################
    ####### v5 #########
    p0 = -80#0.5#-80
    p1 = 0.001#0.01#0.01
    p2 = 500#-2-05#200 
    p3 = 80#1e-08#80
    p4 = -0.0005#1#-0.01
    ####################
fit_pt = ROOT.TF1("fit_pt","({3}+{4}*x)*erf((x+{2})*{1})+{0}".format(p0, p1, p2, p3, p4),pt_min,pt_max) #TT
#fit_pt = ROOT.TF1("fit_pt","({0}+{1}*pow(x,1)+{2}*pow(x,2)+{3}*pow(x,3))".format(p0, p1, p2, p3),pt_min,pt_max) #TT
fit_pt.SetLineColor(ROOT.kCyan+1)
fit_pt.SetLineStyle(2)
hfit_pt = ROOT.TH1F("hfit_pt","fit with 0.95 conf.band", 100, pt_min,pt_max)
hfit_pt.SetStats(0)
hfit_pt.SetFillColor(ROOT.kRed-9)

#fit_eta = ROOT.TF1("fit_eta","(1.30305+0.0241787*pow(x,1)-0.548569*pow(x,2)+0.0104078*pow(x,3)+1.18804*pow(x,4)-0.108035*pow(x,5)-1.52058*pow(x,6)+0.104242*pow(x,7)+0.949414*pow(x,8)-0.0440678*pow(x,9)-0.320573*pow(x,10)+0.00956858*pow(x,11)+0.0602872*pow(x,12)-0.00104624*pow(x,13)-0.00595648*pow(x,14)+4.56576e-05*pow(x,15)+0.000241194*pow(x,16))",eta_min,eta_max) #data

if "TT" in tag:
    ####### v4 #########
    # e0 = 1.0
    # e1 = -0.01
    # e2 = -1.0
    # e3 = 0.1
    # e4 = 2.0
    # e5 = -0.2
    # e6 = -2.0
    # e7 = +0.1
    # e8 = +1.0
    # e9 = -0.1
    # e10 = -0.5
    # e11 = 0.01
    # e12 = 0.1
    # e13 = -0.001
    # e14 = -0.01
    # e15 = +1.0e-04
    # e16 = +0.001
    ####################
    ####### v5 #########
    e0 = 1.0
    e1 = 1
    e2 = -1.0
    e3 = 1
    e4 = 1.0
    e5 = 1
    e6 = 1
    e7 = 1
    e8 = +1.0
    e9 = 1
    e10 = 1
    e11 = 1
    e12 = 1
    e13 = 1
    e14 = 1
    e15 = 1
    e16 = 1
    e17 = 1
    e18 = 1
    ####################
else:
    e0 = 1.26013
    e1 = -0.029722
    e2 = -0.0361391
    e3 = 0.346969
    e4 = 0.431347
    e5 = 0.00772567
    e6 = -0.389398
    e7 = -0.0292272
    e8 = 0.199236
    e9 = 0.0179127
    e10 = -0.0632471
    e11 = -0.00502566
    e12 = 0.0122279
    e13 = 0.000681049
    e14 = -0.00130103
    e15 = -3.6027e-05
    e16 = +5.7929e-05

#fit_eta = ROOT.TF1("fit_eta","({0}+{1}*pow(x,1)+{2}*pow(x,2)+{3}*pow(x,3)+{4}*pow(x,4)+{5}*pow(x,5)+{6}*pow(x,6)+{7}*pow(x,7)+{8}*pow(x,8)+{9}*pow(x,9)+{10}*pow(x,10)+{11}*pow(x,11)+{12}*pow(x,12)+{13}*pow(x,13)+{14}*pow(x,14))".format(e0,e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12,e13,e14),eta_min,eta_max) #TT
fit_eta = ROOT.TF1("fit_eta","({0}+{1}*pow(x,1)+{2}*pow(x,2)+{3}*pow(x,3)+{4}*pow(x,4)+{5}*pow(x,5)+{6}*pow(x,6)+{7}*pow(x,7)+{8}*pow(x,8)+{9}*pow(x,9)+{10}*pow(x,10)+{11}*pow(x,11)+{12}*pow(x,12)+{13}*pow(x,13)+{14}*pow(x,14)+{15}*pow(x,15)+{16}*pow(x,16))".format(e0,e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12,e13,e14,e15,e16),eta_min,eta_max) #TT
#fit_eta = ROOT.TF1("fit_eta","({0}+{1}*pow(x,1)+{2}*pow(x,2)+{3}*pow(x,3)+{4}*pow(x,4)+{5}*pow(x,5)+{6}*pow(x,6)+{7}*pow(x,7)+{8}*pow(x,8)+{9}*pow(x,9)+{10}*pow(x,10)+{11}*pow(x,11)+{12}*pow(x,12))".format(e0,e1,e2,e3,e4,e5,e6,e7,e8,e9,e10,e11,e12),eta_min,eta_max) #TT
fit_eta.SetLineColor(ROOT.kCyan+1)
fit_eta.SetLineStyle(2)
hfit_eta = ROOT.TH1F("hfit_eta","fit with 0.95 conf.band", 100, eta_min,eta_max)
hfit_eta.SetStats(0)
hfit_eta.SetFillColor(ROOT.kRed-9)

#fit_dR = ROOT.TF1("funct_dR","(1.51412-0.509854*pow(x,1)+0.0519758*pow(x,2)+0.0868887*pow(x,3)-0.0430129*pow(x,4)+0.00794824*pow(x,5)-0.000532378*pow(x,6))",dR_min,dR_max) #data
r0 = 1.0
r1 = 1.0
r2 = 1.0
r3 = 1.0
r4 = 1.0
r5 = 1.0
r6 = 1.0
fit_dR = ROOT.TF1("funct_dR","({0}+{1}*pow(x,1)+{2}*pow(x,2)+{3}*pow(x,3)+{4}*pow(x,4)+{5}*pow(x,5)+{6}*pow(x,6))".format(r0,r1,r2,r3,r4,r5,r6),dR_min,dR_max) #TT
fit_dR.SetLineColor(ROOT.kCyan+1)
fit_dR.SetLineStyle(2)
hfit_dR = ROOT.TH1F("hfit_dR","fit with 0.95 conf.band", 100, dR_min,dR_max)
hfit_dR.SetStats(0)
hfit_dR.SetFillColor(ROOT.kRed-9)

print "Setting the other fit functions"
#funct_pt = ROOT.TF1("funct_pt","([3]*x+[2]*x*x)*[1]*x+[0]",pt_min,pt_max)
#funct_pt = ROOT.TF1("funct_pt","pol3",pt_min,pt_max)
funct_pt = ROOT.TF1("funct_pt","([3]+[4]*x)*erf((x+[2])*[1])+[0]",pt_min,pt_max)
#funct_pt.SetParameters(-83.0,0.0169,87.5,-86.3,-0.00126)
funct_pt.SetParameters(p0, p1, p2, p3, p4)

funct_eta = ROOT.TF1("funct_eta","pol16",eta_min,eta_max) #was pol20
#funct_eta.SetParameters(1.4,0.06,-0.7,-0.2,1.9,0.3)
funct_eta.SetParameters(e0,e1,e2,e3,e4,e5,e6,e7,e8,e9)

funct_dR = ROOT.TF1("funct_dR","pol6",dR_min,dR_max)
funct_dR.SetParameters(r0, r1,r2,r3,r4,r5,r6)

funct_eta2 = ROOT.TF1("funct_eta2","pol4",eta_min,eta_max)
funct_eta2.SetParameters(1.0,0.0001,-0.001,-0.0002,0.001)

print "Projection....."
########### get 3D histos ########
histoHighCSVTT = ROOT.TH1F()
histoHighCSVdata = ROOT.TH1F()
histoLowCSVTT = ROOT.TH1F()
histoLowCSVdata = ROOT.TH1F()

histoHighCSVTT.Sumw2()
histoHighCSVdata.Sumw2()
histoLowCSVTT.Sumw2()
histoLowCSVdata.Sumw2()
sel = "{0} && ht30>500 && jets_pt[5]>40 && nBDeepCSVM>=2 && (jets_btagDeepCSV < MaxIf$(jets_btagDeepCSV, jets_btagDeepCSV < Max$(jets_btagDeepCSV)))".format(trigger) #was rank>=2
if cut3b:
    sel += "&& ({0} || {1})".format(cut3b, cut4b)
print sel
#if not "4b" in sel:
#    quit() #temp
if not useFitHistos:
    print "Drawing data"
    trees["data"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histo({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),"{0}_btagDeepCSV>{1} && {0}_btagDeepCSV<{2} && {3} ".format(jet,btag_high,btag_max,sel),"GOFF")
    histoHighCSVdata = ROOT.gDirectory.Get("histo").Clone("histoHighCSVdata")
    #histoHighCSVdata.Sumw2() #temp

    print "Drawing TT"
    SLselectionHigh = "({0}_btagDeepCSV>{1} && {0}_btagDeepCSV<{2} && {3})*puWeight*get_TriggerSF3D(ht30, jets_pt[5], nBCSVM)*bTagSF * bTagSF_norm*sign(genWeight)".format(jet,btag_high,btag_max,sel)
    trees["SL"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histoSL({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),SLselectionHigh,"GOFF")
    #*triggerWeight*qgWeight
    histoHighCSVTT_SL = ROOT.gDirectory.Get("histoSL").Clone("histoHighCSVTT_SL")
    DLselectionHigh = "({0}_btagDeepCSV>{1} && {0}_btagDeepCSV<{2} && {3})*puWeight*get_TriggerSF3D(ht30, jets_pt[5], nBCSVM)*bTagSF * bTagSF_norm*sign(genWeight)".format(jet,btag_high,btag_max,sel)
    trees["DL"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histoDL({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),DLselectionHigh,"GOFF")
    #*triggerWeight*qgWeight
    histoHighCSVTT_DL = ROOT.gDirectory.Get("histoDL").Clone("histoHighCSVTT_DL")
    FHselectionHigh = "({0}_btagDeepCSV>{1} && {0}_btagDeepCSV<{2} && {3})*puWeight*get_TriggerSF3D(ht30, jets_pt[5], nBCSVM)*bTagSF * bTagSF_norm*sign(genWeight)".format(jet,btag_high,btag_max,sel)
    trees["FH"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histoFH({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),FHselectionHigh,"GOFF")
    #*triggerWeight*qgWeight
    histoHighCSVTT_FH = ROOT.gDirectory.Get("histoFH").Clone("histoHighCSVTT_FH")

    print FHselectionHigh

    histoHighCSVTT_SL.Scale(xsec["SL"]*luminosity/float(nGen["SL"]))
    histoHighCSVTT_DL.Scale(xsec["DL"]*luminosity/float(nGen["DL"]))
    histoHighCSVTT_FH.Scale(xsec["FH"]*luminosity/float(nGen["FH"]))

    histoHighCSVTT = histoHighCSVTT_SL.Clone("histoHighCSVTT")
    histoHighCSVTT.Add(histoHighCSVTT_DL)
    histoHighCSVTT.Add(histoHighCSVTT_FH)
    print histoHighCSVTT.Integral()
    


if doCSVL:
    if not useFitHistos:
        LRSelection = "{0}_btagCSV>{1} && {0}_btagDeepCSV<{2} && {3}".format(jet,btag_csv_low,btag_high,sel)
        print "LR Selection Data:",LRSelection
        trees["data"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histo({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),LRSelection,"GOFF")
        histoLowCSVdata = ROOT.gDirectory.Get("histo").Clone("histoLowCSVdata")
        histoLowCSVdata.Sumw2() #temp

        selectionLow = "({0}_btagCSV>{1} && {0}_btagDeepCSV<{2} && {3})*puWeight*get_TriggerSF3D(ht30, jets_pt[5], nBCSVM)*bTagSF * bTagSF_norm*sign(genWeight)".format(jet,btag_csv_low,btag_high,sel)
        print "LR selection TT:",selectionLow
        trees["SL"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histoSL({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),selectionLow,"GOFF")
        #*triggerWeight*qgWeight
        histoLowCSVTT_SL = ROOT.gDirectory.Get("histoSL").Clone("histoLowCSVTT_SL")
        trees["DL"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histoDL({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),selectionLow,"GOFF")
        #*triggerWeight*qgWeight
        histoLowCSVTT_DL = ROOT.gDirectory.Get("histoDL").Clone("histoLowCSVTT_DL")
        trees["FH"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histoFH({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),selectionLow,"GOFF")
        #*triggerWeight*qgWeight
        histoLowCSVTT_FH = ROOT.gDirectory.Get("histoFH").Clone("histoLowCSVTT_FH")
else:
    if not useFitHistos:
        trees["data"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histo({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),"{0}_btagDeepCSV>{1} && {0}_btagDeepCSV<{2} && {3}".format(jet,btag_low,btag_high,sel),"GOFF")
        histoLowCSVdata = ROOT.gDirectory.Get("histo").Clone("histoLowCSVdata")
        #histoLowCSVdata.Sumw2() #temp

        SLselectionLow = "({0}_btagDeepCSV>{1} && {0}_btagDeepCSV<{2} && {3})*puWeight*get_TriggerSF3D(ht30, jets_pt[5], nBCSVM)*bTagSF * bTagSF_norm*sign(genWeight)".format(jet,btag_low,btag_high,sel)
        trees["SL"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histoSL({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),SLselectionLow,"GOFF")
        #*triggerWeight*qgWeight
        histoLowCSVTT_SL = ROOT.gDirectory.Get("histoSL").Clone("histoLowCSVTT_SL")
        DLselectionLow = "({0}_btagDeepCSV>{1} && {0}_btagDeepCSV<{2} && {3})*puWeight*get_TriggerSF3D(ht30, jets_pt[5], nBCSVM)*bTagSF * bTagSF_norm*sign(genWeight)".format(jet,btag_low,btag_high,sel)
        trees["DL"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histoDL({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),DLselectionLow,"GOFF")
        #*triggerWeight*qgWeight
        histoLowCSVTT_DL = ROOT.gDirectory.Get("histoDL").Clone("histoLowCSVTT_DL")
        FHselectionLow = "({0}_btagDeepCSV>{1} && {0}_btagDeepCSV<{2} && {3})*puWeight*get_TriggerSF3D(ht30, jets_pt[5], nBCSVM)*bTagSF * bTagSF_norm*sign(genWeight)".format(jet,btag_low,btag_high,sel)
        trees["FH"].Draw("{0}_pt:{0}_eta:{0}_dRmin >> histoFH({1},{2},{3},{4},{5},{6},{7},{8},{9})".format(jet,dR_nbins,dR_min,dR_max,eta_nbins,eta_min,eta_max,pt_nbins,pt_min,pt_max),FHselectionLow,"GOFF")
        #*triggerWeight*qgWeight
        histoLowCSVTT_FH = ROOT.gDirectory.Get("histoFH").Clone("histoLowCSVTT_FH")

if not useFitHistos:
    histoLowCSVTT_SL.Scale(xsec["SL"]*luminosity/float(nGen["SL"]))
    histoLowCSVTT_DL.Scale(xsec["DL"]*luminosity/float(nGen["DL"]))
    histoLowCSVTT_FH.Scale(xsec["FH"]*luminosity/float(nGen["FH"]))
    
    histoLowCSVTT = histoLowCSVTT_SL.Clone("histoLowCSVTT")
    histoLowCSVTT.Add(histoLowCSVTT_DL)
    histoLowCSVTT.Add(histoLowCSVTT_FH)

    
if useFitHistos:
    inputFile =  ROOT.TFile(destination+"fitFunctions.root","OPEN")
    inputFile.cd()

    histoHighCSVTT = inputFile.Get("histoHighCSVTT")
    histoHighCSVdata = inputFile.Get("histoHighCSVdata")
    histoLowCSVTT = inputFile.Get("histoLowCSVTT")
    histoLowCSVdata = inputFile.Get("histoLowCSVdata")

    #inputFile.Close()


if "ddQCD" in tag:
    histoHighCSV = histoHighCSVdata.Clone("histoHighCSV")
    histoHighCSV.Add(histoHighCSVTT,-1)
    histoLowCSV = histoLowCSVdata.Clone("histoLowCSV")
    histoLowCSV.Add(histoLowCSVTT,-1)
elif "TT" in tag:
    histoHighCSV = histoHighCSVTT.Clone("histoHighCSV")
    histoLowCSV = histoLowCSVTT.Clone("histoLowCSV")
elif "data" in tag:
    histoLowCSV = histoLowCSVdata.Clone("histoLowCSV")
    histoHighCSV = histoHighCSVdata.Clone("histoHighCSV")

print "histoHighCSV.Integral():",histoHighCSV.Integral()
HighIntegral = histoHighCSV.Integral()

print "histoLowCSV.Integral():",histoLowCSV.Integral()
LowIntegral = histoLowCSV.Integral()

if saveFitHistos:
    outFile = ROOT.TFile(destination+"fitFunctions.root","RECREATE")
    outFile.cd()

    histoHighCSVTT.Write()
    histoHighCSVdata.Write()

    histoLowCSVTT.Write()
    histoLowCSVdata.Write()
    outFile.Close()
    



#histoLowCSV.Scale(1./histoLowCSV.Integral())
#print "post scale - histoLowCSV.Integral():",histoLowCSV.Integral()

hHighPt, hLowPt = getPt(histoHighCSV,histoLowCSV, HighIntegral, LowIntegral)
hHighEta, hLowEta = getEta(histoHighCSV,histoLowCSV, HighIntegral, LowIntegral)
hHighDR, hLowDR = getDR(histoHighCSV,histoLowCSV, HighIntegral, LowIntegral)

########### get pt ratio and fit pt corrections ########
ratio_pt = getRatioPt(histoHighCSV,histoLowCSV)
ratio_eta = getRatioEta(histoHighCSV,histoLowCSV)
ratio_dR = getRatioDR(histoHighCSV,histoLowCSV)

"""
ratio_pt.SetLineWidth(2)
ratio_pt.Draw()
c53.Update()
raw_input("next")
ratio_eta.Draw()
c53.Update()
raw_input("next")
ratio_dR.Draw()
c53.Update()
raw_input("next")
"""
print ratio_pt.GetSumOfWeights(),ratio_pt.Integral()
if "data" in tag:
    fitRes_pt = ratio_pt.Fit(funct_pt,"WWS")
else:
    fitRes_pt = ratio_pt.Fit(funct_pt,"WWS")

ROOT.TVirtualFitter.GetFitter().GetConfidenceIntervals(hfit_pt)
#errors_pt = fitRes_pt.GetErrors()
#print "errors_pt", errors_pt
print "CovMatrixStatus_pt", fitRes_pt.CovMatrixStatus()
#print "CovMatrix_pt:"
#for i in range(6):
#    for j in range(6):
#        print fitRes_pt.CovMatrix(i,j),
#    print ""
# print "Correlation_pt:"
# for i in range(6):
#     for j in range(6):
#         print fitRes_pt.Correlation(i,j),
#     print ""
print "--------------------------"
########### apply pt corrections to LowCSV ########
raw_input("next fit")
funct3D_pt_str = str(funct_pt.GetExpFormula("P")).replace("x","z") + " + 0*x + 0*y"
funct3D_pt_str = funct3D_pt_str.replace("+-","-")
funct3D_pt = ROOT.TF3("funct3D_pt", funct3D_pt_str, dR_min, dR_max, eta_min, eta_max, pt_min, pt_max)
histoLowCSV_ptCorr = histoLowCSV.Clone("histoLowCSV_ptCorr")
histoLowCSV_ptCorr.Multiply(funct3D_pt,1)

########### get eta ratio and fit eta corrections ########

ratio_pt_ptCorr = getRatioPt(histoHighCSV,histoLowCSV_ptCorr)
ratio_eta_ptCorr = getRatioEta(histoHighCSV,histoLowCSV_ptCorr)
ratio_dR_ptCorr = getRatioDR(histoHighCSV,histoLowCSV_ptCorr)

fitRes_eta = ratio_eta_ptCorr.Fit(funct_eta,"WWS")
ROOT.TVirtualFitter.GetFitter().GetConfidenceIntervals(hfit_eta)
#errors_eta = fitRes_eta.GetErrors()
#print "errors_eta", errors_eta
print "CovMatrixStatus_eta", fitRes_eta.CovMatrixStatus()
# print "Correlation_eta:"
# for i in range(22):
#     for j in range(22):
#         print fitRes_eta.Correlation(i,j),
#     print ""
print "--------------------------"
########### apply pt-eta corrections to LowCSV ########

funct3D_eta_str = "("+str(funct_eta.GetExpFormula("P")).replace("x","y") + " )*( " + str(funct_pt.GetExpFormula("P")).replace("x","z")+")"+ " + 0*x"
funct3D_eta_str = funct3D_eta_str.replace("+-","-")
print funct3D_eta_str
funct3D_eta = ROOT.TF3("funct3D_eta", funct3D_eta_str, dR_min, dR_max, eta_min, eta_max, pt_min, pt_max)
print "00000000000000000000000000000000000000000000000",funct3D_eta.Eval(40, -2, 0)
#print hf.GetBinContent(5)
histoLowCSV_etaCorr = histoLowCSV.Clone("histoLowCSV_etaCorr")
histoLowCSV_etaCorr.Multiply(funct3D_eta,1)

########### get dR ratio and fit dR corrections ########

ratio_pt_etaCorr = getRatioPt(histoHighCSV,histoLowCSV_etaCorr)
ratio_eta_etaCorr = getRatioEta(histoHighCSV,histoLowCSV_etaCorr)
ratio_dR_etaCorr = getRatioDR(histoHighCSV,histoLowCSV_etaCorr)

fitRes_dR = ratio_dR_etaCorr.Fit(funct_dR,"S")
ROOT.TVirtualFitter.GetFitter().GetConfidenceIntervals(hfit_dR)

print "CovMatrixStatus_dR", fitRes_dR.CovMatrixStatus()
print "--------------------------"
########### apply pt-eta-dR corrections to LowCSV ########

funct3D_dR_str = "("+str(funct_dR.GetExpFormula("P")) + " )*("+str(funct_eta.GetExpFormula("P")).replace("x","y") + " )*( " + str(funct_pt.GetExpFormula("P")).replace("x","z")+")"
funct3D_dR_str = funct3D_dR_str.replace("+-","-")
funct3D_dR = ROOT.TF3("funct3D_dR", funct3D_dR_str, dR_min, dR_max, eta_min, eta_max, pt_min, pt_max)
histoLowCSV_dRCorr = histoLowCSV.Clone("histoLowCSV_dRCorr")
histoLowCSV_dRCorr.Multiply(funct3D_dR,1)

########### get final (pt,eta,dR) ratios ########

ratio_pt_dRCorr = getRatioPt(histoHighCSV,histoLowCSV_dRCorr)
ratio_eta_dRCorr = getRatioEta(histoHighCSV,histoLowCSV_dRCorr)
ratio_dR_dRCorr = getRatioDR(histoHighCSV,histoLowCSV_dRCorr)

if redoEta:
    ########### re-fit eta corrections ########
    fitRes_eta2 = ratio_eta_dRCorr.Fit(funct_eta2,"S")

    ########### reapply eta corrections to LowCSV ########
    funct3D_eta2_str = "("+str(funct_dR.GetExpFormula("P")) + " )*("+str(funct_eta.GetExpFormula("P")).replace("x","y") + " )*("+str(funct_eta2.GetExpFormula("P")).replace("x","y") + " )*( " + str(funct_pt.GetExpFormula("P")).replace("x","z")+")"
    funct3D_eta2 = ROOT.TF3("funct3D_eta2", funct3D_eta2_str, dR_min, dR_max, eta_min, eta_max, pt_min, pt_max)
    histoLowCSV_eta2Corr = histoLowCSV.Clone("histoLowCSV_eta2Corr")
    histoLowCSV_eta2Corr.Multiply(funct3D_eta2,1)

    ########### get final (pt,eta,dR) ratios ########
    ratio_pt_eta2Corr = getRatioPt(histoHighCSV,histoLowCSV_eta2Corr)
    ratio_eta_eta2Corr = getRatioEta(histoHighCSV,histoLowCSV_eta2Corr)
    ratio_dR_eta2Corr = getRatioDR(histoHighCSV,histoLowCSV_eta2Corr)

################### Draw  checks ##############
ratio_pt.SetLineColor(ROOT.kBlack)
ratio_pt_ptCorr.SetLineColor(ROOT.kBlue)
ratio_pt_etaCorr.SetLineColor(ROOT.kRed)
ratio_pt_dRCorr.SetLineColor(ROOT.kGreen+1)

ratio_eta.SetLineColor(ROOT.kBlack)
ratio_eta_ptCorr.SetLineColor(ROOT.kBlue)
ratio_eta_etaCorr.SetLineColor(ROOT.kRed)
ratio_eta_dRCorr.SetLineColor(ROOT.kGreen+1)

ratio_dR.SetLineColor(ROOT.kBlack)
ratio_dR_ptCorr.SetLineColor(ROOT.kBlue)
ratio_dR_etaCorr.SetLineColor(ROOT.kRed)
ratio_dR_dRCorr.SetLineColor(ROOT.kGreen+1)

if redoEta:
    ratio_pt_eta2Corr.SetLineColor(ROOT.kMagenta)
    ratio_eta_eta2Corr.SetLineColor(ROOT.kMagenta)
    ratio_dR_eta2Corr.SetLineColor(ROOT.kMagenta)

leg = ROOT.TLegend(0.4,0.2,0.6,0.5)
leg.SetFillStyle(0)
leg.SetBorderSize(0)
leg.SetTextSize(0.03)

leg.AddEntry(ratio_pt, "uncorrected", "ple")
leg.AddEntry(ratio_pt_ptCorr, "pt corrected", "ple")
leg.AddEntry(ratio_pt_etaCorr, "pt+eta corrected", "ple")
leg.AddEntry(ratio_pt_dRCorr, "pt+eta+dR corrected", "ple")
if redoEta:
    leg.AddEntry(ratio_pt_eta2Corr, "re-eta corrected", "ple")  
leg.AddEntry(funct_pt,"fit function","l")
#leg.AddEntry(fit_pt,"full range fit","l")

c1 = ROOT.TCanvas("c1","",5,30,640,580)

if doCSVL:
    ratio_pt.SetTitle("jetsDeepCSVM/jetsCSVL;p_{T} (GeV);ratio")
else:
    ratio_pt.SetTitle("jetsDeepCSVM/jetsDeepCSVL;p_{T} (GeV);ratio")
ratio_pt.Draw("pe3")
hfit_pt.Draw("e3 same")
ratio_pt.Draw("same")
ratio_pt_ptCorr.Draw("same")
ratio_pt_etaCorr.Draw("same")
ratio_pt_dRCorr.Draw("same")
if redoEta:
    ratio_pt_eta2Corr.Draw("same")
funct_pt.Draw("same")
#fit_pt.Draw("same")
leg.Draw()

c1.SaveAs(destination+"/ratio_pt"+tag+".pdf")

c2 = ROOT.TCanvas("c2","",5,30,640,580)
if doCSVL:
    ratio_eta.SetTitle("jetsDeepCSVM/jetsCSVL;p_{T} (GeV);ratio")
else:
    ratio_eta.SetTitle("jetsDeepCSVM/jetsDeepCSVL;#eta;ratio")
ratio_eta.Draw("")
hfit_eta.Draw("e3 same")
ratio_eta.Draw("same")
ratio_eta_ptCorr.Draw("same")
ratio_eta_etaCorr.Draw("same")
ratio_eta_dRCorr.Draw("same")
if redoEta:
    ratio_eta_eta2Corr.Draw("same")
funct_eta.Draw("same")
#fit_eta.Draw("same")
leg.Draw()

c2.SaveAs(destination+"/ratio_eta"+tag+".pdf")

c20 = ROOT.TCanvas("c20","",5,30,640,580)

if "TT" in tag:
    ratio_dR.GetYaxis().SetRangeUser(0,1.5)
if doCSVL:
    ratio_dR.SetTitle("jetsDeepCSVM/jetsCSVL;p_{T} (GeV);ratio")
else:
    ratio_dR.SetTitle("jetsDeepCSVM/jetsDeepCSVL;#DeltaR_{min};ratio")
ratio_dR.Draw("")
hfit_dR.Draw("e3 same")
ratio_dR.Draw("same")
ratio_dR_ptCorr.Draw("same")
ratio_dR_etaCorr.Draw("same")
ratio_dR_dRCorr.Draw("same")
if redoEta:
    ratio_dR_eta2Corr.Draw("same")
funct_dR.Draw("same")
#fit_dR.Draw("same")
leg.Draw()

c20.SaveAs(destination+"/ratio_dR"+tag+".pdf")

c3 = ROOT.TCanvas("c3","",5,30,640,580)
ratio_3D_before = histoLowCSV.Clone("ratio_3D_before")
ratio_3D_before.Divide(histoHighCSV,histoLowCSV)

ratio_2D_PtEta_high = histoHighCSV.Project3D("zy") #Pt verticle
ratio_2D_PtEta_high.Scale(1.0/ratio_2D_PtEta_high.Integral())
ratio_2D_PtEta_low_before = histoLowCSV.Project3D("zy")
ratio_2D_PtEta_low_before.Scale(1.0/ratio_2D_PtEta_low_before.Integral())

ratio_2D_PtEta_before = ratio_2D_PtEta_high.Clone("ratio_2D_PtEta_before")
ratio_2D_PtEta_before.Divide(ratio_2D_PtEta_low_before)
ratio_2D_PtEta_before.SetMaximum(1.5)
ratio_2D_PtEta_before.SetMinimum(0.5)
ratio_2D_PtEta_before.SetTitle("jetsDeepCSVM/jetsDeepCSVL before;#eta;p_{T} (GeV)")
ratio_2D_PtEta_before.Draw("COLZ")
c3.SaveAs(destination+"/ratio_2D_PtEta_before"+tag+".pdf")

c30 = ROOT.TCanvas("c30","",5,30,640,580)
ratio_2D_DrEta_high = histoHighCSV.Project3D("xy") #DRmin verticle
ratio_2D_DrEta_high.Scale(1.0/ratio_2D_DrEta_high.Integral())
ratio_2D_DrEta_low_before = histoLowCSV.Project3D("xy")
ratio_2D_DrEta_low_before.Scale(1.0/ratio_2D_DrEta_low_before.Integral())

ratio_2D_DrEta_before = ratio_2D_DrEta_high.Clone("ratio_2D_DrEta_before")
ratio_2D_DrEta_before.Divide(ratio_2D_DrEta_low_before)
ratio_2D_DrEta_before.SetMaximum(1.5)
ratio_2D_DrEta_before.SetMinimum(0.5)
ratio_2D_DrEta_before.SetTitle("jetsDeepCSVM/jetsDeepCSVL before;#eta;#DeltaR_{min}")
ratio_2D_DrEta_before.Draw("COLZ")
c30.SaveAs(destination+"/ratio_2D_DrEta_before"+tag+".pdf")

c31 = ROOT.TCanvas("c31","",5,30,640,580)
ratio_2D_PtDr_high = histoHighCSV.Project3D("zx") #Pt verticle
ratio_2D_PtDr_high.Scale(1.0/ratio_2D_PtDr_high.Integral())
ratio_2D_PtDr_low_before = histoLowCSV.Project3D("zx")
ratio_2D_PtDr_low_before.Scale(1.0/ratio_2D_PtDr_low_before.Integral())

ratio_2D_PtDr_before = ratio_2D_PtDr_high.Clone("ratio_2D_PtDr_before")
ratio_2D_PtDr_before.Divide(ratio_2D_PtDr_low_before)
ratio_2D_PtDr_before.SetMaximum(1.5)
ratio_2D_PtDr_before.SetMinimum(0.5)
ratio_2D_PtDr_before.SetTitle("jetsDeepCSVM/jetsDeepCSVL before;#DeltaR_{min};p_{T} (GeV)")
ratio_2D_PtDr_before.Draw("COLZ")
c31.SaveAs(destination+"/ratio_2D_PtDr_before"+tag+".pdf")

c4 = ROOT.TCanvas("c4","",5,30,640,580)
if redoEta:
    ratio_3D_after = histoLowCSV_eta2Corr.Clone("ratio_3D_after")
    ratio_3D_after.Divide(histoHighCSV,histoLowCSV_eta2Corr)    
else:
    ratio_3D_after = histoLowCSV_dRCorr.Clone("ratio_3D_after")
    ratio_3D_after.Divide(histoHighCSV,histoLowCSV_dRCorr)

if redoEta:
    ratio_2D_PtEta_low_after = histoLowCSV_eta2Corr.Project3D("zy") #Pt verticle
else:
    ratio_2D_PtEta_low_after = histoLowCSV_dRCorr.Project3D("zy") #Pt verticle
ratio_2D_PtEta_low_after.Scale(1.0/ratio_2D_PtEta_low_after.Integral())

ratio_2D_PtEta_after = ratio_2D_PtEta_high.Clone("ratio_2D_PtEta_after")
ratio_2D_PtEta_after.Divide(ratio_2D_PtEta_low_after)
ratio_2D_PtEta_after.SetMaximum(1.5)
ratio_2D_PtEta_after.SetMinimum(0.5)
ratio_2D_PtEta_after.SetTitle("jetsDeepCSVM/jetsDeepCSVL after;#eta;p_{T} (GeV)")
ratio_2D_PtEta_after.Draw("COLZ")
c4.SaveAs(destination+"/ratio_2D_PtEta_after"+tag+".pdf")

c40 = ROOT.TCanvas("c40","",5,30,640,580)
if redoEta:
    ratio_2D_DrEta_low_after = histoLowCSV_eta2Corr.Project3D("xy") #DRmin verticle
else:
    ratio_2D_DrEta_low_after = histoLowCSV_dRCorr.Project3D("xy") #DRmin verticle
ratio_2D_DrEta_low_after.Scale(1.0/ratio_2D_DrEta_low_after.Integral())

ratio_2D_DrEta_after = ratio_2D_DrEta_high.Clone("ratio_2D_DrEta_after")
ratio_2D_DrEta_after.Divide(ratio_2D_DrEta_low_after)
ratio_2D_DrEta_after.SetMaximum(1.5)
ratio_2D_DrEta_after.SetMinimum(0.5)
ratio_2D_DrEta_after.SetTitle("jetsDeepCSVM/jetsDeepCSVL after;#eta;#DeltaR_{min}")
ratio_2D_DrEta_after.Draw("COLZ")
c40.SaveAs(destination+"/ratio_2D_DrEta_after"+tag+".pdf")

c41 = ROOT.TCanvas("c41","",5,30,640,580)
if redoEta:
    ratio_2D_PtDr_low_after = histoLowCSV_eta2Corr.Project3D("zx") #Pt verticle
else:
    ratio_2D_PtDr_low_after = histoLowCSV_dRCorr.Project3D("zx") #Pt verticle
ratio_2D_PtDr_low_after.Scale(1.0/ratio_2D_PtDr_low_after.Integral())

ratio_2D_PtDr_after = ratio_2D_PtDr_high.Clone("ratio_2D_PtDr_after")
ratio_2D_PtDr_after.Divide(ratio_2D_PtDr_low_after)
ratio_2D_PtDr_after.SetMaximum(1.5)
ratio_2D_PtDr_after.SetMinimum(0.5)
ratio_2D_PtDr_after.SetTitle("jetsDeepCSVM/jetsDeepCSVL after;#DeltaR_{min};p_{T} (GeV)")
ratio_2D_PtDr_after.Draw("COLZ")
c41.SaveAs(destination+"/ratio_2D_PtDr_after"+tag+".pdf")

dump, hLowPt_ptcorr = getPt(histoHighCSV,histoLowCSV_ptCorr, 1, 1)
dump, hLowEta_ptcorr = getEta(histoHighCSV,histoLowCSV_ptCorr, 1, 1)
dump, hLowDR_ptcorr = getDR(histoHighCSV,histoLowCSV_ptCorr, 1, 1)
dump, hLowPt_etacorr = getPt(histoHighCSV,histoLowCSV_etaCorr, 1, 1)
dump, hLowEta_etacorr = getEta(histoHighCSV,histoLowCSV_etaCorr, 1, 1)
dump, hLowDR_etacorr = getDR(histoHighCSV,histoLowCSV_etaCorr, 1, 1)
dump, hLowPt_dRcorr = getPt(histoHighCSV,histoLowCSV_dRCorr, 1, 1)
dump, hLowEta_dRcorr = getEta(histoHighCSV,histoLowCSV_dRCorr, 1, 1)
dump, hLowDR_dRcorr = getDR(histoHighCSV,histoLowCSV_dRCorr, 1, 1)


c51 = ROOT.TCanvas("c51","",5,30,640,580)
leg = ROOT.TLegend(0.7,0.7,0.9,0.86)
leg.SetFillStyle(0)
leg.SetBorderSize(0)
leg.SetTextSize(0.03)
hHighPt.SetLineColor(ROOT.kRed)
hLowPt_ptcorr.SetLineColor(ROOT.kGreen-2)
hLowPt_etacorr.SetLineColor(ROOT.kCyan)
hLowPt_dRcorr.SetLineColor(ROOT.kViolet) 
leg.AddEntry(hHighPt, "High area", "ple")
leg.AddEntry(hLowPt, "Low area", "ple")
leg.AddEntry(hLowPt_ptcorr, "Low area (pt corr)", "ple")
leg.AddEntry(hLowPt_etacorr, "Low area (eta corr", "ple")
leg.AddEntry(hLowPt_dRcorr, "Low area (dR Corr)", "ple")
hHighPt.GetXaxis().SetTitle("#p_{T}")
hHighPt.Draw()
hLowPt.Draw("same")
hLowPt_ptcorr.Draw("same")
hLowPt_etacorr.Draw("same")
hLowPt_dRcorr.Draw("same")
leg.Draw("same")
c51.SaveAs(destination+"/Pt_comp"+tag+".pdf")
c52 = ROOT.TCanvas("c52","",5,30,640,580)
hHighEta.SetLineColor(ROOT.kRed)
hLowEta_ptcorr.SetLineColor(ROOT.kGreen-2)
hLowEta_etacorr.SetLineColor(ROOT.kCyan)
hLowEta_dRcorr.SetLineColor(ROOT.kViolet) 
hHighEta.GetXaxis().SetTitle("#eta")
hHighEta.Draw()
hLowEta.Draw("same")
hLowEta_ptcorr.Draw("same")
hLowEta_etacorr.Draw("same")
hLowEta_dRcorr.Draw("same")
leg.Draw("same")
c52.SaveAs(destination+"/Eta_comp"+tag+".pdf")
c53 = ROOT.TCanvas("c53","",5,30,640,580)
hHighDR.SetLineColor(ROOT.kRed)
hLowDR_ptcorr.SetLineColor(ROOT.kGreen-2)
hLowDR_etacorr.SetLineColor(ROOT.kCyan)
hLowDR_dRcorr.SetLineColor(ROOT.kViolet)
hHighDR.GetXaxis().SetTitle("#Delta R")
hHighDR.Draw()
hLowDR.Draw("same")
hLowDR_ptcorr.Draw("same")
hLowDR_etacorr.Draw("same")
hLowDR_dRcorr.Draw("same")
leg.Draw("same")
c53.SaveAs(destination+"/DR_comp"+tag+".pdf")

for bin_ in range(hHighEta.GetNbinsX()):
    print bin_, hHighEta.GetBinContent(bin_), hLowEta_etacorr.GetBinContent(bin_), hHighEta.GetBinContent(bin_)/hLowEta_etacorr.GetBinContent(bin_) if hLowEta_etacorr.GetBinContent(bin_) > 0 else -1

print
print funct3D_pt_str
print 
print funct3D_eta_str
print 
print funct3D_dR_str
print
if redoEta:
    print funct3D_eta2_str
    print    

################### make .h file with weights ##############

funct3D_final_str = funct3D_eta2_str if redoEta else funct3D_dR_str

funct3D_final_str = funct3D_final_str.replace("--","+")
funct3D_final_str = funct3D_final_str.replace("++","+")
funct3D_final_str = funct3D_final_str.replace("-+","-")
funct3D_final_str = funct3D_final_str.replace("+-","-")
funct3D_final_str = funct3D_final_str.replace("x","minDeltaRb")
funct3D_final_str = funct3D_final_str.replace("y","eta")
funct3D_final_str = funct3D_final_str.replace("z","pt")
#funct3D_final_str = funct3D_final_str.replace("abs(eta)","eta")

txt = '''
#include<algorithm>

float btagCorrection(float pt, float eta, float minDeltaRb, float csv){
    if(csv<%f || csv>%f) return 1;
    else return %s;
}
'''%(btag_low,btag_high,funct3D_final_str)

outputFile = file(version+"/"+ofname,"w")
outputFile.write(txt)
outputFile.close()




# c6 = ROOT.TCanvas("c6")
# ratio_pt.Draw()
#fitResults = ratio_pt.Fit(funct_pt,"ESV0")
# #res = fitResults.Get()
# matr = fitRes_pt.GetCovarianceMatrix()
# #matr.Print()
# funct_pt.Draw("same")

# funct_pt_err = []
# eigenvalues = getattr(ROOT,"TVectorT<double>")()
# eigenvectors = matr.EigenVectors(eigenvalues)

# for i in range(funct_pt.GetNpar()):
#     funct_pt_err.append(funct_pt.Clone("funct_pt_err"+str(i)))
#     delta=[0.]*funct_pt.GetNpar()
#     for j in range(funct_pt.GetNpar()):
#         centralValue = funct_pt.GetParameter(j)
#         delta = eigenvectors[j][i] * (eigenvalues[i])**0.5 
#         funct_pt_err[i].SetParameter(j, centralValue + delta*2)
#         #print eigenvectors[j][i],"\t",eigenvalues[i],"\t",delta,"\t",centralValue,"\t",centralValue+delta
#     funct_pt_err[i].SetLineColor(1+i)
#     funct_pt_err[i].Draw("same")

# c6.SaveAs(destination+"/pt_errors"+tag+".pdf")

# c7 = ROOT.TCanvas("c7")
# ratio_eta.Draw()
# #fitResults = ratio_eta.Fit(funct_eta,"ES")
# #res = fitResults.Get()
# matr = fitRes_eta.GetCovarianceMatrix()
# #matr.Print()
# funct_eta.Draw("same")

# funct_eta_err = []
# eigenvalues = getattr(ROOT,"TVectorT<double>")()
# eigenvectors = matr.EigenVectors(eigenvalues)

# for i in range(funct_eta.GetNpar()):
#     funct_eta_err.append(funct_eta.Clone("funct_eta_err"+str(i)))
#     delta=[0.]*funct_eta.GetNpar()
#     for j in range(funct_eta.GetNpar()):
#         centralValue = funct_eta.GetParameter(j)
#         delta = eigenvectors[j][i] * (eigenvalues[i])**0.5
#         funct_eta_err[i].SetParameter(j, centralValue + delta*2)
#         #print eigenvectors[j][i],"\t",eigenvalues[i],"\t",delta,"\t",centralValue,"\t",centralValue+delta
#     funct_eta_err[i].SetLineColor(1+i)
#     funct_eta_err[i].Draw("same")

# c7.SaveAs(destination+"/eta_errors"+tag+".pdf")

raw_input("press Enter to quit")
