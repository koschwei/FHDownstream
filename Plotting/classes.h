#include "TFile.h"
#include "TH3F.h"

class TriggerSF3D {
 public:
  TriggerSF3D( );
  ~TriggerSF3D( );

  float getTriggerWeight( float ht, float jet_pt, int nB );
  
 private:
  TH3F *h_bin_0;
};


  //PUBLIC
  TriggerSF3D::TriggerSF3D( ){

    const char * histoInputFile = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p1/CMSSW_9_4_9/src/TTH/FHDownstream/Plotting/TriggerPlots_ttH_AH_v5/sf3d_out_3DSF_JEC32_looseCut_All_CSV.root";
    //const char * histoInputFile = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/CMSSW_9_4_9/src/TTH/FHDownstream/Plotting/TriggerPlots_ttH_AH_newSkim_v2p1/sf3d_out_3DSF_Final_looseCut_All_CSV_updated.root";
    //const char * histoInputFile = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/CMSSW_9_4_9/src/TTH/Plotting/Daniel/TriggerPlots_ttH_AH_v2/sf3d_out_3DSF_FullDataset_tightCut_wPuGen_ht30.root";
    TFile *sfFile = new TFile(histoInputFile, "READ");

    //h_bin_0 = (TH3F*)sfFile->Get("h3SF_tot_mod");
    h_bin_0 = (TH3F*)sfFile->Get("h3SF_tot");
    
  }

  TriggerSF3D::~TriggerSF3D( ){

  }

  float TriggerSF3D::getTriggerWeight( float ht, float jet_pt,  int nB){

    float SF = 1;
    //if (nB == 0){
    //  nB = 1;
    //}
    int bin = h_bin_0->FindBin(ht, jet_pt, nB);
    SF = h_bin_0->GetBinContent(bin);

    if (SF == 0){
      SF = 1;
    }
    return SF;
  }


class bTagNormalization {
 public:
  bTagNormalization( );
  ~bTagNormalization( );

  float getbTagNormalization( string Sample, int nJets );

 private:
  TH1F *TH1F_QCD300;
  TH1F *TH1F_QCD500;
  TH1F *TH1F_QCD700;
  TH1F *TH1F_QCD1000;
  TH1F *TH1F_QCD1500;
  TH1F *TH1F_QCD2000;
 
  TH1F *TH1F_st_s;
  TH1F *TH1F_st_t;
  TH1F *TH1F_st_tW;
  TH1F *TH1F_stbar_t;
  TH1F *TH1F_stbar_tW;

  TH1F *TH1F_tt_SL;
  TH1F *TH1F_tt_DL;
  TH1F *TH1F_tt_FH;
  
  TH1F *TH1F_ttW;
  TH1F *TH1F_ttZ;

  TH1F *TH1F_WJets_400;
  TH1F *TH1F_WJets_600;
  TH1F *TH1F_WJets_800;
  TH1F *TH1F_ZJets_400;
  TH1F *TH1F_ZJets_600;
  TH1F *TH1F_ZJets_800;

  TH1F *TH1F_ZZ;
  TH1F *TH1F_WZ;
  TH1F *TH1F_WW;

  TH1F *TH1F_ttHbb;
  TH1F *TH1F_ttHnonbb;
};


//PUBLIC
bTagNormalization::bTagNormalization(){

  const char * hIn_QCD1000 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_QCD_HT1000to1500_TuneCP5_13TeV-madgraph-pythia8.root";
  const char * hIn_QCD1500 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_QCD_HT1500to2000_TuneCP5_13TeV-madgraph-pythia8.root";
  const char * hIn_QCD2000 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_QCD_HT2000toInf_TuneCP5_13TeV-madgraph-pythia8.root";
  const char * hIn_QCD300 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_QCD_HT300to500_TuneCP5_13TeV-madgraph-pythia8.root";
  const char * hIn_QCD500 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_QCD_HT500to700_TuneCP5_13TeV-madgraph-pythia8.root";
  const char * hIn_QCD700 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_QCD_HT700to1000_TuneCP5_13TeV-madgraph-pythia8.root";
  const char * hIn_st_s = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_ST_s-channel_4f_leptonDecays_TuneCP5_PSweights_13TeV-amcatnlo-pythia8.root";
  const char * hIn_stbar_t = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_ST_t-channel_antitop_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8.root";
  const char * hIn_st_t = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_ST_t-channel_top_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8.root";
  const char * hIn_stbar_tW = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_ST_tW_antitop_5f_inclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8.root";
  const char * hIn_st_tW = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_ST_tW_top_5f_inclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8.root";
  const char * hIn_tt_DL = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root";
  const char * hIn_tt_FH = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root";
  const char * hIn_tt_SL = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root";
  const char * hIn_ttW = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8.root";
  const char * hIn_ttZ = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8.root";
  const char * hIn_WJets_800 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_WJetsToQQ_HT-800toInf_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8.root";
  const char * hIn_WJets_400 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_WJetsToQQ_HT400to600_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8.root";
  const char * hIn_WJets_600 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_WJetsToQQ_HT600to800_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8.root";
  const char * hIn_WW = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_WW_TuneCP5_13TeV-pythia8.root";
  const char * hIn_WZ = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_WZ_TuneCP5_13TeV-pythia8.root";
  const char * hIn_ZJets_800 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_ZJetsToQQ_HT-800toInf_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8.root";
  const char * hIn_ZJets_400 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_ZJetsToQQ_HT400to600_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8.root";
  const char * hIn_ZJets_600 = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_ZJetsToQQ_HT600to800_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8.root";
  const char * hIn_ZZ = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_ZZ_TuneCP5_13TeV-pythia8.root";
  const char * hIn_ttHnonbb = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8.root";
  const char * hIn_ttHbb = "/mnt/t3nfs01/data01/shome/koschwei/tth/Plotting/HEPPlotster/data/bTagNorm/bTagNormWeights_ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8.root";

  TFile *sfFile_QCD300 = new TFile(hIn_QCD1000, "READ");
  TFile *sfFile_QCD500 = new TFile(hIn_QCD1500, "READ");
  TFile *sfFile_QCD700 = new TFile(hIn_QCD2000, "READ");
  TFile *sfFile_QCD1000 = new TFile(hIn_QCD300 , "READ");
  TFile *sfFile_QCD1500 = new TFile(hIn_QCD500 , "READ");
  TFile *sfFile_QCD2000 = new TFile(hIn_QCD700 , "READ");
  TFile *sfFile_st_s = new TFile(hIn_st_s, "READ");
  TFile *sfFile_st_t = new TFile(hIn_stbar_t, "READ");
  TFile *sfFile_st_tW = new TFile(hIn_st_t, "READ");
  TFile *sfFile_stbar_t = new TFile(hIn_stbar_tW , "READ");
  TFile *sfFile_stbar_tW = new TFile(hIn_st_tW, "READ");
  TFile *sfFile_tt_SL = new TFile(hIn_tt_DL, "READ");
  TFile *sfFile_tt_DL = new TFile(hIn_tt_FH, "READ");
  TFile *sfFile_tt_FH = new TFile(hIn_tt_SL, "READ");
  TFile *sfFile_ttW = new TFile(hIn_ttW, "READ");
  TFile *sfFile_ttZ = new TFile(hIn_ttZ, "READ");
  TFile *sfFile_WJets_400 = new TFile(hIn_WJets_800, "READ");
  TFile *sfFile_WJets_600 = new TFile(hIn_WJets_400, "READ");
  TFile *sfFile_WJets_800 = new TFile(hIn_WJets_600, "READ");
  TFile *sfFile_ZJets_400 = new TFile(hIn_WW, "READ");
  TFile *sfFile_ZJets_600 = new TFile(hIn_WZ, "READ");
  TFile *sfFile_ZJets_800 = new TFile(hIn_ZJets_800, "READ");
  TFile *sfFile_ZZ = new TFile(hIn_ZJets_400, "READ");
  TFile *sfFile_WZ = new TFile(hIn_ZJets_600, "READ");
  TFile *sfFile_WW = new TFile(hIn_ZZ, "READ");
  TFile *sfFile_ttHbb = new TFile(hIn_ttHnonbb , "READ");
  TFile *sfFile_ttHnonbb = new TFile(hIn_ttHbb, "READ");

  TH1F_QCD300  = (TH1F*)sfFile_QCD300->Get("hWeight_btagWeight_shape");
  TH1F_QCD500  = (TH1F*)sfFile_QCD500->Get("hWeight_btagWeight_shape");
  TH1F_QCD700  = (TH1F*)sfFile_QCD700->Get("hWeight_btagWeight_shape");
  TH1F_QCD1000  = (TH1F*)sfFile_QCD1000->Get("hWeight_btagWeight_shape");
  TH1F_QCD1500  = (TH1F*)sfFile_QCD1500->Get("hWeight_btagWeight_shape");
  TH1F_QCD2000  = (TH1F*)sfFile_QCD2000->Get("hWeight_btagWeight_shape");
  TH1F_st_s  = (TH1F*)sfFile_st_s->Get("hWeight_btagWeight_shape");
  TH1F_st_t  = (TH1F*)sfFile_st_t->Get("hWeight_btagWeight_shape");
  TH1F_st_tW  = (TH1F*)sfFile_st_tW->Get("hWeight_btagWeight_shape");
  TH1F_stbar_t = (TH1F*)sfFile_stbar_t->Get("hWeight_btagWeight_shape");
  TH1F_stbar_tW  = (TH1F*)sfFile_stbar_tW->Get("hWeight_btagWeight_shape");
  TH1F_tt_SL  = (TH1F*)sfFile_tt_SL->Get("hWeight_btagWeight_shape");
  TH1F_tt_DL  = (TH1F*)sfFile_tt_DL->Get("hWeight_btagWeight_shape");
  TH1F_tt_FH  = (TH1F*)sfFile_tt_FH->Get("hWeight_btagWeight_shape");
  TH1F_ttW  = (TH1F*)sfFile_ttW->Get("hWeight_btagWeight_shape");
  TH1F_ttZ  = (TH1F*)sfFile_ttZ->Get("hWeight_btagWeight_shape");
  TH1F_WJets_400  = (TH1F*)sfFile_WJets_400->Get("hWeight_btagWeight_shape");
  TH1F_WJets_600  = (TH1F*)sfFile_WJets_600->Get("hWeight_btagWeight_shape");
  TH1F_WJets_800  = (TH1F*)sfFile_WJets_800->Get("hWeight_btagWeight_shape");
  TH1F_ZJets_400  = (TH1F*)sfFile_ZJets_400->Get("hWeight_btagWeight_shape");
  TH1F_ZJets_600  = (TH1F*)sfFile_ZJets_600->Get("hWeight_btagWeight_shape");
  TH1F_ZJets_800  = (TH1F*)sfFile_ZJets_800->Get("hWeight_btagWeight_shape");
  TH1F_ZZ  = (TH1F*)sfFile_ZZ->Get("hWeight_btagWeight_shape");
  TH1F_WZ  = (TH1F*)sfFile_WZ->Get("hWeight_btagWeight_shape");
  TH1F_WW  = (TH1F*)sfFile_WW->Get("hWeight_btagWeight_shape");
  TH1F_ttHbb  = (TH1F*)sfFile_ttHbb->Get("hWeight_btagWeight_shape");
  TH1F_ttHnonbb  = (TH1F*)sfFile_ttHnonbb->Get("hWeight_btagWeight_shape");

  /*
  sfFile_QCD300->Close();
  sfFile_QCD500->Close();
  sfFile_QCD700->Close() ;
  sfFile_QCD1000->Close();
  sfFile_QCD1500->Close();
  sfFile_QCD2000->Close();
  sfFile_st_s->Close();
  sfFile_st_t->Close();
  sfFile_st_tW->Close();
  sfFile_stbar_t->Close();
  sfFile_stbar_tW->Close();
  sfFile_tt_SL->Close();
  sfFile_tt_DL->Close();
  sfFile_tt_FH->Close();
  sfFile_ttW->Close();
  sfFile_ttZ->Close();
  sfFile_WJets_400->Close();
  sfFile_WJets_600->Close();
  sfFile_WJets_800->Close();
  sfFile_ZJets_400->Close();
  sfFile_ZJets_600->Close();
  sfFile_ZJets_800->Close();
  sfFile_ZZ->Close();
  sfFile_WZ->Close();
  sfFile_WW->Close();
  sfFile_ttHbb->Close();
  sfFile_ttHnonbb->Close();
  */
}

bTagNormalization::~bTagNormalization(){ };

float bTagNormalization::getbTagNormalization( string Sample, int nJets ){

  int bin = -1;
  float SF = -1;
  if (Sample == "QCD300"){
    SF= TH1F_QCD300->GetBinContent(TH1F_QCD300->FindBin(nJets));
  }
  else if (Sample == "QCD500"){
    SF= TH1F_QCD500->GetBinContent(TH1F_QCD500->FindBin(nJets));
  }
  else if (Sample == "QCD700"){
    SF= TH1F_QCD700->GetBinContent(TH1F_QCD700->FindBin(nJets));
  }
  else if (Sample == "QCD1000"){  
    SF= TH1F_QCD1000->GetBinContent(TH1F_QCD1000->FindBin(nJets));
  }
  else if (Sample == "QCD1500"){
    SF= TH1F_QCD1500->GetBinContent(TH1F_QCD1500->FindBin(nJets));
  }
  else if (Sample == "QCD2000"){
    SF= TH1F_QCD2000->GetBinContent(TH1F_QCD2000->FindBin(nJets));
  }
  else if (Sample == "st_s"){
    SF= TH1F_st_s->GetBinContent(TH1F_st_s->FindBin(nJets));
  }
  else if (Sample == "st_t"){
    SF= TH1F_st_t->GetBinContent(TH1F_st_t->FindBin(nJets));
  }
  else if (Sample == "st_tw"){  
    SF= TH1F_st_tW->GetBinContent(TH1F_st_tW->FindBin(nJets));
  }
  else if (Sample == "stbar_t"){    
    SF= TH1F_stbar_t->GetBinContent(TH1F_stbar_t->FindBin(nJets));
  }
  else if (Sample == "stbar_tw"){  
    SF= TH1F_stbar_tW->GetBinContent(TH1F_stbar_tW->FindBin(nJets));
  }
  else if (Sample == "TTbar_sl"){
    SF= TH1F_tt_SL->GetBinContent(TH1F_tt_SL->FindBin(nJets));
  }
  else if (Sample == "TTbar_dl"){
    SF= TH1F_tt_DL->GetBinContent(TH1F_tt_DL->FindBin(nJets));
  }
  else if (Sample == "TTbar_fh"){
    SF= TH1F_tt_FH->GetBinContent(TH1F_tt_FH->FindBin(nJets));
  }
  else if (Sample == "ttw_wqq"){
    SF= TH1F_ttW->GetBinContent(TH1F_ttW->FindBin(nJets));
  }
  else if (Sample == "ttz_zqq"){
    SF= TH1F_ttZ->GetBinContent(TH1F_ttZ->FindBin(nJets));
  }
  else if (Sample == "WjetstoQQ400"){            
    SF= TH1F_WJets_400->GetBinContent(TH1F_WJets_400->FindBin(nJets));
  }
  else if (Sample == "WJetsToQQ600"){
    SF= TH1F_WJets_600->GetBinContent(TH1F_WJets_600->FindBin(nJets));
  }
  else if (Sample == "WJetsToQQ800"){
    SF= TH1F_WJets_800->GetBinContent(TH1F_WJets_800->FindBin(nJets));
  }
  else if (Sample == "ZJetsToQQ400"){
    SF= TH1F_ZJets_400->GetBinContent(TH1F_ZJets_400->FindBin(nJets));
  }
  else if (Sample == "ZJetsToQQ600"){
    SF= TH1F_ZJets_600->GetBinContent(TH1F_ZJets_600->FindBin(nJets));
  }
  else if (Sample == "ZJetsToQQ800"){
    SF= TH1F_ZJets_800->GetBinContent(TH1F_ZJets_800->FindBin(nJets));
  }
  else if (Sample == "zz"){
    SF= TH1F_ZZ->GetBinContent(TH1F_ZZ->FindBin(nJets));
  }
  else if (Sample == "wz"){
    SF= TH1F_WZ->GetBinContent(TH1F_WZ->FindBin(nJets));
  }
  else if (Sample == "ww"){
    SF= TH1F_WW->GetBinContent(TH1F_WW->FindBin(nJets));
  }
  else if (Sample == "ttH_hbb"){      
    SF= TH1F_ttHbb->GetBinContent(TH1F_ttHbb->FindBin(nJets));
  }
  else if (Sample == "ttH_nonhbb"){
    bin = TH1F_ttHnonbb->FindBin(nJets);
    SF = TH1F_ttHnonbb->GetBinContent(bin);
  }
  else {
    std::cout << "SOMETHING IS WRONG IN THE BTAG WEIGHT NAME -- "<< Sample << std::endl;
    SF = 1;
  }
  //std::cout << "Sample "<< Sample << " nJets "<< nJets << " bin " << bin <<" SF " << SF << " recalc " << TH1F_ttHnonbb->GetBinContent(bin) << std::endl;
  return SF;   
  }
