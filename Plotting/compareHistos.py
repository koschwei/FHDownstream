import logging

import ROOT

import classes.ratios as rb
from classes.PlotHelpers import saveCanvasListAsPDF, initLogging

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

def compareHistos(inputFiles, outputName, outputFolder = ".", nameFilters = None, legendElements = None):
    rFiles = {}
    keys = {}
    for inputFile in inputFiles:
        rFiles[inputFile] = ROOT.TFile.Open(inputFile)
        keys[inputFile] = []
        for key in rFiles[inputFile].GetListOfKeys():
            keys[inputFile].append(key.GetName())

    legendElements_ = []
    if legendElements is None:
        for f in inputFiles:
            legendElements_.append(f.split("/")[-1].split(".")[0])
    else:
        legendElements_ = legendElements
    legendElements = legendElements_
    logging.debug("legendElements %s", legendElements)
    allCanvases = []
    logging.info("Starting loop for plots")
    for hName in keys[inputFiles[0]]:
        if not all(hName in keys[inputFile] for inputFile in inputFiles):
            logging.debug("%s not in all input files. Skipping", hName)
            continue
        if nameFilters is not None:
            if not all(nameFilter in hName for nameFilter in nameFilters):
                logging.debug("Skipping %s because not passing name filter")
                continue
        logging.debug("Creating histo list for ratio for %s", hName)
        hList = []
        for inputFile in inputFiles:
            hList.append(rFiles[inputFile].Get(hName))
        logging.debug("hList: %s", hList)            
        thisRatio = rb.RatioPlot("CompareHistos_{0}".format(hName))
        thisRatio.useDefaultColors = True
        thisRatio.ratioRange = (0.95, 1.05)
        thisRatio.passHistos(hList)
        allCanvases.append(thisRatio.drawPlot(legendElements, plotTitle = hName))

    logging.info("Saving output")
    saveCanvasListAsPDF(allCanvases, outputName, outputFolder)
        
    

if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--inputs",
        action = "store",
        help = "Input files (At least 2 would be great",
        type = str,
        nargs="+",
        required = True
    )
    argumentparser.add_argument(
        "--inputsLegend",
        action = "store",
        help = "Optional legend text for plots per input file. If not set, filename will be used",
        type = str,
        nargs="+",
        default = None
    )
    argumentparser.add_argument(
        "--output",
        action = "store",
        help = "File name of the output file (will append .pdf)",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--outputfolder",
        action = "store",
        help = "File name of the output file (will append .pdf)",
        type = str,
        default = "."
    )
    argumentparser.add_argument(
        "--nameFilters",
        action = "store",
        help = "String that is used to filter histograms. Technically it will look for this substring in the histoname",
        type = str,
        nargs = "+",
        default = None
    )
    
    args = argumentparser.parse_args()
    #
    ##############################################################################################################
    ##############################################################################################################
    initLogging(args.logging)

    if  args.inputsLegend is not None:
        if len(args.inputs) != len(args.inputsLegend):
            raise Exception("If --inputsLegend is set it requires to have the same number of elements as inputs")
    
    compareHistos(args.inputs, args.output, args.outputfolder, args.nameFilters, args.inputsLegend)
    logging.info("Exiting")
