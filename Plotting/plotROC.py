import ROOT

from classes.roc import ROC
from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos
import Helper

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.INFO,
)
#############################################################
#############################################################
ROOT.gROOT.LoadMacro("classes.h")
ROOT.gInterpreter.ProcessLine("TriggerSF3D* internalTriggerSF3D = new TriggerSF3D()")
ROOT.gROOT.LoadMacro("functions.h") #inlcude functions that can be used in TTree::Draw

def main():
    SignalInput = {
        "names" : ["ttH"],
        "files" : ["root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8.root"],
        "nGens" : [7565864.5],
        "xsecs" : [0.2934045],
        "color" : ROOT.kBlue
        }
    DataInput = {
        "names" : ["data"],
        "files" : ["root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/data.root"],
        "nGens" : [1],
        "xsecs" : [1],
        "color" : ROOT.kBlack
        }
    BackgroundInput = {
        "names" : ["ttAH", "ttDL", "ttSL"],
        "files" : ["root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root",
                   "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root",
                   "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root"],
        "nGens" : [120589264.0, 62658272.0, 85394144.0],
        "xsecs" : [377.96, 88.34, 365.46],
        "color" : ROOT.kRed
        }

    BackgroundInput_QCD = {
        "names" : ["QCD2000", "QCD1500", "QCD1000", "QCD700", "QCD500", "QCD300"],
        "files" : ["root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/QCD_HT2000toInf_TuneCP5_13TeV-madgraph-pythia8.root",
                   "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/QCD_HT1500to2000_TuneCP5_13TeV-madgraph-pythia8.root",
                   "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/QCD_HT1000to1500_TuneCP5_13TeV-madgraph-pythia8.root",
                   "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/QCD_HT700to1000_TuneCP5_13TeV-madgraph-pythia8.root",
                   "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/QCD_HT500to700_TuneCP5_13TeV-madgraph-pythia8.root",
                   "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/QCD_HT300to500_TuneCP5_13TeV-madgraph-pythia8.root"],
        "nGens" : [5353634.5, 11183607.0, 16462078.0, 41963160.0, 54303148.0, 59775652.0],
        "xsecs" : [20.20, 99.31, 1094.0, 6361, 30010, 323400],
        "moreweight" : "QCDnJetWeight(njets)",
        "color" : Helper.colors["qcd"]
        }

    cuts = Helper.get_cuts(DeltaEta = False)
    preSel2017 = Helper.preselection
    categories = {
        "7J3T" : "{0} && {1} && {2}".format(preSel2017, cuts["7j"], cuts["3bSR"]),
        "8J3T" : "{0} && {1} && {2}".format(preSel2017, cuts["8j"], cuts["3bSR"]),
        "9J3T" : "{0} && {1} && {2}".format(preSel2017, cuts["9j"], cuts["3bSR"]),
        "7J4T" : "{0} && {1} && {2}".format(preSel2017, cuts["7j"], cuts["4bSR"]),
        "8J4T" : "{0} && {1} && {2}".format(preSel2017, cuts["8j"], cuts["4bSR"]),
        "9J4T" : "{0} && {1} && {2}".format(preSel2017, cuts["9j"], cuts["4bSR"]),
    }

    qglNorm = Helper.get_QGNormaliziation(deltaEta = False)
    catsForNorm = {
        "7J3T" : ( "7j", "3t","SR"),
        "8J3T" : ( "8j", "3t","SR"),
        "9J3T" : ( "9j", "3t","SR"),
        "7J4T" : ( "7j", "4t","SR"),
        "8J4T" : ( "8j", "4t","SR"),
        "9J4T" : ( "9j", "4t","SR"),
    }
    mcWeight = Helper.get_weights()
    rfiles = {}
    trees = {}
    allFiles = SignalInput["files"]+BackgroundInput["files"]+BackgroundInput_QCD["files"]+DataInput["files"]
    allNames = SignalInput["names"]+BackgroundInput["names"]+BackgroundInput_QCD["names"]+DataInput["names"]
    logging.info("Creating TFiles and TTrees")
    for i in range(len(allFiles)):
        #rfiles[allNames[i]] = ROOT.TFile(allFiles[i], "READ")
        rfiles[allNames[i]] = ROOT.TFile.Open(allFiles[i])
        trees[allNames[i]] = rfiles[allNames[i]].Get("tree")

    cuts = []
    cutNames = ["7J3T", "8J3T", "9J3T", "7J4T", "8J4T", "9J4T"]
    for name in cutNames:
       cuts.append(categories[name])
    #            nJets >= 6  nJets >= 7  nJets >= 8  nJets >= 9
    variables = ["Detaj[4]", "Detaj[5]", "Detaj[6]", "Detaj[7]"]
    histos = [ROOT.TH1F("hBaseDetaj4","hBaseDetaj4", 40,0,3),
              ROOT.TH1F("hBaseDetaj5","hBaseDetaj5", 40,0,3.5),
              ROOT.TH1F("hBaseDetaj6","hBaseDetaj6", 40,0.5,4),
              ROOT.TH1F("hBaseDetaj7","hBaseDetaj7", 40,0.5,4)]
    niceNames = {
        "Detaj[4]" : "#Delta#etaJ_{5}",
        "Detaj[5]" : "#Delta#etaJ_{6}",
        "Detaj[6]" : "#Delta#etaJ_{7}",
        "Detaj[7]" : "#Delta#etaJ_{8}",
        "7J3T" : Helper.CatLabels["j7t3"],
        "8J3T" : Helper.CatLabels["j8t3"],
        "9J3T" : Helper.CatLabels["j9t3"],
        "7J4T" : Helper.CatLabels["j7t4"],
        "8J4T" : Helper.CatLabels["j8t4"],
        "9J4T" : Helper.CatLabels["j9t4"]
    }
    histos[0].GetXaxis().SetTitle("#Delta#etaJ_{5}")
    histos[1].GetXaxis().SetTitle("#Delta#etaJ_{6}")
    histos[2].GetXaxis().SetTitle("#Delta#etaJ_{7}")
    histos[3].GetXaxis().SetTitle("#Delta#etaJ_{8}")
    # histos[0].SetTitle("")
    # histos[1].SetTitle("")
    # histos[2].SetTitle("")
    # histos[3].SetTitle("")

    
    for icut, cut in enumerate(cuts):
        logging.info("Processing cut: %s",cutNames[icut])
        idJ, idB, idR = catsForNorm[cutNames[icut]]
        allCanvases = []
        thisTables = []
        distCanvases = []
        for ivar, variable in enumerate(variables):
            logging.info("Processing variables: %s", variable)
            hSignal = histos[ivar].Clone("hSignal_"+variable+"_"+cutNames[icut])
            hSignal.SetLineColor(SignalInput["color"])
            for iname, name in enumerate(SignalInput["names"]):
                tName = hSignal.GetName()+"_"+str(iname)
                hSigTmp = hSignal.Clone(tName)
                weight = "((1000 * {0})/{1}) * {2} * {3}".format(SignalInput["xsecs"][iname], SignalInput["nGens"][iname], mcWeight, qglNorm["ttH_hbb"][idJ][idB][idR])
                trees[name].Project(tName, variable, "({0}) * ({1})".format(cut, weight))
                hSignal.Add(hSigTmp)

            hData = histos[ivar].Clone("hData_"+variable+"_"+cutNames[icut])
            hData.SetLineColor(DataInput["color"])
            for iname, name in enumerate(DataInput["names"]):
                tName = hData.GetName()+"_"+str(iname)
                hDataTmp = hData.Clone(tName)
                weight = "1"
                trees[name].Project(tName, variable, "({0}) * ({1})".format(cut, weight))
                hData.Add(hDataTmp)
                
            hBackground = histos[ivar].Clone("hBackground_"+variable+"_"+cutNames[icut])
            hBackground.SetLineColor(BackgroundInput["color"])
            for iname, name in enumerate(BackgroundInput["names"]):
                smapleNameforNorm = ""
                if name == "ttAH":
                    smapleNameforNorm = "TTbar_fh"
                elif name == "ttDL":
                    smapleNameforNorm = "TTbar_dl"
                elif name =="ttSL":
                    smapleNameforNorm = "TTbar_sl"
                tName = hBackground.GetName()+"_"+str(iname)
                hBkgTmp = hBackground.Clone(tName)
                weight = "((1000 * {0})/{1}) * {2} * {3}".format(BackgroundInput["xsecs"][iname], BackgroundInput["nGens"][iname], mcWeight, qglNorm[smapleNameforNorm][idJ][idB][idR])
                trees[name].Project(tName, variable, "({0}) * ({1})".format(cut, weight))
                hBackground.Add(hBkgTmp)

            hBackground_QCD = histos[ivar].Clone("hBackground_QCD_"+variable+"_"+cutNames[icut])
            hBackground_QCD.SetLineColor(BackgroundInput_QCD["color"])
            for iname, name in enumerate(BackgroundInput_QCD["names"]):
                tName = hBackground_QCD.GetName()+"_"+str(iname)
                hBkgTmp = hBackground_QCD.Clone(tName)
                weight = "((1000 * {0})/{1}) * {2} * {3} * {4}".format(BackgroundInput_QCD["xsecs"][iname], BackgroundInput_QCD["nGens"][iname], mcWeight, BackgroundInput_QCD["moreweight"], qglNorm[name][idJ][idB][idR])
                trees[name].Project(tName, variable, "({0}) * ({1})".format(cut, weight))
                hBackground_QCD.Add(hBkgTmp)

                
            thisROC = ROC(variable,  rocType="sEff-bEff")
            thisROC.passHistos(hSignal, hBackground)
            thisROC.passHistos(hSignal, hBackground_QCD)
            #thisROC.passHistos(hBackground, hBackground_QCD)
            #thisROC.color[2] = ROOT.kOrange
            thisROC.passHistos(hSignal, hData)
            thisROC.addLabel(niceNames[cutNames[icut]], 0.16, 0.87, "Left", scaleText = 2)
            thisROC.addLabel(niceNames[variable], 0.13, 1-(0.068/2), "Left", scaleText = 2)
            allCanvases.append(
                thisROC.drawROC(
                    ["ttH vs ttbar", "ttH vs QCD", "ttH vs Data"],
                    invert = True,
                    drawCMS = True
                )
            )
            thisTables.append(thisROC.getCutTable(1))
            #distCanvases.append(makeCanvasOfHistos("blubb"+str(ivar),variable,[hSignal, hBackground, hBackground_QCD], legendText = ["ttH","ttbar","QCD"], normalized = True))
            
        saveCanvasListAsPDF(allCanvases+distCanvases, "ROCs_"+cutNames[icut],".")
        thisROC.writeTableOutput(thisTables, variables, "ttH vs. QCD MC - {0}".format(cutNames[icut]), "Detaj2017_Cuts_"+cutNames[icut])
        #break
if __name__ == "__main__":
    main()
