import ROOT
import collections
import json
import os

luminosity = 41.527540362872
lumi_Trigger =  41.527540362872

colors = { "ddQCD": 9007, "qcd": 9007,
           "ttH_hbb": 9001, "ttH": 9001, "total_signal":9001,
           "ttbarPlusBBbar": ROOT.kRed+3,
           "ttbarPlus2B":  ROOT.kRed-5,
           "ttbarPlusB" : ROOT.kRed+1, "ttb":9004,
           "ttbarPlusCCbar": ROOT.kRed-9,
           "ttbarOther": ROOT.kRed+2,
           "ttH_nonhbb": 9008, "ttH_hcc": 9008, "ttH_hww": 9008, "ttH_hzz": 9008,
           "ttH_htt": 9008, "ttH_hgg": 9008, "ttH_hgluglu": 9008, "ttH_hzg": 9008,
           "wjets": 9009, "vjets":9009,
           "zjets":9010,
           "diboson":9011, "EWK":9011,
           "stop":9012,
           "singlet":9012,
           "ttv":9013, "ttbarW":9013, "ttbarV":9013, "ttbarZ":9013, "tOther":9013,
           "JetHT": 1,
           "TTbar_inc": ROOT.kRed+2,
           "Minor": ROOT.kAzure+8}
#histogram colors (index, r, b, g)
col_tth     = ROOT.TColor(colors["ttH_hbb"], 44/255., 62/255., 167/255.)
col_qcd     = ROOT.TColor(colors["ddQCD"], 102/255., 201/255., 77/255.)
#col_ttbarBB = ROOT.TColor(colors["ttbarPlusBBbar"], 102/255., 0/255., 0/255.)
#col_ttbar2B = ROOT.TColor(colors["ttbarPlus2B"], 80/255., 0/255., 0/255.)
#col_ttbarB  = ROOT.TColor(colors["ttbarPlusB"], 153/255., 51/255., 51/255.)
#col_ttbarCC = ROOT.TColor(colors["ttbarPlusCCbar"], 204/255., 2/255., 0/255.) 
#col_ttbarJJ = ROOT.TColor(colors["ttbarOther"], 251/255., 102/255., 102/255.)
col_tthnon  = ROOT.TColor(colors["ttH_nonhbb"], 90/255., 115/255., 203/255.)
col_wjets   = ROOT.TColor(colors["wjets"], 254/255., 195/255., 8/255.)
col_zjets   = ROOT.TColor(colors["zjets"], 191/255., 193/255., 222/255.)
col_diboson = ROOT.TColor(colors["diboson"], 229/255., 198/255., 218/255.)
col_stop    = ROOT.TColor(colors["stop"], 235/255., 73/255., 247/255.)
col_ttv     = ROOT.TColor(colors["ttv"], 246/255., 236/255., 145/255.)

legnames = { "ddQCD": "Multijet",
             "qcd": "Multijet",
             "ttH_hbb": "t#bar{t}H(bb)",
             "ttH": "t#bar{t}H",
             "total_signal": "t#bar{t}H",
             "ttH_nonhbb": "t#bar{t}H(non)",
             "ttbarPlusBBbar": "t#bar{t}+b#bar{b}",
             "ttbarPlus2B": "t#bar{t}+2b",
             "ttbarPlusB" : "t#bar{t}+b",
             "ttb": "t#bar{t}+b#bar{b}",
             "ttbarPlusCCbar": "t#bar{t}+c#bar{c}",
             "ttbarOther": "t#bar{t}+lf",
             "wjets": "W+jets",
             "zjets": "Z+jets",
             "vjets": "V+jets",
             "diboson": "Diboson",
             "EWK":"EWK",
             "stop": "Single t",
             "singlet": "Single t",
             "ttbarV": "t#bar{t}+V",
             "ttz": "t#bar{t}+V",
             "tOther":"Other t",
             "JetHT": "Data",
             "TTbar_inc": "t#bar{t}+jets",
             "total_background" : "Total bkg",
             "total_signal" : "Signal",
             "data": "Data"}


tablenamesAN = { "ddQCD": "Multijet",
                 "qcd": "QCD (MC)",
                 "ttH_hbb": "t#bar{t}H(bb)",
                 "ttH": "t#bar{t}H",
                 "total_signal": "t#bar{t}H",
                 "ttH_nonhbb": "t#bar{t}H(non)",
                 "ttbarPlusBBbar": "\\ttbb",
                 "ttbarPlus2B": "\\tttwob",
                 "ttbarPlusB" : "\\ttb",
                 "ttb": "t#bar{t}+b#bar{b}",
                 "ttbarPlusCCbar": "\\ttcc ",
                 "ttbarOther": "\\ttlf",
                 "wjets": "W+jets",
                 "zjets": "Z+jets",
                 "vjets": "\\Vjets",
                 "diboson": "\\diboson",
                 "EWK":"EWK",
                 "stop": "Single t",
                 "singlet": "\\singlet",
                 "ttbarV": "\\ttV ",
                 "ttz": "t#bar{t}+V",
                 "tOther":"Other t",
                 "JetHT": "Data",
                 "TTbar_inc": "t#bar{t}+jets",
                 "total_background" : "Total bkg",
                 "total_signal" : "\\ttH",
                 "data": "Data"}

datasets = collections.OrderedDict([
    ("data","data"),
    ("SingleMuon","SingleMuon"),
    ("ttH_hbb","ttHTobb_M125_TuneCP5_13TeV-powheg-pythia8"),
    ("ttH_nonhbb","ttHToNonbb_M125_TuneCP5_13TeV-powheg-pythia8"),
    ("TTbar_dl","TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8"),
    ("TTbar_sl","TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8"),
    ("TTbar_fh","TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8"),
    ("QCD300","QCD_HT300to500_TuneCP5_13TeV-madgraph-pythia8"),
    ("QCD500","QCD_HT500to700_TuneCP5_13TeV-madgraph-pythia8"),
    ("QCD700","QCD_HT700to1000_TuneCP5_13TeV-madgraph-pythia8"),
    ("QCD1000","QCD_HT1000to1500_TuneCP5_13TeV-madgraph-pythia8"),
    ("QCD1500","QCD_HT1500to2000_TuneCP5_13TeV-madgraph-pythia8"),
    ("QCD2000","QCD_HT2000toInf_TuneCP5_13TeV-madgraph-pythia8"),
    ("ww","WW_TuneCP5_13TeV-pythia8"),
    ("wz","WZ_TuneCP5_13TeV-pythia8"),
    ("zz","ZZ_TuneCP5_13TeV-pythia8"),
    ("st_t","ST_t-channel_top_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8"),
    ("stbar_t","ST_t-channel_antitop_4f_inclusiveDecays_TuneCP5_13TeV-powhegV2-madspin-pythia8"),
    ("st_s","ST_s-channel_4f_leptonDecays_TuneCP5_PSweights_13TeV-amcatnlo-pythia8"),
    ("st_tw","ST_tW_top_5f_inclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8"),
    ("stbar_tw","ST_tW_antitop_5f_inclusiveDecays_TuneCP5_PSweights_13TeV-powheg-pythia8"),
    ("ttw_wqq","TTWJetsToQQ_TuneCP5_13TeV-amcatnloFXFX-madspin-pythia8"),
    ("ttz_zqq","TTZToQQ_TuneCP5_13TeV-amcatnlo-pythia8"),
    ("WjetstoQQ400","WJetsToQQ_HT400to600_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8"),
    ("WJetsToQQ600","WJetsToQQ_HT600to800_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8"),
    ("WJetsToQQ800","WJetsToQQ_HT-800toInf_qc19_3j_TuneCP5_13TeV-madgraphMLM-pythia8"),
    ("ZJetsToQQ400","ZJetsToQQ_HT400to600_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8"),
    ("ZJetsToQQ600","ZJetsToQQ_HT600to800_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8"),
    ("ZJetsToQQ800","ZJetsToQQ_HT-800toInf_qc19_4j_TuneCP5_13TeV-madgraphMLM-pythia8"),
    ])



xsecs = {
    "ttH_hbb": 0.2934045, #0.2934045,
    "ttH_nonhbb": 0.2150955, #0.2150955,
    "TTbar_dl": 88.34,
    "TTbar_sl": 365.46,
    "TTbar_fh": 377.96,
    "QCD300": 323400,
    "QCD500": 30010,
    "QCD700": 6361,
    "QCD1000": 1094.0,
    "QCD1500": 99.31,
    "QCD2000": 20.20,
    "ww": 118.7,
    "wz": 47.31,
    "zz": 16.52,
    "st_t": 136.02,
    "stbar_t": 80.95,
    "st_s": 3.7,
    "st_tw": 35.85,
    "stbar_tw": 35.85,
    "ttw_wqq": 0.4357,
    "ttz_zqq": 0.5104,
    "WjetstoQQ400": 313.1,
    "WJetsToQQ600": 68.54,
    "WJetsToQQ800": 34.0,
    "ZJetsToQQ400": 146.0,
    "ZJetsToQQ600": 34.1,
    "ZJetsToQQ800": 18.69,
    }

nGen = { "ttH_hbb": 7565864.5,
         "ttH_nonhbb": 7478198.5,
         "TTbar_dl": 62658272.0,
         "TTbar_sl": 85394144.0,
         "TTbar_fh": 120589264.0,
         "QCD300": 59775652.0,
         "QCD500": 54303148.0,
         "QCD700": 41963160.0,
         "QCD1000": 16462078.0,
         "QCD1500": 11183607.0,
         "QCD2000": 5353634.5,
         "ww": 7791012.5,
         "wz": 3928502.75,
         "zz": 1931127.75,
         "st_t": 5778951.0,
         "stbar_t": 3939857.25,
         "st_s": 6176873.0,
         "st_tw": 7883547.5,
         "stbar_tw": 7931641.5,
         "ttw_wqq": 439775.0,
         "ttz_zqq": 349241.96875,
         "WjetstoQQ400": 9687051.0,
         "WJetsToQQ600": 8759082.0,
         "WJetsToQQ800": 8024024.0,
         "ZJetsToQQ400": 10249102.0,
         "ZJetsToQQ600": 8841903.0,
         "ZJetsToQQ800": 7531148.0,
         }

sampleIDMap = {
     "QCD300" : 1,
     "QCD500" : 2,
     "QCD700" : 3,
     "QCD1000" : 4,
     "QCD1500" : 5,
     "QCD2000" : 6,
     "st_s" : 7,
     "st_t" : 8,
     "st_tw" : 9,
     "stbar_t" : 10,
     "stbar_tw" : 11,
     "TTbar_sl" : 12,
     "TTbar_dl" : 13,
     "TTbar_fh" : 14,
     "ttw_wqq" : 15,
     "ttz_zqq" : 16,
     "WjetstoQQ400" : 17,
     "WJetsToQQ600" : 18,
     "WJetsToQQ800" : 19,
     "ZJetsToQQ400" : 20,
     "ZJetsToQQ600" : 21,
     "ZJetsToQQ800" : 22,
     "zz" : 23,
     "wz" : 24,
     "ww" : 25,
     "ttH_hbb" : 26,
     "ttH_nonhbb" : 27,
}

stackOrderDatacard = collections.OrderedDict([
    ("diboson",  ["diboson"]),
    ("ttbarV",  ["ttbarZ", "ttbarW"]),
    ("vjets",  ["zjets","wjets"]),
    ("singlet",  ["singlet"]),
    ("ttbarPlusBBbar",  ["ttbarPlusBBbar"]),
    ("ttbarPlusB",  ["ttbarPlusB"]),
    ("ttbarPlus2B",  ["ttbarPlus2B"]),
    ("ttbarPlusCCbar",  ["ttbarPlusCCbar"]),
    ("ttbarOther",  ["ttbarOther"]),
    ("ddQCD",  ["ddQCD"]),
    ])

stackOrderBaseline = collections.OrderedDict([
    ("diboson",  ["diboson"]),
    ("ttbarV",  ["ttbarZ", "ttbarW"]),
    ("vjets",  ["zjets","wjets"]),
    ("singlet",  ["singlet"]),
    ("ttbarPlusBBbar",  ["ttbarPlusBBbar"]),
    ("ttbarPlusB",  ["ttbarPlusB"]),
    ("ttbarPlus2B",  ["ttbarPlus2B"]),
    ("ttbarPlusCCbar",  ["ttbarPlusCCbar"]),
    ("ttbarOther",  ["ttbarOther"]),
    ("qcd",  ["qcd"]),
    ])


tableOrderDatacard = collections.OrderedDict([
    ("ddQCD",  ["ddQCD"]),
    ("ttbarOther",  ["ttbarOther"]),
    ("ttbarPlusCCbar",  ["ttbarPlusCCbar"]),
    ("ttbarPlus2B",  ["ttbarPlus2B"]),
    ("ttbarPlusB",  ["ttbarPlusB"]),
    ("ttbarPlusBBbar",  ["ttbarPlusBBbar"]),
    ("singlet",  ["singlet"]),
    ("vjets",  ["zjets","wjets"]),
    ("ttbarV",  ["ttbarZ", "ttbarW"]),
    ("diboson",  ["diboson"]),
    ("total_background",  ["total_background"]),
    ("total_signal",  ["total_signal"]),
    ("data", ["data"])
    ])


tableOrderBaseline = collections.OrderedDict([
    ("qcd",  ["qcd"]),
    ("ttbarOther",  ["ttbarOther"]),
    ("ttbarPlusCCbar",  ["ttbarPlusCCbar"]),
    ("ttbarPlus2B",  ["ttbarPlus2B"]),
    ("ttbarPlusB",  ["ttbarPlusB"]),
    ("ttbarPlusBBbar",  ["ttbarPlusBBbar"]),
    ("singlet",  ["singlet"]),
    ("vjets",  ["zjets","wjets"]),
    ("ttbarV",  ["ttbarZ", "ttbarW"]),
    ("diboson",  ["diboson"]),
    ("total_background",  ["total_background"]),
    ("total_signal",  ["total_signal"]),
    ("data", ["data"])
    ])

signalsDatacard = [
    "ttH_hbb",
    "ttH_hcc",
    "ttH_htt",
    "ttH_hgluglu",
    "ttH_hgg",
    "ttH_hww",
    "ttH_hzz",
    "ttH_hzg",
    ]
"""
qgWtFac = { "ttH_hbb": 0.954,
            "ttH_nonhbb": 0.938,
            "ttH_hcc": 0.955,
            "ttH_hgg": 0.818,
            "ttH_hzz": 0.944,
            "ttH_hgluglu": 0.992,
            "ttH_hww": 0.930,
            "ttH_hss": 0.908,
            "ttH_hmm": 1.033,
            "ttH_htt": 0.858,
            "ttH_hzg": 0.867,
            "TTbar_inc": 0.912,
            "ttbarPlusBBbar": 0.940,
            "ttbarPlusCCbar": 0.931,
            "ttbarPlus2B": 0.928,
            "ttbarPlusB": 0.927,
            "ttbarOther": 0.902,
            "QCD300": 0.966,
            "QCD500": 1.000,
            "QCD700": 1.002,
            "QCD1000": 0.979,
            "QCD1500": 0.931,
            "QCD2000": 0.924,
            "ww": 0.871,
            "wz": 0.913,
            "zz": 0.896,
            "st_t": 0.912,
            "stbar_t": 0.910,
            "st_s_inc": 0.914,
            "st_tw": 0.873,
            "stbar_tw": 0.882,
            "ttw_wqq": 0.894,
            "ttz_zqq": 0.916,
            "WJetsToQQ600": 0.902,
            "WJetsToQQ180": 0.891,
            "WJetsToQQ180_gencut": 0.892, #gencut, lheHT>400
            "ZJetsToQQ600": 0.949,
            "WWTo4Q": 0.902,
            "ZZTo4Q": 0.910,
            "st_s_lep": 1,
            "ttw_wlnu": 1,
            "ttz_zllnunu": 1,
            "wjets100": 1,
            "wjets200": 1,
            "wjets400": 1,
            "wjets600": 1,
            "wjets800": 1,
            "wjets1200": 1,
            "wjets2500": 1,
            "zjets_m5_100": 1,
            "zjets_m5_200": 1,
            "zjets_m5_400": 1,
            "zjets_m5_600": 1,
            "zjets_m50_100": 1,
            "zjets_m50_200": 1,
            "zjets_m50_400": 1,
            "zjets_m50_600": 1,
            "zjets_m50_800": 1,
            "zjets_m50_1200": 1,
            "zjets_m50_2500": 1
            }

qgWtFacSL = {"ttH_hbb": 0.982,
             "TTbar_inc": 0.953,
             "ttbarPlusBBbar": 0.977,
             "ttbarPlusCCbar": 0.970,
             "ttbarPlus2B": 0.970,
             "ttbarPlusB": 0.969,
             "ttbarOther": 0.948,
             "QCD300": 0.602,
             "QCD500": 0.944,
             "QCD700": 0.908,
             "QCD1000": 1.071,
             "QCD1500": 0.934,
             "QCD2000": 1.031,
             "ww": 0.909,
             "wz": 0.928,
             "zz": 0.920,
             "st_t": 0.948,
             "stbar_t": 0.948,
             "st_s_inc": 0.961,
             "st_tw": 0.947,
             "stbar_tw": 0.946,
             "ttw_wqq": 0.941,
             "ttz_zqq": 0.958,
             "WJetsToQQ180": 0.958,
             "WJetsToQQ180_gencut": 0.958,
             "ZJetsToQQ600": 1.033,
             "st_s_lep": 0.961,
             "ttw_wlnu": 0.936,
             "ttz_zllnunu": 0.946,
             "wjets100": 0.936,
             "wjets200": 0.941,
             "wjets400": 0.934,
             "wjets600": 0.930,
             "wjets800": 0.920,
             "wjets1200": 0.892,
             "wjets2500": 0.833,
             "zjets_m5_100": 0.952,
             "zjets_m5_200": 0.948,
             "zjets_m5_400": 0.933,
             "zjets_m5_600": 0.940,
             "zjets_m50_100": 0.906,
             "zjets_m50_200": 0.917,
             "zjets_m50_400": 0.911,
             "zjets_m50_600": 0.903,
             "zjets_m50_800": 0.904,
             "zjets_m50_1200": 0.879,
             "zjets_m50_2500": 0.825,
             }

qgWtFacSL5j = {"ttH_hbb": 0.977,
               "TTbar_inc": 0.941,
               "ttbarPlusBBbar": 0.970,
               "ttbarPlusCCbar": 0.961,
               "ttbarPlus2B": 0.959,
               "ttbarPlusB": 0.958,
               "ttbarOther": 0.933,
               "QCD300": 0.513,
               "QCD500": 0.952,
               "QCD700": 0.995,
               "QCD1000": 1.076,
               "QCD1500": 0.982,
               "QCD2000": 1.031,
               "ww": 0.858,
               "wz": 0.918,
               "zz": 0.968,
               "st_t": 0.928,
               "stbar_t": 0.945,
               "st_s_inc": 0.952,
               "st_tw": 0.936,
               "stbar_tw": 0.928,
               "ttw_wqq": 0.931,
               "ttz_zqq": 0.948,
               "WJetsToQQ180": 0.958,
               "WJetsToQQ180_gencut": 0.958,
               "ZJetsToQQ600": 1.033,
               "st_s_lep": 0.968,
               "ttw_wlnu": 0.923,
               "ttz_zllnunu": 0.931,
               "wjets100": 0.931,
               "wjets200": 0.924,
               "wjets400": 0.928,
               "wjets600": 0.930,
               "wjets800": 0.922,
               "wjets1200": 0.899,
               "wjets2500": 0.833,
               "zjets_m5_100": 0.878,
               "zjets_m5_200": 0.935,
               "zjets_m5_400": 0.927,
               "zjets_m5_600": 0.954,
               "zjets_m50_100": 0.985,
               "zjets_m50_200": 0.904,
               "zjets_m50_400": 0.901,
               "zjets_m50_600": 0.899,
               "zjets_m50_800": 0.894,
               "zjets_m50_1200": 0.884,
               "zjets_m50_2500": 0.827,
               }

qgWtFacSL6j = {"ttH_hbb": 0.970,
               "TTbar_inc": 0.934,
               "ttbarPlusBBbar": 0.966,
               "ttbarPlusCCbar": 0.952,
               "ttbarPlus2B": 0.949,
               "ttbarPlusB": 0.948,
               "ttbarOther": 0.923,
               "QCD300": 0.400,
               "QCD500": 1.004,
               "QCD700": 0.963,
               "QCD1000": 1.321,
               "QCD1500": 0.987,
               "QCD2000": 1.031,
               "ww": 0.803,
               "wz": 0.814,
               "zz": 0.777,
               "st_t": 0.896,
               "stbar_t": 0.958,
               "st_s_inc": 0.961,
               "st_tw": 0.935,
               "stbar_tw": 0.908,
               "ttw_wqq": 0.915,
               "ttz_zqq": 0.937,
               "WJetsToQQ180": 1,
               "WJetsToQQ180_gencut": 1,
               "ZJetsToQQ600": 1.034,
               "st_s_lep": 0.941,
               "ttw_wlnu": 0.909,
               "ttz_zllnunu": 0.919,
               "wjets100": 1.041,
               "wjets200": 0.910,
               "wjets400": 0.918,
               "wjets600": 0.937,
               "wjets800": 0.917,
               "wjets1200": 0.900,
               "wjets2500": 0.839,
               "zjets_m5_100": 1,
               "zjets_m5_200": 1.046,
               "zjets_m5_400": 0.879,
               "zjets_m5_600": 0.926,
               "zjets_m50_100": 1.601,
               "zjets_m50_200": 0.941,
               "zjets_m50_400": 0.867,
               "zjets_m50_600": 0.892,
               "zjets_m50_800": 0.905,
               "zjets_m50_1200": 0.890,
               "zjets_m50_2500": 0.812,
               }
"""
preselection = "(HLT_ttH_FH == 1 || HLT_BIT_HLT_PFHT1050 == 1) && ht30>=500 && jets_pt[5]>=40 && passMETFilters == 1"
preselSL = "HLT_BIT_HLT_IsoMu27 && is_sl && abs(leps_pdgId[0]) == 13 && njets >= 4 && nBDeepCSVM>=2  && Flag_METFilters == 1 && met_pt > 20"

def get_cuts(systematic="", DeltaEta = True):
    DeepCSVL = 0.1522
    DeepCSVML = 0.2550 #Value that splits CR/CR2 in half
    if systematic != "":
        raise NotImplementedError("See warning! Disabled until then :)")
    if DeltaEta:
        detaJCut3b = "(Alt$(Detaj[5],0)<2.36 && numJets==7)||(Alt$(Detaj[6],0)<2.6 && numJets==8)||(Alt$(Detaj[7],0)<2.64 && numJets>=9)"
        detaJCut4b = "(Alt$(Detaj[5],0)<2.25 && numJets==7)||(Alt$(Detaj[6],0)<2.52 && numJets==8)||(Alt$(Detaj[7],0)<2.6 && numJets>=9)"
    else:
        detaJCut3b = "1"
        detaJCut4b = "1"
    cuts = {"6j":"numJets{0}==6".format(systematic),
            "ge6j":"numJets{0}>=6".format(systematic),
            "ge7j":"numJets{0}>=7 && Wmass{0}>60 && Wmass{0}<100".format(systematic),
            "ge8j":"numJets{0}>=8 && Wmass{0}>60 && Wmass{0}<100".format(systematic),
            "7j":"numJets{0}==7 && Wmass{0}>60 && Wmass{0}<100".format(systematic),
            "8j":"numJets{0}==8 && Wmass{0}>60 && Wmass{0}<100".format(systematic),
            "9j":"numJets{0}>=9 && Wmass{0}>70 && Wmass{0}<92".format(systematic),
            "7jnoW":"numJets{0}==7".format(systematic),
            "8jnoW":"numJets{0}==8".format(systematic),
            "9jnoW":"numJets{0}>=9".format(systematic),
            "3bSR":"nBDeepCSVM{0}==3 && qg_LR_3b_flavour_5q_0q{0}>0.5 && ({1})".format(systematic,detaJCut3b),
            "4bSR":"nBDeepCSVM{0}>=4 && qg_LR_4b_flavour_5q_0q{0}>0.5 && ({1})".format(systematic,detaJCut4b),
            "3bCR":"nBDeepCSVM{0}==2 && nBDeepCSVL{0}==3 && qg_LR_3b_flavour_5q_0q{0}>0.5 && ({2})".format(systematic, DeepCSVL,detaJCut3b),
            "4bCR":"nBDeepCSVM{0}==2 && nBDeepCSVL{0}>=4 && qg_LR_4b_flavour_5q_0q{0}>0.5 && ({2})".format(systematic, DeepCSVL,detaJCut4b),
            "3bVR":"nBDeepCSVM{0}==3 && qg_LR_3b_flavour_5q_0q{0}<0.5 && ({1})".format(systematic,detaJCut3b),
            "4bVR":"nBDeepCSVM{0}>=4 && qg_LR_4b_flavour_5q_0q{0}<0.5 && ({1})".format(systematic,detaJCut4b),
            "3bCR2":"nBDeepCSVM{0}==2 && nBDeepCSVL{0}==3 && qg_LR_3b_flavour_5q_0q{0}<0.5 && ({2})".format(systematic, DeepCSVL,detaJCut3b),
            "4bCR2":"nBDeepCSVM{0}==2 && nBDeepCSVL{0}>=4 && qg_LR_4b_flavour_5q_0q{0}<0.5 && ({2})".format(systematic, DeepCSVL,detaJCut4b),

            "3bSRx":"nBDeepCSVM==2 && nBDeepCSVL==3 && Sum$(jets_btagDeepCSV>{1})==3 && qg_LR_3b_flavour_5q_0q>0.5".format(systematic, DeepCSVML),
            "4bSRx":"nBDeepCSVM==2 && nBDeepCSVL>=4 && Sum$(jets_btagDeepCSV>{1})>=4 && qg_LR_4b_flavour_5q_0q>0.5".format(systematic, DeepCSVML),
            "3bCRx":"nBDeepCSVM==2 && nBDeepCSVL==3 && Sum$(jets_btagDeepCSV>{1})==2 && qg_LR_3b_flavour_5q_0q>0.5".format(systematic, DeepCSVML),
            "4bCRx":"nBDeepCSVM==2 && nBDeepCSVL>=4 && Sum$(jets_btagDeepCSV>{1})==2 && qg_LR_4b_flavour_5q_0q>0.5".format(systematic, DeepCSVML),
            "3bVRx":"nBDeepCSVM==2 && nBDeepCSVL==3 && Sum$(jets_btagDeepCSV>{1})==3 && qg_LR_3b_flavour_5q_0q<0.5".format(systematic, DeepCSVML),
            "4bVRx":"nBDeepCSVM==2 && nBDeepCSVL>=4 && Sum$(jets_btagDeepCSV>{1})>=4 && qg_LR_4b_flavour_5q_0q<0.5".format(systematic, DeepCSVML),
            "3bCR2x":"nBDeepCSVM==2 && nBDeepCSVL==3 && Sum$(jets_btagDeepCSV>{1})==2 && qg_LR_3b_flavour_5q_0q<0.5".format(systematic, DeepCSVML),
            "4bCR2x":"nBDeepCSVM==2 && nBDeepCSVL>=4 && Sum$(jets_btagDeepCSV>{1})==2 && qg_LR_4b_flavour_5q_0q<0.5".format(systematic, DeepCSVML),

            "3bSRy":"nBDeepCSVM{0}==3 && qg_LR_3b_flavour_5q_0q{0}>0.3 && qg_LR_3b_flavour_5q_0q{0}<0.5".format(systematic),
            "4bSRy":"nBDeepCSVM{0}>=4 && qg_LR_4b_flavour_5q_0q{0}>0.3 && qg_LR_3b_flavour_5q_0q{0}<0.5".format(systematic),
            "3bCRy":"nBDeepCSVM{0}==2 && nBDeepCSVL==3 && qg_LR_3b_flavour_5q_0q{0}>0.3 && qg_LR_3b_flavour_5q_0q{0}<0.5".format(systematic, DeepCSVL),
            "4bCRy":"nBDeepCSVM{0}==2 && nBDeepCSVL>=4 && qg_LR_4b_flavour_5q_0q{0}>0.3 && qg_LR_3b_flavour_5q_0q{0}<0.5".format(systematic, DeepCSVL),
            "3bVRy":"nBDeepCSVM{0}==3 && qg_LR_3b_flavour_5q_0q{0}<0.3".format(systematic),
            "4bVRy":"nBDeepCSVM{0}>=4 && qg_LR_4b_flavour_5q_0q{0}<0.3".format(systematic),
            "3bCR2y":"nBDeepCSVM{0}==2 && nBDeepCSVL==3 && qg_LR_3b_flavour_5q_0q{0}<0.3".format(systematic, DeepCSVL),
            "4bCR2y":"nBDeepCSVM{0}==2 && nBDeepCSVL>=4 && qg_LR_4b_flavour_5q_0q{0}<0.3".format(systematic, DeepCSVL),

            "2bPresel":"nBDeepCSVM>=2",
            "3bPresel":"nBDeepCSVM>=2",
            "4bPresel":"nBDeepCSVM>=2",
            "ge4j":"numJets>=4",
            "ge5j":"numJets>=5",

}
    return cuts




ttCuts = {"ttbarPlusBBbar": "ttCls>52",
          "ttbarPlus2B": "ttCls==52",
          "ttbarPlusB": "ttCls==51",
          "ttbarPlusCCbar": "ttCls>40 && ttCls<46",
          "ttbarOther": "ttCls<40"}

ttHcuts = { "ttH_hcc": "genHiggsDecayMode==4", 
            "ttH_hww": "genHiggsDecayMode==24",
            "ttH_hzz": "genHiggsDecayMode==23",
            "ttH_htt": "genHiggsDecayMode==15",
            "ttH_hgg": "genHiggsDecayMode==22",
            "ttH_hgluglu": "genHiggsDecayMode==21",
            "ttH_hzg": "genHiggsDecayMode==230022",
            "ttH_hss": "genHiggsDecayMode==3",
            "ttH_hmm": "genHiggsDecayMode==13"}

systematics = [
    "CMS_btag_cferr1_2017",
    "CMS_btag_cferr2_2017",
    "CMS_btag_hf_2017",
    "CMS_btag_hfstats1_2017",
    "CMS_btag_hfstats2_2017",
    "CMS_btag_lf_2017",
    "CMS_btag_lfstats1_2017",
    "CMS_btag_lfstats2_2017",
    "CMS_effTrigger_fh_2017",
    "CMS_scaleAbsoluteMPFBias_j_2017",
    "CMS_scaleAbsoluteScale_j_2017",
    "CMS_scaleAbsoluteStat_j",
    "CMS_scaleFlavorQCD_j_2017",
    "CMS_scaleFragmentation_j_2017",
    "CMS_scalePileUpDataMC_j_2017",
    "CMS_scalePileUpPtBB_j_2017",
    "CMS_scalePileUpPtEC1_j_2017",
    "CMS_scalePileUpPtEC2_j_2017",
    "CMS_scalePileUpPtHF_j_2017",
    "CMS_scalePileUpPtRef_j_2017",
    "CMS_scaleRelativeBal_j_2017",
    "CMS_scaleRelativeFSR_j_2017",
    "CMS_scaleRelativeJEREC1_j",
    "CMS_scaleRelativeJEREC2_j",
    "CMS_scaleRelativeJERHF_j_2017",
    "CMS_scaleRelativePtBB_j_2017",
    "CMS_scaleRelativePtEC1_j_2017",
    "CMS_scaleRelativePtEC2_j_2017",
    "CMS_scaleRelativePtHF_j_2017",
    "CMS_scaleRelativeStatEC_j",
    "CMS_scaleRelativeStatFSR_j",
    "CMS_scaleRelativeStatHF_j",
    "CMS_scaleSinglePionECAL_j_2017",
    "CMS_scaleSinglePionHCAL_j_2017",
    "CMS_scaleTimePtEta_j",
    "CMS_ttHbb_FSR_ttbarOther_2017",
    "CMS_ttHbb_FSR_ttbarPlus2B_2017",
    "CMS_ttHbb_FSR_ttbarPlusBBbar_2017",
    "CMS_ttHbb_FSR_ttbarPlusB_2017",
    "CMS_ttHbb_FSR_ttbarPlusCCbar_2017",
    "CMS_ttHbb_HTreweight3b_2017",
    "CMS_ttHbb_HTreweight4b_2017",
    "CMS_ttHbb_ISR_ttbarOther_2017",
    "CMS_ttHbb_ISR_ttbarPlus2B_2017",
    "CMS_ttHbb_ISR_ttbarPlusBBbar_2017",
    "CMS_ttHbb_ISR_ttbarPlusB_2017",
    "CMS_ttHbb_ISR_ttbarPlusCCbar_2017",
    "CMS_ttHbb_PDF_2017",
    "CMS_ttHbb_PU",
    "CMS_ttHbb_bgnorm_ttbarPlus2B_2017",
    "CMS_ttHbb_bgnorm_ttbarPlusB_2017",
    "CMS_ttHbb_bgnorm_ttbarPlusBBbar_2017",
    "CMS_ttHbb_bgnorm_ttbarPlusCCbar_2017",
    "CMS_ttHbb_ddQCD_2017",
    "CMS_ttHbb_scaleMuF",
    "CMS_ttHbb_scaleMuR",
    "CMS_ttHbb_HDAMP_ttbarPlusBBbar_2017",
    "CMS_ttHbb_HDAMP_ttbarPlus2B_2017",
    "CMS_ttHbb_HDAMP_ttbarPlusB_2017",
    "CMS_ttHbb_HDAMP_ttbarPlusCCbar_2017",
    "CMS_ttHbb_HDAMP_ttbarOther_2017",
    "CMS_ttHbb_UE_ttbarPlusBBbar_2017",
    "CMS_ttHbb_UE_ttbarPlus2B_2017",
    "CMS_ttHbb_UE_ttbarPlusB_2017",
    "CMS_ttHbb_UE_ttbarPlusCCbar_2017",
    "CMS_ttHbb_UE_ttbarOther_2017",
    "QCDscale_V",
    "QCDscale_VV",
    "QCDscale_singlet",
    "QCDscale_ttH",
    "QCDscale_ttbar",
    "lumi_13TeV_2017",
    "pdf_Higgs_ttH",
    "pdf_gg",
    "pdf_qg",
    "pdf_qqbark",
]

Normsys = ["lumi_13TeV",
           "pdf_Higgs_ttH",
           "pdf_gg",
           "pdf_qqbar",
           "pdf_qg",
           "QCDscale_ttH",
           "QCDscale_tt",
           "QCDscale_t",
           "QCDscale_V",
           "QCDscale_VV",
           "bgnorm_ttbarPlus2B",
           "bgnorm_ttbarPlusB",
           "bgnorm_ttbarPlusBBbar",
           "bgnorm_ttbarPlusCCbar"]


CatLabels = {
    "j7t3" : "FH (7 jets, 3 b tags)",
    "j8t3" : "FH (8 jets, 3 b tags)",
    "j9t3" : "FH (#geq 9 jets, 3 b tags)",
    "j7t4" : "FH (7 jets, #geq 4 b tags)",
    "j8t4" : "FH (8 jets, #geq 4 b tags)",
    "j9t4" : "FH (#geq 9 jets, #geq 4 b tags)",

}

CatLabelsSRExt = {
    "j7t3" : "FH SR#scale[0.7]{#it{ext}} (7 jets, 3 b tags)",
    "j8t3" : "FH SR#scale[0.7]{#it{ext}} (8 jets, 3 b tags)",
    "j9t3" : "FH SR#scale[0.7]{#it{ext}} (#geq 9 jets, 3 b tags)",
    "j7t4" : "FH SR#scale[0.7]{#it{ext}} (7 jets, #geq 4 b tags)",
    "j8t4" : "FH SR#scale[0.7]{#it{ext}} (8 jets, #geq 4 b tags)",
    "j9t4" : "FH SR#scale[0.7]{#it{ext}} (#geq 9 jets, #geq 4 b tags)",

}


#Plotting size guides
#Canvas Width: 600, Height: 500
#gin top: 0.08, right: 0.04, bottom: 0.12, left: 0.12 
#axis titles: 0.055 (0.06 recommended)
#axis labels: 0.048 (0.05 recommended)
#legend: 0.045

#def create_paves(lumi, data, CMSpos=1, PrelimPos=1, xlo=0.11, ylo=0.951, xhi=0.95, yhi=1.0):
def create_paves(lumi, label, CMSposX=0.11, CMSposY=0.9, prelimPosX=0.11, prelimPosY=0.85, lumiPosX=0.95, lumiPosY=0.951, alignRight=False, CMSsize=0.75*0.08, prelimSize=0.75*0.08*0.76, lumiSize=0.6*0.08):

    #pt_lumi = ROOT.TPaveText(xhi-0.25, ylo, xhi, yhi,"brNDC")
    pt_lumi = ROOT.TPaveText(lumiPosX-0.25, lumiPosY, lumiPosX, 1.0,"brNDC")
    pt_lumi.SetFillStyle(0)
    pt_lumi.SetBorderSize(0)
    pt_lumi.SetFillColor(0)
    pt_lumi.SetTextFont(42) 
    pt_lumi.SetTextSize(lumiSize)
    pt_lumi.SetTextAlign(31) #left=10, bottom=1, centre=2 
    pt_lumi.AddText( "{0:.1f}".format(lumi)+" fb^{-1} (13 TeV)" )
    
    # if CMSpos == 0: #outside frame
    #     pt_CMS = ROOT.TPaveText(xlo, ylo, xlo+0.1, yhi,"brNDC")
    # elif CMSpos == 1: #left
    #     pt_CMS = ROOT.TPaveText(xlo+0.04, ylo-0.09, xlo+0.14, ylo-0.04,"brNDC")
    # elif CMSpos == 2: #center 
    #     pt_CMS = ROOT.TPaveText(xlo+0.4, ylo-0.09, xlo+0.5, ylo-0.04,"brNDC")
    # elif CMSpos == 3: #right
    #     pt_CMS = ROOT.TPaveText(xhi-0.2, ylo-0.09, xhi-0.1, ylo-0.04,"brNDC")
    if alignRight:
        pt_CMS = ROOT.TPaveText(CMSposX-0.1, CMSposY, CMSposX, CMSposY+0.05,"brNDC")
    else:
        pt_CMS = ROOT.TPaveText(CMSposX, CMSposY, CMSposX+0.1, CMSposY+0.05,"brNDC") 
    pt_CMS.SetFillStyle(0)
    pt_CMS.SetBorderSize(0)
    pt_CMS.SetFillColor(0)
    pt_CMS.SetTextFont(61)
    pt_CMS.SetTextSize(CMSsize)
    #pt_CMS.SetTextAlign(31 if CMSpos==3 else 11)
    pt_CMS.SetTextAlign(31 if alignRight else 11 )
    pt_CMS.AddText("CMS")
    
    # if PrelimPos == 0: #outside frame
    #     pt_prelim = ROOT.TPaveText(xlo+0.09, ylo, xlo+0.3, yhi,"brNDC")
    # elif PrelimPos == 1: #left beside CMS
    #     pt_prelim = ROOT.TPaveText(xlo+0.13, ylo-0.09, xlo+0.34, ylo-0.04,"brNDC")
    # elif PrelimPos == 2: #left under CMS
    #     pt_prelim = ROOT.TPaveText(xlo+0.04, ylo-0.15, xlo+0.14, ylo-0.10,"brNDC")
    # elif PrelimPos == 3: #right under CMS
    #     pt_prelim = ROOT.TPaveText(xhi-0.2, ylo-0.15, xhi-0.1, ylo-0.10,"brNDC")
    if alignRight:
        pt_prelim = ROOT.TPaveText(prelimPosX-0.2, prelimPosY, prelimPosX, prelimPosY+0.05,"brNDC")
    else:
        pt_prelim = ROOT.TPaveText(prelimPosX, prelimPosY, prelimPosX+0.2, prelimPosY+0.05,"brNDC")
    pt_prelim.SetFillStyle(0)
    pt_prelim.SetBorderSize(0)
    pt_prelim.SetFillColor(0)
    pt_prelim.SetTextFont(52) 
    pt_prelim.SetTextSize(prelimSize)
    #pt_prelim.SetTextAlign(31 if PrelimPos==3 else 11)
    pt_prelim.SetTextAlign(31 if alignRight else 11 )    
    if label == "SimPAS":
        pt_prelim.AddText("Simulation Preliminary")
    elif label == "DataPAS":
        pt_prelim.AddText("Preliminary")
    elif label == "Sim":
        pt_prelim.AddText("Simulation")
    elif label == "Data":
        pt_prelim.AddText("")
    elif label == "SimSupp":
        pt_prelim.AddText("Simulation Supplementary")
    elif label == "DataSupp":
        pt_prelim.AddText("Supplementary")
    elif label == "DataWiP":
        pt_prelim.AddText("Work in Progress")
    
    return {"lumi":pt_lumi, "CMS":pt_CMS, "label":pt_prelim}


def get_weights(bTagSys="", useQG=True, triggerType="function"):
    """
    Helper function for getting the usual weight used in the anaylsis of the skimmed nTuples

    Args:
    -----
    bTagSys (str) : Define the b-tag systeamtic used for the weight (not Implemeneted exceot nom
    useQG (bool) ; If True the qgWeight will be added to the weight string
    triggerType (str) : If "tree" the weifght in the skim will be used
                        If "function" the ususal definiton in functions.h will be used (this 
                        might need additional steps in the scirpt so setup correctly)
    
    Returns:
    --------
    retWeight (str) : weight string for ROOT TTree:Draw and similar operations
    """
    retWeight = "puWeight * sign(genWeight)"
    if bTagSys == "":
        retWeight += " * bTagSF * bTagSF_norm"
    else:
        raise NotImplementedError
    if useQG:
        retWeight += " * qgWeight"
    if triggerType == "tree":
        retWeight += " * triggerFHWeight"
    elif triggerType == "function":
        retWeight += " * get_TriggerSF3D(ht30, jets_pt[5], nBCSVM)"
    else:
        raise NotImplementedError

    return retWeight



def get_QGNormaliziation(deltaEta = True):
    """
    Function for getting the normalization for the qgWeight factor for all samples

    Returns:
    --------
    data (dict) : Returns nested dict with scale factors for each sample for analyisis category/region
                  --> Call with data[datasetname]["7j"/"8j"/"9j"]["2t"/"3t"]["SR"/"CR"/"CR2"/"VR"]
    """
    path =os.environ["CMSSW_BASE"]+"/src/TTH/FHDownstream/Plotting/"
    if deltaEta:
        fileName = path+"qgNorm_wDetaj_v5-bSF.json"
    else:
        fileName = path+"qgNorm_v5-bSF.json"
    with open(fileName, "r") as normFile:
        data = json.load(normFile)
    

    return data
