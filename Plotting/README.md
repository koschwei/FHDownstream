# Plot scripts for FH analysis

| Script | Description | Relevant for results |
|--------|-------------|----------|
| BestFitMu.py | Hardcoded script for creating best fit overview plot  | Yes |
| FitDiagnosticsPiePlots.py | Standalone script for creating pie charts from FitDiagnostics file (also in ttHbb datacard repo) | Yes |
| FitDiagnosticsROC.py | Script for caluclating MEM ROCs/Distribution and Yield plots from FitDiagnostics file | yes |
| FitDiagnosticsValidation.py | Script for creating VR plots from FitDiagnostics | yes |
| GetEfficiencyTH3F.py | Calculation of Effciency from 2 TH3F histogramss | no |
| Helper.py | Collection of definitions | yes |
| Trigger_SFs.py | Calculation of Trigger SF from skim | yes |
| Yield_plots.py | Calculate Yields from sparse file | no |
| compareHistos.py | Script for autimatically comparing two root files with histograms. E.g. when comparing w/ and w/o reweighting | no |
| doDetaJVal.py | PLotting correlation between DEtaJ and Wmass | no (ARC review) |
| editSFFile.py  | Script for modifying the SF file from Trigger_SF.py | yes |
| plotCRCorrEff.py | Script for plotting ddQCD in CR w/ and w/o CRCorr and SR | no |
| plotDetaJROC.py | Plot DetaJ ROC and Distributions from QCD_estimate.py output | yes |
| plotQGLR.py | Scirpt for plotting QGLR for differen processes from skim | no |
| plotROC.py |  Scirpt for plotting dEtaJ for differen processes from skim + ROC | no |
| plotSFslices.py | Script for plotting 3D Trigger SF as 2D histograms as function of z axis (b-tagging) | yes |
| plotbTagNormWeights.py | Script for plotting the normlaization factor for b.tagging | no |
| plotddDistr.py | Plot ddQCD distibutions from skim (w/o correction). Not sure why this is here | no |
