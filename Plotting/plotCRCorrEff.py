import ROOT

from classes.PlotHelpers import saveCanvasListAsPDF, makeCanvasOfHistos
from classes.ratios import RatioPlot


ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

inFile1 = "/t3home/koschwei/scratch/ttH/sparse/sparsev5_final_v2/merged_SR_new_woff_ddQCD_all.root"
inFile2 = "/t3home/koschwei/scratch/ttH/sparse/sparsev5_final_v2/merged_SR_new_woff_ddQCDSR.root"

channels = {
    "j7t3" : "fh_j7_t3__mem_FH_4w2h1t_p",
    "j7t4" : "fh_j7_t4__mem_FH_3w2h2t_p",
    "j8t3" : "fh_j8_t3__mem_FH_4w2h1t_p",
    "j8t4" : "fh_j8_t4__mem_FH_3w2h2t_p",
    "j9t3" : "fh_j9_t3__mem_FH_4w2h1t_p",
    "j9t4" : "fh_j9_t4__mem_FH_4w2h2t_p",
}
variable = "MEM"

channelNames = {
    "j7t3" : "FH ( 7 jets, 3 btags)",
    "j7t4" : "FH ( 7 jets, #geq 4 b tags)",
    "j8t3" : "FH ( 8 jets, 3 btags)",
    "j8t4" : "FH ( 8 jets, #geq 4 b tags)",
    "j9t3" : "FH ( #geq 9 jets, 3 btags)",
    "j9t4" : "FH ( #geq 9 jets, #geq 4 b tags)",
}

# channels = {
#     "j7t3" : "fh_j7_t3__ht30",
#     "j7t4" : "fh_j7_t4__ht30",
#     "j8t3" : "fh_j8_t3__ht30",
#     "j8t4" : "fh_j8_t4__ht30",
#     "j9t3" : "fh_j9_t3__ht30",
#     "j9t4" : "fh_j9_t4__ht30",
# }
# variable = "HT"


rFile1 = ROOT.TFile.Open(inFile1)
rFile2 = ROOT.TFile.Open(inFile2)


for cat in channels:
    allCanvases = []
    hCRNom = rFile1.Get("ddQCD__"+channels[cat])
    hCRnoCorr = rFile1.Get("ddQCD__"+channels[cat]+"__CRCorr_offUp")
    hSRNom = rFile2.Get("ddQCD__"+channels[cat])

    hSRNom.SetLineColor(ROOT.kBlack)
    hCRNom.SetLineColor(ROOT.kBlue)
    hCRnoCorr.SetLineColor(ROOT.kRed)

    errorband = hSRNom.Clone("Errorband")

    legendObjwText = []
    legendObjwText.append((hSRNom, "data - sim SR", "L"))#
    legendObjwText.append((hCRnoCorr, "ddQCD shape w/o CR corr", "L"))#
    legendObjwText.append((hCRNom, "ddQCD Shape", "L"))#
    legendObjwText.append((errorband, "Stat. uncertainty"))#
    
    thisRatio = RatioPlot(cat+variable)
    thisRatio.formatErrorband(errorband)
    thisRatio.passHistos([hSRNom, hCRnoCorr, hCRNom], normalize = True)
    #thisRatio.ratioRange = (-2.7, 2.7)
    #errorband = None
    thisRatio.ratioRange = (0.65, 1.35)
    thisRatio.addLabel(channelNames[cat], yStart = 0.96, scaleText = 1.2)
    thisRatio.legendSize = (0.45, 0.6, 0.9, 0.9)
    allCanvases.append(
       thisRatio.drawPlot(
           None,
           xTitle = variable,
           #drawPulls = True,
           errorband = errorband,
           histolegend = legendObjwText,
       )
    )

    saveCanvasListAsPDF(allCanvases, "ddQCD_CRCorr_comp_"+variable+"_"+cat+"_"+"", ".")
