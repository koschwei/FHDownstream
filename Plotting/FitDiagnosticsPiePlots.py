import ROOT
from array import array
from copy import deepcopy
import json

import math
import logging

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

color_qcd = ROOT.TColor(9007, 102/255., 201/255., 77/255.)

colors = {
    "ddQCD" : 9007,
    "ttbarOther" : ROOT.kRed+2,
    "ttbarPlusCCbar" : ROOT.kRed-9,
    "ttbarPlusB" : ROOT.kRed+1,
    "ttbarPlus2B" : ROOT.kRed-5,
    "ttbarPlusBBbar" : ROOT.kRed+3,
    "signal" : ROOT.kCyan,
    "minor" : ROOT.kGray,
}

niceNames = {
    "ddQCD" : "Multijet",
    "ttbarOther" : "t#bar{t}+lf",
    "ttbarPlusCCbar" : "t#bar{t}+c#bar{c}",
    "ttbarPlusB" : "t#bar{t}+b",
    "ttbarPlus2B" : "t#bar{t}+2b",
    "ttbarPlusBBbar" : "t#bar{t}+b#bar{b}",
    "signal" : "t#bar{t}H",
    "minor" : "Other Bkg",
}

class Config(object):
    def __init__(self, pathtoConfig):
        logging.info("Getting config")
        logging.debug("====================================================")
        import ConfigParser
        thisconfig = ConfigParser.ConfigParser()
        thisconfig.optionxform = str #Use this so the section names keep Uppercase letters
        thisconfig.read(pathtoConfig)

        self.fitFile = thisconfig.get("General", "fitDiagnosticsFile")
        self.printSoverB = thisconfig.getboolean("General", "printSoverB")
        logging.debug("FitDiagnostics file : %s", self.fitFile)
        self.uniqueSlices = thisconfig.get("General", "uniqueSlices")
        self.signalSlices = thisconfig.get("General", "signalSlices")
        
        self.uniqueSlices = self.uniqueSlices.split(",")
        self.signalSlices = self.signalSlices.split(",")

        logging.debug("uniqueSlices : %s", self.uniqueSlices)
        logging.debug("signalSlices : %s", self.signalSlices)
        
        self.categories = thisconfig.get("General", "categories").split(",")

        logging.debug("Categories: %s", self.categories)

        self.muForPlot = thisconfig.get("General", "mu")
        self.addMuToPlot = thisconfig.getboolean("General", "addmu")

        if thisconfig.has_option("General", "centerlabel"):
            self.centerlabel = thisconfig.get("General", "centerlabel")
        else:
            self.centerlabel = None
        
        self.plotSplitting = thisconfig.get("General", "plotsplitting").split(",")
        
        self.catLabels = {}
        self.catNamesFile = {}
        for cat in self.categories:
            self.catLabels[cat] = thisconfig.get(cat, "label") 
            self.catNamesFile[cat] = thisconfig.get(cat, "fitDiagnostics")
            logging.debug("Infos for cat %s : %s, %s", cat, self.catLabels[cat], self.catNamesFile[cat])


        logging.debug("====================================================")

def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
        datefmt="%H:%M:%S"
    )

def getYieldsfromFitdiagnostics(config, prefit = False):
    logging.info("Getting yields from FitDiagnostics file")
    rfile = ROOT.TFile.Open(config.fitFile)
    logging.debug("File: %s", rfile)
    channels = config.catLabels

    yields = {}
    for cat in config.categories:
        yields[cat] = {}
    
    if prefit:
        topfolder = "shapes_prefit"
    else:
        topfolder = "shapes_fit_s"

    #GetProcesses:
    processes = []
    print config.categories[0]
    dirForProcs = topfolder+"/"+config.catNamesFile[config.categories[0]]+"/"
    logging.debug("Getting precesses form %s",dirForProcs)
    for process in rfile.GetDirectory(dirForProcs).GetListOfKeys():
        processes.append(process.GetName())
    logging.debug("Processes in file: %s", processes)
    
    for cat in yields:
        for process in processes:
            if not process.startswith("total"):
                histo = rfile.Get(topfolder+"/"+config.catNamesFile[cat]+"/"+process)
                if histo == None:
                    logging.warning("Histogram for %s is NULL. Will set yield to 0", process)
                    yields[cat][process] = 0
                else:
                    yields[cat][process] = histo.Integral()
                logging.debug("%s | %s | %s | %s",cat, config.catNamesFile[cat], process, yields[cat][process])

    return yields


def getPieCharts(config, yields):
    """
    Function creation the pie charts for each category
    """
    pieCharts = {}
    SandB = {}
    for cat in config.categories:
        logging.debug("Making pie chart for cat: %s", cat)
        allSliceNames = []
        allSliceYields = []
        signalSum = 0
        for signalSlice in config.signalSlices:
            signalSum += yields[cat][signalSlice]
        allSliceNames.append("signal")
        allSliceYields.append(signalSum)
        
        uniqueSum = 0
        for uniqueSlice in config.uniqueSlices:
            allSliceNames.append(uniqueSlice)
            allSliceYields.append(yields[cat][uniqueSlice])
            uniqueSum += yields[cat][uniqueSlice]
        

        minorSum = 0
        for proc in yields[cat].keys():
            if proc not in config.uniqueSlices and proc not in config.signalSlices and proc != "data":
                minorSum += yields[cat][proc]
                logging.debug("Added yield for %s to minor slice", proc)
        allSliceNames.append("minor")
        allSliceYields.append(minorSum)

        chart = ROOT.TPie("Chart_"+cat, "Chart_"+cat, len(allSliceNames), array('f', allSliceYields))
        chart.SetTitle("")
        for iprocess, process in enumerate(allSliceNames):
            if process != "signal":
                chart.SetEntryLabel(iprocess,niceNames[process])
            else:
                if config.addMuToPlot:
                    chart.SetEntryLabel(iprocess,niceNames[process]+" (#mu = {0})".format(config.muForPlot))
                else:
                    chart.SetEntryLabel(iprocess,niceNames[process])
            chart.SetEntryFillColor(iprocess, colors[process])
            chart.SetEntryLineWidth(iprocess, 0)
            chart.SetLabelFormat("")
        leg = chart.MakeLegend()
        chart.SetRadius(chart.GetRadius()*0.8)
        pieCharts[cat] = chart, leg

        SandB[cat] = signalSum,minorSum+uniqueSum

    return deepcopy(pieCharts), SandB

def getCanvas(name, nCharts, width = 1300, height = 720, chartsTop = 3, chartsBottom = 3):
    """
    Function for creating the canvas that will house all pie charts
    """
    if nCharts > 6:
        raise RuntimeError("Only 6 charts per canvas are supported")
    if nCharts != 6:
        raise NotImplementedError()
    if chartsTop not in ["2","3"]:
        raise  RuntimeError("Only 3 or 2 plots in upper row possible: Passed {0}".format(chartsTop))
    if chartsBottom not in ["2","3"]:
        raise  RuntimeError("Only 3 or 2 plots in lower row possible: Passed {0}".format(chartsBottom))
    canvas = ROOT.TCanvas("PieChartCanvas_"+name, "PieChartCanvas_"+name, 1300, 720)
    canvas.Divide(3,2)
    if chartsTop == "3" and chartsBottom == "3":
        logging.debug("Making canvas with 6 pie charts")
        canvas.cd(1).SetPad(0,0.4,0.275,0.9)
        canvas.cd(2).SetPad(0.275,0.4,0.55,0.9)
        canvas.cd(3).SetPad(0.55,0.4,0.825,0.9)
        canvas.cd(4).SetPad(0,-0.075,0.275,0.425)
        canvas.cd(5).SetPad(0.275,-0.075,0.55,0.425)
        canvas.cd(6).SetPad(0.55,-0.075,0.825,0.425)

        canvas.cd(1).SetTopMargin(0.2)
        canvas.cd(2).SetTopMargin(0.2)
        canvas.cd(3).SetTopMargin(0.2)
        canvas.cd(4).SetBottomMargin(0)
        canvas.cd(5).SetBottomMargin(0)
        canvas.cd(6).SetBottomMargin(0)
    elif chartsTop == "2" and chartsBottom == "3":
        logging.debug("Making canvas with 5 pie charts. 2 on top")
        canvas.cd(1).SetPad(0,0.4,0.275,0.9)
        canvas.cd(2).SetPad(0.275,0.4,0.55,0.9)
        canvas.cd(6).SetPad(0.55,0.4,0.825,0.9)
        canvas.cd(3).SetPad(0,-0.075,0.275,0.425)
        canvas.cd(4).SetPad(0.275,-0.075,0.55,0.425)
        canvas.cd(5).SetPad(0.55,-0.075,0.825,0.425)

        canvas.cd(1).SetTopMargin(0.2)
        canvas.cd(2).SetTopMargin(0.2)
        canvas.cd(3).SetBottomMargin(0)
        canvas.cd(4).SetBottomMargin(0)
        canvas.cd(5).SetBottomMargin(0)
        canvas.cd(6).SetTopMargin(0.2)
        canvas.cd(6).SetFillStyle(4000)
        print canvas.cd(6)

        
    elif chartsTop == "3" and chartsBottom == "2":
        logging.debug("Making canvas with 5 pie charts. 2 on bottom")
        canvas.cd(1).SetPad(0,0.4,0.275,0.9)
        canvas.cd(2).SetPad(0.275,0.4,0.55,0.9)
        canvas.cd(3).SetPad(0.55,0.4,0.825,0.9)
        canvas.cd(4).SetPad(0,-0.075,0.275,0.425)
        canvas.cd(5).SetPad(0.275,-0.075,0.55,0.425)
        canvas.cd(6).SetPad(0.55,-0.075,0.825,0.425)

        canvas.cd(1).SetTopMargin(0.2)
        canvas.cd(2).SetTopMargin(0.2)
        canvas.cd(3).SetTopMargin(0.2)
        canvas.cd(4).SetBottomMargin(0)
        canvas.cd(5).SetBottomMargin(0)
        canvas.cd(6).SetBottomMargin(0)
    else:
        raise RuntimeError("Somthing went wrong with the plotsplitting. Check config")
        

    return canvas

def main(config, prefit = False, tag = "PlotName"):
    
    yields = getYieldsfromFitdiagnostics(config, prefit)

    charts, SandBs = getPieCharts(config, yields)
    
    nCats = len(config.categories)

    nCanvas = 1
    if nCats > 6:
        nCanvas += int(nCats)/6
        logging.debug("More than 6 cats defined in config. Will split multiple canvases")
        logging.debug("Number of canvases: %s", nCanvas)

    nCatsPlot = (int(nCats)/6)*[6]
    if nCanvas > 1:
        nCatsPlot += [nCats-6*(int(nCats)/6)]

    if not nCatsPlot:
        nCatsPlot = [nCats]
        
    iCat = 0
    #Save all labels in dicts otherwise only the last shows up in the plot
    title = {}
    SBlabel = {}
    for iCanvas in range(nCanvas):
        logging.info("Creating canvas %s",iCanvas)
        print nCatsPlot
        nTop, nBottom = config.plotSplitting[0], config.plotSplitting[1]
        thisCanvas = getCanvas(str(iCanvas), 6,chartsTop = nTop, chartsBottom = nBottom)
        for catNum in range(nCatsPlot[iCat]):
            cat = config.categories[iCat]
            thisCanvas.cd(catNum+1)
            thisChart, leg = charts[cat]
            thisChart.Draw("")
            logging.debug("Drawing %s", thisChart)
            pieTitle = "#bf{#scale[1.8]{"+config.catLabels[cat]+"}}"
            logging.debug("Using title %s for %s", pieTitle, cat)
            title[cat] = ROOT.TLatex(0.5, 0.93, pieTitle)
            title[cat].SetTextFont(42)
            title[cat].SetTextAlign(21)
            title[cat].Draw("same")
            if config.printSoverB:
                S, B = SandBs[cat]
                SBlabel[cat] = ROOT.TLatex(0.5, 0.8445, "#scale[1.3]{"+"S/B = {:5.4f}, S/#sqrt{{B}} = {:5.2f}".format(S/B,S/math.sqrt(B))+"}")
                SBlabel[cat].SetTextFont(42)
                SBlabel[cat].SetTextAlign(21)
                SBlabel[cat].Draw("same")
            #Add S/B
            iCat += 1
        thisCanvas.cd(0)
        cms = ROOT.TLatex( 0.03, 0.935, '#scale[1.2]{#scale[1.4]{#bf{CMS}} #it{Preliminary}}')
        #cms = ROOT.TLatex( 0.03, 0.935, '#scale[1.2]{#scale[1.4]{#bf{CMS}} #it{Supplementary}}')
        cms.SetTextFont(42)
        cms.Draw("same")

        if prefit:
            prefitLabel = ROOT.TLatex( 0.97, 0.947, 'Pre-fit expectation')
            prefitLabel.SetTextFont(42)
            prefitLabel.SetTextAlign(32)
            prefitLabel.Draw("same")

        if config.centerlabel is not None:
            logging.info("Drawing Center label %s", config.centerlabel)
            centerlabel = ROOT.TLatex( 0.5, 0.95, "#scale[1.2]{"+config.centerlabel+"}")
            centerlabel.SetTextFont(42)
            centerlabel.SetTextAlign(22)
            centerlabel.Draw("same")
            
        leg.SetX1(0.82)
        leg.SetY1(0.1)
        leg.SetX2(0.98)
        leg.SetY2(0.7)
        leg.SetBorderSize(0)
        leg.SetTextFont(42)
        leg.SetFillStyle(0)
        leg.Draw("same")

        thisCanvas.Update()
        thisCanvas.Print("Pie_{0}_{1}.pdf".format(tag, iCanvas))
        thisCanvas.Close()

        
if __name__ == "__main__":
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--logging",
        action = "store",
        help = "Define logging level: CRITICAL - 50, ERROR - 40, WARNING - 30, INFO - 20, DEBUG - 10, NOTSET - 0 \nSet to 0 to activate ROOT root messages",
        type=int,
        default=20
    )
    argumentparser.add_argument(
        "--config",
        action = "store",
        help = "Config defining the categories to be plotted",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--tag",
        action = "store",
        help = "Tag used for output file",
        type = str,
        required = True
    )
    argumentparser.add_argument(
        "--prefit",
        action = "store_true",
        help = "If passed prefit will be used",
    )
    
    args = argumentparser.parse_args()
    initLogging(args.logging)

    plotConfig = Config(args.config)

    tag = args.tag + "_prefit" if args.prefit else ""
    main(plotConfig, args.prefit, tag)
