import ROOT
import sys, os
sys.path.insert(0, os.path.abspath('..'))
import copy
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetErrorX(0) #turns of horizontal error bars (but also for bkg)
ROOT.gROOT.SetBatch(1)
import numpy as np
import Helper
from classes import ratios
from classes.PlotHelpers import saveCanvasListAsPDF
#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.INFO,
)
#############################################################
#############################################################

folderBase = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p1/CMSSW_9_4_9/src/TTH/FHDownstream/Plotting/HTReweighting/QCD_estimate_plots_v5/v5/"
inFileWeight = [
    folderBase + "check_7j_3b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_HTrewt_signal.root",
    folderBase + "check_7j_4b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_HTrewt_signal.root",
    folderBase + "check_8j_3b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_HTrewt_signal.root",
    folderBase + "check_8j_4b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_HTrewt_signal.root",
    folderBase + "check_9j_3b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_HTrewt_signal.root",
    folderBase + "check_9j_4b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_HTrewt_signal.root"]
inFileNoWeight = [
    folderBase + "check_7j_3b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root",
    folderBase + "check_7j_4b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root",
    folderBase + "check_8j_3b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root",
    folderBase + "check_8j_4b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root",
    folderBase + "check_9j_3b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root",
    folderBase + "check_9j_4b_SR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root"]

labels = ["#geq 7 jet, = 3 tags",
          "#geq 7 jet, #geq 4 tags",
          "#geq 8 jet, = 3 tags",
          "#geq 8 jet, #geq 4 tags",
          "#geq 9 jet, = 3 tags",
          "#geq 9 jet, #geq 4 tags"]

histosWeight = []
histosNoWeight = []


rFilesWeight = []
rFilesNoWeight = []

logging.info("Getting histogram w/ reweighting")
for ifile, file_ in enumerate(inFileWeight):
    rFilesWeight.append(ROOT.TFile(inFileWeight[ifile],"READ"))
    histosWeight.append(rFilesWeight[ifile].Get("ddQCD"))
logging.info("Getting histogram w/o reweighting")
for ifile, file_ in enumerate(inFileNoWeight):
    rFilesNoWeight.append(ROOT.TFile(inFileNoWeight[ifile],"READ"))
    histosNoWeight.append(rFilesNoWeight[ifile].Get("ddQCD"))

for histo in histosWeight:
    histo.SetLineColor(ROOT.kRed)
    histo.SetLineWidth(2)
    histo.SetMarkerStyle(1)
    histo.SetFillStyle(0)
    histo.GetYaxis().SetTitleOffset(histo.GetYaxis().GetTitleOffset()*1.2)
for histo in histosNoWeight:
    histo.SetLineColor(ROOT.kBlack)
    histo.SetLineWidth(2)
    histo.SetMarkerStyle(1)
    histo.SetFillStyle(0)
    histo.GetYaxis().SetTitleOffset(histo.GetYaxis().GetTitleOffset()*2)
    
logging.info("Starting with ratio plots")
allCanvases = []
for iPair in range(len(histosWeight)):
    logging.info("Processing pair %s", iPair)
    thisRatio = ratios.RatioPlot("ddHTReW_{0}".format(iPair))
    thisRatio.passHistos([histosNoWeight[iPair], histosWeight[iPair]])
    thisRatio.addCMSLabel(text="Prelim")
    thisRatio.addLabel(labels[iPair], xStart=0.65, yStart=0.9, scaleText=1.5)
    thisRatio.ratioRange = (0.7, 1.3)
    thisRatio.legendSize = (0.5,0.6,0.7,0.8)
    allCanvases.append( thisRatio.drawPlot(["Nominal","w/ HT reweighting"], xTitle="MEM discriminant") )

saveCanvasListAsPDF(allCanvases, "ddQCD_HTReweighting_comp",".")
                    


    
