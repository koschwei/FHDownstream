#!/usr/bin/env python
import ROOT, os, sys
sys.path.insert(0, os.path.abspath('..'))
from classes.PlotHelpers import moveOverUnderFlow
forCpp = True
file_3b = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p1/CMSSW_9_4_9/src/TTH/FHDownstream/Plotting/HTReweighting/QCD_estimate_plots_v5/v5/check_ge7j_3b_VR_6jHT_alphaCR_btagCorrDataMC_topPt_signal_pulls.root"
#check_ge7j_3b_VR_6jHT_R_alphaCR_btagCorrDataMC_kBest_signal.root"

file_4b = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p1/CMSSW_9_4_9/src/TTH/FHDownstream/Plotting/HTReweighting/QCD_estimate_plots_v5/v5/check_ge7j_4b_VR_6jHT_alphaCR_btagCorrDataMC_topPt_signal_pulls.root"
#check_ge7j_4b_VR_6jHT_R_alphaCR_btagCorrDataMC_kBest_signal.root"

f3b = ROOT.TFile.Open(file_3b)
ddQCD3b = f3b.Get("ddQCD")
data3b = f3b.Get("hdataVR")
ttbar3b = f3b.Get("httbarVR")
bkg3b = f3b.Get("hbkgVR")

moveOverUnderFlow(ddQCD3b, True, True)
moveOverUnderFlow(data3b, True, True)
moveOverUnderFlow(ttbar3b, True, True)
moveOverUnderFlow(bkg3b, True, True)

f4b = ROOT.TFile.Open(file_4b)
ddQCD4b = f4b.Get("ddQCD")
data4b = f4b.Get("hdataVR")
ttbar4b = f4b.Get("httbarVR")
bkg4b = f4b.Get("hbkgVR")

moveOverUnderFlow(ddQCD4b, True, True)
moveOverUnderFlow(data4b, True, True)
moveOverUnderFlow(ttbar4b, True, True)
moveOverUnderFlow(bkg4b, True, True)


for i in range(ddQCD3b.GetNbinsX()+2):
    nqcd3b = ddQCD3b.GetBinContent(i)
    ndata3b = data3b.GetBinContent(i)-ttbar3b.GetBinContent(i)-bkg3b.GetBinContent(i)
    nqcd4b = ddQCD4b.GetBinContent(i)
    ndata4b = data4b.GetBinContent(i)-ttbar4b.GetBinContent(i)-bkg4b.GetBinContent(i)
    edge = ddQCD3b.GetBinLowEdge(i+1)
    if nqcd3b == 0:
        nqcd3b = 1
    if nqcd4b == 0:
        nqcd4b = 1
    if forCpp:
        print "if(HT<%s){ if(threeb == 3){return %s;} else{return %s;} }"%(edge, ndata3b/nqcd3b, ndata4b/nqcd4b)
    else:
        print "elif HT<{0}: return {1:0.4f} if threeb else {2:0.4f}".format(edge, ndata3b/nqcd3b, ndata4b/nqcd4b)
    
