"""
Scripts for geeting the normalization factor for the qgWeight from the skimmed nTuples
"""
import sys, os
sys.path.insert(0, os.path.abspath('..'))
import ROOT, copy
ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetErrorX(0) #turns of horizontal error bars (but also for bkg)
ROOT.gROOT.SetBatch(0)
import numpy as np
import Helper
import json

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(0)

#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.INFO,
)
#############################################################
#############################################################

allDS = Helper.datasets
allCuts = Helper.get_cuts(DeltaEta = False)
idMap = Helper.sampleIDMap
version = "v5/bSF/"
folder = "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/"
fullpath = folder+version

rFiles = {}
trees = {}

ROOT.gROOT.LoadMacro("../classes.h")
ROOT.gInterpreter.ProcessLine("TriggerSF3D* internalTriggerSF3D = new TriggerSF3D()")
ROOT.gROOT.LoadMacro("../functions.h") #inlcude functions that can be used in TTree::Draw


for ds in allDS:
    if ds in ["data", "SingleMuon"]:
        continue
    logging.info("Getting RFile and TTree for %s", ds)
    logging.debug("Path: %s.root", fullpath+allDS[ds])
    rFiles[ds] = ROOT.TFile.Open(fullpath+allDS[ds]+".root")
    logging.info("Opening: %s",fullpath+allDS[ds]+".root")
    trees[ds] = rFiles[ds].Get("tree")

preSel = Helper.preselection
cuts = {
    "7j_3t_SR" : preSel + " && " + allCuts["7j"] + " && " + allCuts["3bSR"],
    "7j_4t_SR" : preSel + " && " + allCuts["7j"] + " && " + allCuts["4bSR"],
    "8j_3t_SR" : preSel + " && " + allCuts["8j"] + " && " + allCuts["3bSR"],
    "8j_4t_SR" : preSel + " && " + allCuts["8j"] + " && " + allCuts["4bSR"],
    "9j_3t_SR" : preSel + " && " + allCuts["9j"] + " && " + allCuts["3bSR"],
    "9j_4t_SR" : preSel + " && " + allCuts["9j"] + " && " + allCuts["4bSR"],
    "ge7j_3t_SR" : preSel + " && " + allCuts["ge7j"] + " && " + allCuts["3bSR"],
    "ge7j_4t_SR" : preSel + " && " + allCuts["ge7j"] + " && " + allCuts["4bSR"],
    #"ge8j_3t_SR" : preSel + " && " + allCuts["ge8j"] + " && " + allCuts["3bSR"],
    #"ge8j_4t_SR" : preSel + " && " + allCuts["ge8j"] + " && " + allCuts["4bSR"],

    
    "7j_3t_CR" : preSel + " && " + allCuts["7j"] + " && " + allCuts["3bCR"],
    "7j_4t_CR" : preSel + " && " + allCuts["7j"] + " && " + allCuts["4bCR"],
    "8j_3t_CR" : preSel + " && " + allCuts["8j"] + " && " + allCuts["3bCR"],
    "8j_4t_CR" : preSel + " && " + allCuts["8j"] + " && " + allCuts["4bCR"],
    "9j_3t_CR" : preSel + " && " + allCuts["9j"] + " && " + allCuts["3bCR"],
    "9j_4t_CR" : preSel + " && " + allCuts["9j"] + " && " + allCuts["4bCR"],
    "ge7j_3t_CR" : preSel + " && " + allCuts["ge7j"] + " && " + allCuts["3bCR"],
    "ge7j_4t_CR" : preSel + " && " + allCuts["ge7j"] + " && " + allCuts["4bCR"],
    #"ge8j_3t_CR" : preSel + " && " + allCuts["ge8j"] + " && " + allCuts["3bCR"],
    #"ge8j_4t_CR" : preSel + " && " + allCuts["ge8j"] + " && " + allCuts["4bCR"],


    "7j_3t_VR" : preSel + " && " + allCuts["7j"] + " && " + allCuts["3bVR"],
    "7j_4t_VR" : preSel + " && " + allCuts["7j"] + " && " + allCuts["4bVR"],
    "8j_3t_VR" : preSel + " && " + allCuts["8j"] + " && " + allCuts["3bVR"],
    "8j_4t_VR" : preSel + " && " + allCuts["8j"] + " && " + allCuts["4bVR"],
    "9j_3t_VR" : preSel + " && " + allCuts["9j"] + " && " + allCuts["3bVR"],
    "9j_4t_VR" : preSel + " && " + allCuts["9j"] + " && " + allCuts["4bVR"],
    "ge7j_3t_VR" : preSel + " && " + allCuts["ge7j"] + " && " + allCuts["3bVR"],
    "ge7j_4t_VR" : preSel + " && " + allCuts["ge7j"] + " && " + allCuts["4bVR"],
    #"ge8j_3t_VR" : preSel + " && " + allCuts["ge8j"] + " && " + allCuts["3bVR"],
    #"ge8j_4t_VR" : preSel + " && " + allCuts["ge8j"] + " && " + allCuts["4bVR"],


    
    "7j_3t_CR2" : preSel + " && " + allCuts["7j"] + " && " + allCuts["3bCR2"],
    "7j_4t_CR2" : preSel + " && " + allCuts["7j"] + " && " + allCuts["4bCR2"],
    "8j_3t_CR2" : preSel + " && " + allCuts["8j"] + " && " + allCuts["3bCR2"],
    "8j_4t_CR2" : preSel + " && " + allCuts["8j"] + " && " + allCuts["4bCR2"],
    "9j_3t_CR2" : preSel + " && " + allCuts["9j"] + " && " + allCuts["3bCR2"],
    "9j_4t_CR2" : preSel + " && " + allCuts["9j"] + " && " + allCuts["4bCR2"],
    "ge7j_3t_CR2" : preSel + " && " + allCuts["ge7j"] + " && " + allCuts["3bCR2"],
    "ge7j_4t_CR2" : preSel + " && " + allCuts["ge7j"] + " && " + allCuts["4bCR2"],
    #"ge8j_3t_CR2" : preSel + " && " + allCuts["ge8j"] + " && " + allCuts["3bCR2"],
    #"ge8j_4t_CR2" : preSel + " && " + allCuts["ge8j"] + " && " + allCuts["4bCR2"],

}

logging.debug("Building output dict")
allNorm = {}
for ds in allDS:
    allNorm[ds] = {}
    for j in ["7j","8j","9j","ge7j","ge8j"]:
        allNorm[ds][j] = {}
        for t in ["3t", "4t"]:
            allNorm[ds][j][t] = {}
            for r in ["SR", "CR", "VR", "CR2"]:
                allNorm[ds][j][t][r] = - 1

logging.debug("Getting weights")
baseWeight = Helper.get_weights(useQG = False)
fullWeight = Helper.get_weights(useQG = True)

hBase = ROOT.TH1F("h_ht30", "h_ht30", 50, 500, 2500)

for ds in allDS:
    if ds in ["data", "SingleMuon"]:
        continue
    logging.info("Processing DS: %s", ds)
    bNormID = idMap[ds]
    for cut in cuts:
        logging.info("Processing cut: %s", cut)
        logging.debug("Sel: %s", cuts[cut])
        logging.debug("BaseWei: %s", baseWeight)
        logging.debug("FullWei: %s", fullWeight)
        #Get yield w/o qgWeight
        hfullWeightName = hBase.GetName()+"_fullWeight_"+ds+"_"+cut
        hbaseWeightName = hBase.GetName()+"_baseWeight_"+ds+"_"+cut
        hfullW = hBase.Clone(hfullWeightName)
        hbaseW = hBase.Clone(hbaseWeightName)
        trees[ds].Project(hbaseWeightName, "ht30", "({0}) * ({1})".format(cuts[cut], baseWeight))
        #Get yield w/ qgWeight
        trees[ds].Project(hfullWeightName, "ht30", "({0}) * ({1})".format(cuts[cut], fullWeight))
        #GetRatio
        yield_noWeight = hbaseW.Integral()
        yield_weight = hfullW.Integral()
        if yield_weight <= 1e-5 or yield_noWeight <= 1e-5:
            logging.warning("Yields below threshold (%s, %s). Setting Ratio to 1",yield_noWeight,yield_weight)
            ratio = 1
        else:
            ratio = float(yield_noWeight)/float(yield_weight)
            logging.debug("Yield noweight = %s, weight = %s, ratio = %s", yield_noWeight, yield_weight, ratio)
        jetIndex, tagIndex, regionIndex = cut.split("_")

        allNorm[ds][jetIndex][tagIndex][regionIndex] = ratio
        
    
print json.dumps(allNorm, sort_keys=True,
                 indent=4, separators=(',', ': '))

v4out = version.replace("/","-")
if v4out.endswith("-"):
    v4out = v4out[:-1] 
with open("qgNorm_wDetaj_"+v4out+".json", "w") as o:
    json.dump(allNorm, o, sort_keys=True,
              indent=4, separators=(',', ': '))
