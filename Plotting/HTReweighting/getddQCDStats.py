import ROOT


path = "QCD_estimate_plots_v5/v5/"

files = {
    "7j3t" : "check_7j_3b_VR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root",
    "7j4t" : "check_7j_4b_VR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root",
    "8j3t" : "check_8j_3b_VR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root",
    "8j4t" : "check_8j_4b_VR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root",
    "9j3t" : "check_9j_3b_VR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root",
    "9j4t" : "check_9j_4b_VR_mem_ratio_R_alphaCR_btagCorrDataMC_topPt_signal.root",
    }

cats = [
    "7j3t",
    "7j4t",
    "8j3t",
    "8j4t",
    "9j3t",
    "9j4t",
]

output = {}
nBins = []
for key in files:
    rFile = ROOT.TFile.Open(path+files[key])
    ddQCD = rFile.Get("ddQCD")
    output[key] = {}
    nBins.append(ddQCD.GetNbinsX()+2)
    for ibin in range(ddQCD.GetNbinsX()+2):
        #print ibin, ddQCD.GetBinContent(ibin), ddQCD.GetBinError(ibin)
        output[key][ibin] = (
            ddQCD.GetBinContent(ibin),
            ddQCD.GetBinError(ibin)
        )


header = ""
for key in cats:
    header += "& \\multicolumn{3}{c}{%s}"%key
header += "\\\\"
lines = []
for ibin in range(max(nBins)):
    line = "{0}".format(ibin)
    for key in cats:
        entryies, err = output[key][ibin]
        line += "& {0:6.2f} & $\\pm$ &  {1:6.2f}".format(entryies, err)
    line += "\\\\"
    lines.append(line)

print header
for line in lines:
    print line

        
    
