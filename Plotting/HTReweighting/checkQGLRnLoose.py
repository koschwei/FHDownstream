import os
import sys
sys.path.insert(0, os.path.abspath('..'))
import ROOT, copy
import Helper
import logging
from classes.PlotHelpers import makeCanvasOfHistos, makeCanvas2DwCorrelation, moveOverUnderFlow, saveCanvasListAsPDF, initLogging, project
from classes.ratios import RatioPlot

initLogging(10)

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)


jets = ["7j","8j","9j"] #run all 6 categories
bjets = ["3b","4b"]
hBase = ROOT.TH1F("hBase", "hBase", 5,  2 , 7)
allCuts = Helper.get_cuts()
preSel = Helper.preselection + "&& numJets >= 7"
eventWeight = Helper.get_weights(triggerType = "tree")
qgWeights = Helper.get_QGNormaliziation()

bCut_3l = "nBDeepCSVM == 2 && nBDeepCSVL == 3"
bCut_4l = "nBDeepCSVM == 2 && nBDeepCSVL >= 4 "
bCut_3m = "nBDeepCSVM == 3 "
bCut_4m = "nBDeepCSVM >= 4 "

inFile = "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root"
datasetName = "TTbar_fh"
eventWeight = Helper.get_weights(triggerType = "tree")
qgWeights = Helper.get_QGNormaliziation()


# inFile = "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/data.root"
# datasetName = "data"
# eventWeight = "1"
            
rFile = ROOT.TFile.Open(inFile)
tree = rFile.Get("tree")

hHigh_4b_3l = hBase.Clone("hHigh_4b_3l")
hLow_4b_3l = hBase.Clone("hLow_4b_3l")
hHigh_3b_3l = hBase.Clone("hHigh_3b_3l")
hLow_3b_3l = hBase.Clone("hLow_3b_3l")

hHigh_4b_4l = hBase.Clone("hHigh_4b_4l")
hLow_4b_4l = hBase.Clone("hLow_4b_4l")
hHigh_3b_4l = hBase.Clone("hHigh_3b_4l")
hLow_3b_4l = hBase.Clone("hLow_3b_4l")

hHigh_4b_3m = hBase.Clone("hHigh_4b_3m")
hLow_4b_3m = hBase.Clone("hLow_4b_3m")
hHigh_3b_3m = hBase.Clone("hHigh_3b_3m")
hLow_3b_3m = hBase.Clone("hLow_3b_3m")

hHigh_4b_4m = hBase.Clone("hHigh_4b_4m")
hLow_4b_4m = hBase.Clone("hLow_4b_4m")
hHigh_3b_4m = hBase.Clone("hHigh_3b_4m")
hLow_3b_4m = hBase.Clone("hLow_3b_4m")



project(tree, "hHigh_4b_3l", "nBDeepCSVM", "{0} && {1} && qg_LR_4b_flavour_5q_0q>0.5".format(preSel, bCut_3l), eventWeight,"High 4b")
project(tree, "hLow_4b_3l", "nBDeepCSVM", "{0} && {1} && qg_LR_4b_flavour_5q_0q<0.5".format(preSel, bCut_3l), eventWeight,"Low 4b")
project(tree, "hHigh_3b_3l", "nBDeepCSVM", "{0} && {1} && qg_LR_3b_flavour_5q_0q>0.5".format(preSel, bCut_3l), eventWeight,"High 3b")
project(tree, "hLow_3b_3l", "nBDeepCSVM", "{0} && {1} && qg_LR_3b_flavour_5q_0q<0.5".format(preSel, bCut_3l), eventWeight,"Low 3b")

project(tree, "hHigh_4b_4l", "nBDeepCSVM", "{0} && {1} && qg_LR_4b_flavour_5q_0q>0.5".format(preSel, bCut_4l), eventWeight,"High 4b")
project(tree, "hLow_4b_4l", "nBDeepCSVM", "{0} && {1} && qg_LR_4b_flavour_5q_0q<0.5".format(preSel, bCut_4l), eventWeight,"Low 4b")
project(tree, "hHigh_3b_4l", "nBDeepCSVM", "{0} && {1} && qg_LR_3b_flavour_5q_0q>0.5".format(preSel, bCut_4l), eventWeight,"High 3b")
project(tree, "hLow_3b_4l", "nBDeepCSVM", "{0} && {1} && qg_LR_3b_flavour_5q_0q<0.5".format(preSel, bCut_4l), eventWeight,"Low 3b")

project(tree, "hHigh_4b_3m", "nBDeepCSVM", "{0} && {1} && qg_LR_4b_flavour_5q_0q>0.5".format(preSel, bCut_3m), eventWeight,"High 4b")
project(tree, "hLow_4b_3m", "nBDeepCSVM", "{0} && {1} && qg_LR_4b_flavour_5q_0q<0.5".format(preSel, bCut_3m), eventWeight,"Low 4b")
project(tree, "hHigh_3b_3m", "nBDeepCSVM", "{0} && {1} && qg_LR_3b_flavour_5q_0q>0.5".format(preSel, bCut_3m), eventWeight,"High 3b")
project(tree, "hLow_3b_3m", "nBDeepCSVM", "{0} && {1} && qg_LR_3b_flavour_5q_0q<0.5".format(preSel, bCut_3m), eventWeight,"Low 3b")

project(tree, "hHigh_4b_4m", "nBDeepCSVM", "{0} && {1} && qg_LR_4b_flavour_5q_0q>0.5".format(preSel, bCut_4m), eventWeight,"High 4b")
project(tree, "hLow_4b_4m", "nBDeepCSVM", "{0} && {1} && qg_LR_4b_flavour_5q_0q<0.5".format(preSel, bCut_4m), eventWeight,"Low 4b")
project(tree, "hHigh_3b_4m", "nBDeepCSVM", "{0} && {1} && qg_LR_3b_flavour_5q_0q>0.5".format(preSel, bCut_4m), eventWeight,"High 3b")
project(tree, "hLow_3b_4m", "nBDeepCSVM", "{0} && {1} && qg_LR_3b_flavour_5q_0q<0.5".format(preSel, bCut_4m), eventWeight,"Low 3b")


nHigh_4b_3l = hHigh_4b_3l.Integral()
nLow_4b_3l = hLow_4b_3l.Integral()
nHigh_3b_3l = hHigh_3b_3l.Integral()
nLow_3b_3l = hLow_3b_3l.Integral()

nHigh_4b_4l = hHigh_4b_4l.Integral()
nLow_4b_4l = hLow_4b_4l.Integral()
nHigh_3b_4l = hHigh_3b_4l.Integral()
nLow_3b_4l = hLow_3b_4l.Integral()

nHigh_4b_3m = hHigh_4b_3m.Integral()
nLow_4b_3m = hLow_4b_3m.Integral()
nHigh_3b_3m = hHigh_3b_3m.Integral()
nLow_3b_3m = hLow_3b_3m.Integral()

nHigh_4b_4m = hHigh_4b_4m.Integral()
nLow_4b_4m = hLow_4b_4m.Integral()
nHigh_3b_4m = hHigh_3b_4m.Integral()
nLow_3b_4m = hLow_3b_4m.Integral()


logging.info("==========================================================0")
logging.info("          \t 3L \t 4L \t 3M \t 4M")
logging.info("High QGLR \t %s \t %s \t %s \t %s",nHigh_3b_3l, nHigh_4b_4l, nHigh_3b_3m, nHigh_4b_4m)
logging.info(" Low QGLR \t %s \t %s \t %s \t %s",nLow_3b_3l, nLow_4b_4l, nLow_3b_3m, nLow_4b_4m)
logging.info("==========================================================0")


# thesePlots = []

# plot4b = RatioPlot("RatioPlot_4b")
# plot4b.useDefaultColors = True
# plot4b.ratioText = "Pulls"
# plot4b.ratioRange = (-5, 5)
# #plot4b.ratioRange = (0.2, 1.8)
# plot4b.addLabel("QGLR (4b) for nBDeepCSVM = 2", xStart = 0.15, yStart = 0.95)
# plot4b.passHistos([hHigh_4b, hLow_4b], normalize = True)
# thesePlots.append(
#     plot4b.drawPlot(
#         legendText = ["High QGLR", "Low QGLR"],
#         xTitle = "Number of loose b-tags",
#         drawPulls = True,
#     )
# )
    
# plot3b = RatioPlot("RatioPlot_3b")
# plot3b.useDefaultColors = True
# plot3b.ratioText = "Pulls"
# plot3b.ratioRange = (-5, 5)
# #plot3b.ratioRange = (0.2, 1.8)
# plot3b.addLabel("QGLR (3b) for nBDeepCSVM = 2", xStart = 0.15, yStart = 0.95)
# plot3b.passHistos([hHigh_3b, hLow_3b], normalize = True)
# thesePlots.append(
#     plot3b.drawPlot(
#         legendText = ["High QGLR", "Low QGLR"],
#         xTitle = "Number of loose b-tags",
#         drawPulls = True,
#     )
# )

        
# saveCanvasListAsPDF(thesePlots, "nLoosebyQGLR_pulls_"+datasetName, ".")
