import os
import sys
sys.path.insert(0, os.path.abspath('..'))
import ROOT, copy
import Helper
import logging
from classes.PlotHelpers import makeCanvasOfHistos, makeCanvas2DwCorrelation, moveOverUnderFlow, saveCanvasListAsPDF, initLogging, project
from classes.ratios import RatioPlot

initLogging(20)

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)


jets = ["7j","8j","9j"] #run all 6 categories
bjets = ["3b","4b"]
hBase = ROOT.TH1F("hBase", "hBase", 4,  3 , 7)
allCuts = Helper.get_cuts()
preSel = Helper.preselection
eventWeight = Helper.get_weights(triggerType = "tree")
qgWeights = Helper.get_QGNormaliziation()


inFile = "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/bSF/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root"
datasetName = "TTbar_fh"
eventWeight = Helper.get_weights(triggerType = "tree")
qgWeights = Helper.get_QGNormaliziation()


inFile = "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v5/data.root"
datasetName = "data"
eventWeight = "1"
qgWeights = Helper.get_QGNormaliziation()
for jc in qgWeights[datasetName]:
    for bc in qgWeights[datasetName][jc]:
        for r in qgWeights[datasetName][jc][bc]:
            qgWeights[datasetName][jc][bc][r] = 1


rFile = ROOT.TFile.Open(inFile)
tree = rFile.Get("tree")


for jc in jets:
    logging.info("Processing jet cat: %s", jc)
    jetCut = allCuts[jc]
    for bc in bjets:
        thesePlots = []
        
        logging.info("Processing bTag cat: %s", bc)
        VRCut = allCuts[bc+"VR"]
        SRCut = allCuts[bc+"SR"]
        CR2Cut = allCuts[bc+"CR2"]
        CRCut = allCuts[bc+"CR"]


        
        hSR = hBase.Clone("hSR_"+jc+"_"+bc)
        hVR = hBase.Clone("hVR_"+jc+"_"+bc)
        hCR = hBase.Clone("hCR_"+jc+"_"+bc)
        hCR2 = hBase.Clone("hCR2_"+jc+"_"+bc)

        
        project(tree, "hSR_"+jc+"_"+bc, "nBDeepCSVL", "{0} && {1} && {2}".format(preSel, jetCut, SRCut), eventWeight+"*"+str(qgWeights[datasetName][jc][bc.replace("b","t")]["SR"]),"SR")
        project(tree, "hVR_"+jc+"_"+bc, "nBDeepCSVL", "{0} && {1} && {2}".format(preSel,jetCut, VRCut), eventWeight+"*"+str(qgWeights[datasetName][jc][bc.replace("b","t")]["VR"]),"VR")
        #project(tree, "hCR_"+jc+"_"+bc, "nBDeepCSVL", "{0} && {1} && {2}".format(preSel,jetCut, CRCut), eventWeight+"*"+str(qgWeights[datasetName][jc][bc.replace("b","t")]["CR"]),"CR")
        #project(tree, "hCR2_"+jc+"_"+bc, "nBDeepCSVL", "{0} && {1} && {2}".format(preSel,jetCut, CR2Cut), eventWeight+"*"+str(qgWeights[datasetName][jc][bc.replace("b","t")]["CR2"]),"CR2")

        
        hErrorBandSR = hSR.Clone(hSR.GetName()+"_ErrBand")
        hErrorBandVR = hVR.Clone(hVR.GetName()+"_ErrBand")
        hErrorBandCR = hCR.Clone(hCR.GetName()+"_ErrBand")
        hErrorBandCR2 = hCR2.Clone(hCR2.GetName()+"_ErrBand")


        hErrorBandSR.SetFillColor(ROOT.kGray+2)
        hErrorBandSR.SetFillStyle(3344)
        hErrorBandVR.SetFillColor(ROOT.kGray+2)
        hErrorBandVR.SetFillStyle(3344)
        hErrorBandCR.SetFillColor(ROOT.kGray+2)
        hErrorBandCR.SetFillStyle(3344)
        hErrorBandCR2.SetFillColor(ROOT.kGray+2)
        hErrorBandCR2.SetFillStyle(3344)
        
        
        SRPlot = RatioPlot("RatioPlot_SR_"+jc+"_"+bc)
        SRPlot.useDefaultColors = True
        SRPlot.ratioText = "#frac{SR}{VR}"
        SRPlot.ratioText = "Pulls"
        SRPlot.ratioRange = (-6, 6)
        SRPlot.addLabel(jc+bc, xStart = 0.15, yStart = 0.95)
        SRPlot.passHistos([hSR, hVR], normalize = True)
        thesePlots.append(
            SRPlot.drawPlot(
                legendText = ["SR", "VR"],
                xTitle = "Number of loose b-tags",
                errorband = hErrorBandSR,
                drawPulls = True,
            )
        )


        # CRPlot = RatioPlot("RatioPlot_CR_"+jc+"_"+bc)
        # CRPlot.useDefaultColors = True
        # CRPlot.ratioText = "#frac{CR}{CR2}"
        # CRPlot.addLabel(jc+bc, xStart = 0.15, yStart = 0.95)
        # CRPlot.passHistos([hCR, hCR2], normalize = True)
        # thesePlots.append(
        #     CRPlot.drawPlot(
        #         legendText = ["CR", "CR2"],
        #         xTitle = "Number of loose b-tags",
        #         errorband = hErrorBandCR,
        #         drawPulls = True,
        #     )
        # )
        
        saveCanvasListAsPDF(thesePlots, "compDist_pulls_"+jc+"_"+bc+"_"+datasetName, ".")
