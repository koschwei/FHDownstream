import ROOT
from glob import glob
from copy import deepcopy
def saveCanvasListAsPDF(listofCanvases, outputfilename, foldername):
    #logging.info("Writing outputfile %s.pdf",outputfilename)
    for icanvas, canves in enumerate(listofCanvases):
        if icanvas == 0:
            canves.Print(foldername+"/"+outputfilename+".pdf(")
        elif icanvas == len(listofCanvases)-1:
            canves.Print(foldername+"/"+outputfilename+".pdf)")
        else:
            canves.Print(foldername+"/"+outputfilename+".pdf")

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

folder = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p1/CMSSW_9_4_9/src/TTH/FHDownstream/utils/bTagNormv4Prev3/"
filterNames = ["ttH", "TTTo"]


allFiles = glob(folder+"*")
filteredFiles = []
for filter_ in filterNames:
    filteredFiles += filter(lambda x: filter_ in x, allFiles)
colors = [ROOT.kBlack, ROOT.kRed, ROOT.kBlue, ROOT.kGreen-2, ROOT.kOrange, ROOT.kAzure, ROOT.kSpring]
    
histos = {}
names = []
rFiles = []

for ifile, file_ in enumerate(filteredFiles):
    print file_
    rFiles.append(ROOT.TFile(file_, "READ"))
    names = map(lambda x: x.GetName(), list(rFiles[ifile].GetListOfKeys()))
    histos[file_] = {}
    for name in names:
        histos[file_][name] = rFiles[ifile].Get(name)

allCanvases = []
for name in names:
    canvas = ROOT.TCanvas("c"+name,"c"+name, 640, 580) 
    canvas.SetTopMargin(0.08*.75)
    canvas.SetRightMargin(0.04)
    canvas.SetLeftMargin(0.11)
    canvas.SetBottomMargin(0.12)

    leg = ROOT.TLegend(0.15,0.15,0.5,0.35)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.05)

    line = ROOT.TF1("line","1",0.0,9000.0)
    line.SetLineColor(12) #Grey
    line.SetLineWidth(1)
    line.SetLineStyle(2) #Dashe
    
    
    for ifile, file_ in enumerate(filteredFiles):
        histos[file_][name].SetLineColor(colors[ifile])
        if ifile == 0:
            histos[file_][name].SetTitle(name)
            histos[file_][name].GetYaxis().SetRangeUser(0.8, 1.05)
            histos[file_][name].Draw("histoe")
        else:
            histos[file_][name].Draw("histoesame")
        leg.AddEntry(histos[file_][name], file_.split("/")[-1].split("_")[1])

    leg.Draw("SAME")
    line.Draw("SAME")
    canvas.Update()
    allCanvases.append(deepcopy(canvas))

saveCanvasListAsPDF(allCanvases, "bTagNorm", ".")
                  






