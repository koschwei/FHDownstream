import logging
from copy import deepcopy
from glob import glob
import ROOT

from classes.ratios import RatioPlot
from classes.roc import ROC
from classes.PlotHelpers import saveCanvasListAsPDF,makeCanvasOfHistos, initLogging
import Helper

ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

ROOT.gROOT.LoadMacro("classes.h")
ROOT.gInterpreter.ProcessLine("TriggerSF3D* internalTriggerSF3D = new TriggerSF3D()")
ROOT.gROOT.LoadMacro("functions.h") #inlcude functions that can be used in TTree::Draw


def getFitDiagnosticsHistos(filename, postfit, cat, variable):
    rfile = ROOT.TFile.Open(filename)

    FitDiagnosticsChannels = {
        "j7t3" : "ttH_hbb_13TeV_2017_fh_7j3b_MEM",
        "j7t4" : "ttH_hbb_13TeV_2017_fh_7j4b_MEM",
        "j8t3" : "ttH_hbb_13TeV_2017_fh_8j3b_MEM",
        "j8t4" : "ttH_hbb_13TeV_2017_fh_8j4b_MEM",
        "j9t3" : "ttH_hbb_13TeV_2017_fh_9j3b_MEM",
        "j9t4" : "ttH_hbb_13TeV_2017_fh_9j4b_MEM"
    }
    histos = {}

    folder = "{0}__{1}".format(cat, variable)
    
    if postfit:
        topfolder = "shapes_fit_s"
    else:
        topfolder = "shapes_prefit"

    logging.info("FILE: %s", rfile)
    logging.info("Folder: %s", folder)
    #GetProcesses:
    processes = []
    for process in rfile.GetDirectory(topfolder+"/{0}/".format(folder)).GetListOfKeys():
        processes.append(process.GetName())
    print processes

    logging.info("Getting histograms for %s", cat)
    for process in processes:
        logging.debug("Getting histogram for %s/%s", cat, process)
        histos[process] = deepcopy(rfile.Get(topfolder+"/"+folder+"/"+process))

    logging.info("Getting %s for total w/ errors", topfolder+"/total_background")
    histos["total_background_errors"] = deepcopy(rfile.Get(topfolder+"/total_background"))
    # for cat in histos:
    #     for process in processes:
    #         histos[cat][process].SetSum
    return histos

def getBinning(variable):
    if variable == "ht30":
        return 10,500,1500
    elif variable == "jetsByPt_0_pt":
        return 11,50,600
    elif variable == "min_dr_btag":
        return 9,0.3,3
    elif variable == "mass_drpair_btag":
        return 9,30,300
    elif variable == "mass_drpair_untag":
        return 11,30,140
    elif variable == "mjjmin":
        return 12,20,80
    elif "mem" in variable:
        return 10,0,1
    else:
        return None


def getNiceName(variable):
    if variable == "ht30":
        return "#it{H}_{T} (GeV)"
    elif variable == "6jHT":
        return "6 jet #it{H}_{T} (GeV)"
    elif variable == "jets_pt[0]" or variable == "jetsByPt_0_pt" :
        return "Leading jet #it{p}_{T} (GeV)"
    elif variable == "jets_eta[0]" :
        return "Leading jet #eta"
    elif variable == "jets_qgl[0]" :
        return "Leading jet QGL"
    elif "mem" in variable:
        return "MEM discriminant"
    elif variable == "jets_qgl[0]":
        return "Highest jet QGL"
    elif variable == "qg_LR_4b_flavour_5q_0q":
        return "QGLR (4b)"
    elif variable == "mass_drpair_btag":
        return "Mass bb(#DeltaR_{min}) (GeV)"
    elif variable == "mjjmin":
        return "Min jj mass (GeV)"
    elif variable == "min_dr_btag":
        return "#DeltaR(bb)_{min}"
    else:
        return variable

def getYTitle(variable):
    if variable == "ht30":
        return "Events / 100 GeV"
    elif variable == "6jHT":
        return "Events / 100 GeV"
    elif variable == "jets_pt[0]" or variable == "jetsByPt_0_pt":
        return "Events / 50 GeV"
    elif variable == "jets_eta[0]" :
        return "Events / 0.25 units"
    elif variable == "jets_qgl[0]" :
        return "Events / 0.05 units"
    elif "mem" in variable:
        return "Events / 0.1 units"
    elif variable == "jets_qgl[0]":
        return "Events / 0.05 units"
    elif variable == "qg_LR_4b_flavour_5q_0q":
        return "Events / 0.05 units"
    elif variable == "mass_drpair_btag":
        return "Events / 30 GeV"
    elif variable == "mjjmin":
        return "Events / 5 GeV"
    elif variable == "min_dr_btag":
        return "Events / 0.3 units"
    else:
        return "Events"


def getCatLabel(cat):
    if cat == "fh_j9_t3":
        return "FH VR (9 jets, 3 b tags)"
    elif cat == "fh_j8_t4":
        return "FH VR (8 jets, #geq 4 b tags)"
    else:
        return "Some Label!"
    
def getBinLabels(variable):
    binning =  getBinning(variable)
    if binning is None:
        return None
    else:
        nBins, firstBin, lastBin = binning
        labels = [firstBin]
        diff = (lastBin-firstBin)/nBins
        for ibin in range(nBins):
            labels.append(firstBin+ibin*diff)
        labels.append(lastBin)
        return labels


def transvertoHwBinning(variable, inHisto, nBins, firstBin, lastBin, title):
    inName = inHisto.GetName()
    hWBinning = ROOT.TH1F("hWBinning_"+inName+"_"+variable, "hWBinning_"+inName+"_"+variable, nBins, firstBin, lastBin)
    hWBinning.GetXaxis().SetTitle(title)
    hWBinning.GetYaxis().SetLabelSize(0.045)
    for ibin in range(1, inHisto.GetNbinsX()+1):
        hWBinning.SetBinContent(ibin, inHisto.GetBinContent(ibin))
        hWBinning.SetBinError(ibin, inHisto.GetBinError(ibin))

    return hWBinning
    
def makeValidationPlots(histos, outPostfix, processCat, variables, drawPulls = False, saveROOTObjects = False, saveMacro = False):
    minorBackgrounds = ["singlet", "wjets", "zjets", "ttbarW", "ttbarZ"]
    ttbarBackgrounds  = ["ttbarPlusBBbar","ttbarPlusB","ttbarPlus2B","ttbarPlusCCbar","ttbarOther"]
    
    for variable in variables:

        for ihisto, minorBackground in enumerate(minorBackgrounds):
            if ihisto == 0:
                hMinor = histos[variable][minorBackground].Clone("hMinor_"+variable)
            else:
                hMinor.Add(histos[variable][minorBackground])

        for ihisto, ttbarBackground in enumerate(ttbarBackgrounds):
            if ihisto == 0:
                httbar = histos[variable][ttbarBackground].Clone("httbar_"+variable)
            else:
                httbar.Add(histos[variable][ttbarBackground])

        hQCD = histos[variable]["ddQCD"].Clone("hQCD_"+variable)

        
        errorband = histos[variable]["total_background_errors"].Clone("Errorband_"+variable)
        

        hTotal = histos[variable]["total_background_errors"].Clone("hTotal_"+variable)

        binContents = {}
        gData = histos[variable]["data"]
        for i in range(gData.GetN()):
            x,y = ROOT.Double(0),ROOT.Double(0)
            gData.GetPoint(i, x, y)
            binContents[i] = (y,max(gData.GetErrorYlow(i), gData.GetErrorYhigh(i)) )
        hData = hQCD.Clone("hdata")
        for iBin in range(1,hData.GetNbinsX()+1):
            content, error = binContents[iBin-1]
            hData.SetBinContent(iBin, content)
            hData.SetBinError(iBin, error)

        
        binning = getBinning(variable)
        if binning is not None:
            nBins, firstBin, lastBin = binning
            hTotal = transvertoHwBinning(variable, hTotal,  nBins, firstBin, lastBin, getNiceName(variable))
            errorband = transvertoHwBinning(variable, errorband,  nBins, firstBin, lastBin, getNiceName(variable))
            hQCD = transvertoHwBinning(variable, hQCD,  nBins, firstBin, lastBin, getNiceName(variable))
            httbar = transvertoHwBinning(variable, httbar,  nBins, firstBin, lastBin, getNiceName(variable))
            hMinor = transvertoHwBinning(variable, hMinor,  nBins, firstBin, lastBin, getNiceName(variable))
            hData = transvertoHwBinning(variable, hData,  nBins, firstBin, lastBin, getNiceName(variable))

        hTotal.SetLineColor(ROOT.kBlack)

        errorband.SetFillColor(ROOT.kBlack)
        errorband.SetLineColor(ROOT.kBlack)
        errorband.SetFillStyle(3645)

        hMinor.SetLineColor(ROOT.kBlack)
        hMinor.SetFillColor(ROOT.kAzure+8)
        hMinor.SetFillStyle(1001)
        
        httbar.SetLineColor(ROOT.kBlack)
        httbar.SetFillColor(ROOT.kRed+2)
        httbar.SetFillStyle(1001)

        hQCD.SetLineColor(ROOT.kBlack)
        hQCD.SetFillColor(Helper.colors["ddQCD"])
        hQCD.SetFillStyle(1001)

        hData.SetLineColor(ROOT.kBlack)
        hData.SetMarkerColor(ROOT.kBlack)
        hData.SetMarkerStyle(20)
        hData.SetMarkerSize(1)
       
        legendObjwText = []
        legendObjwText.append((hMinor, "Minor bkg"))#
        legendObjwText.append((httbar, "t#bar{t}+jets"))#
        legendObjwText.append((hQCD, "Multijet"))#
        legendObjwText.append((errorband, "Uncertainty"))#

        stack = ROOT.THStack("stack_"+variable, "stack_"+variable)
        stack.Add(hQCD)
        stack.Add(httbar)
        stack.Add(hMinor)

        finalOutput = []
        output = RatioPlot(name = "Plot_"+variable)
        output.CMSscale = (1.4, 1.4)
        output.legendSize = (0.65, 0.45, 0.95, 0.76)
        output.invertRatio = True
        if drawPulls:
            output.ratioRange = (-5,5)
        else:
            output.ratioRange = (0.81,1.19)
        output.yTitleOffset = 0.99
        output.yTitleOffsetRatio = 0.45
        output.yTitle = getYTitle(variable)
        output.labelScaleX = 1.3
        output.moveCMS = -0.01
        output.moveCMSTop = -0.01
        output.xTitleOffsetScale = 1.3
        fullLabel = getCatLabel(processCat) + "  Pre-fit expectation"
        output.addLabel(fullLabel, 0.12, 0.95)
        #output.addLabel("Prefit", 0.2, 0.8)
        output.passHistos([hData, stack])
        finalOutput.append(
            output.drawPlot(
                None,
                getNiceName(variable),
                isDataMCwStack = True,
                stacksum=hTotal,
                errorband = errorband,
                drawCMS = True,
                histolegend = legendObjwText,
                drawPulls = drawPulls,
                floatingMax = 1.23
            )
        )

        outfileName = "VR_fitDiagnostics_{0}{1}".format(variable,outPostfix)

        
        if saveROOTObjects:
            outFile = ROOT.TFile("{0}.root".format(outfileName), "RECREATE")
            hQCD.Write()
            httbar.Write()
            hMinor.Write()
            for obj in output.getROOTObjects():
                obj.Write()
            for c in finalOutput:
                c.Write()
            outFile.Close()

        if saveMacro:
            output.canvas.Print("{0}.C".format(outfileName))
                        
        saveCanvasListAsPDF(finalOutput, outfileName,".")



    
if __name__ == "__main__":
    fitDiagnostics = "/mnt/t3nfs01/data01/shome/koschwei/scratch/datacards/HIG-18-030/ResFHforAN/FitDiagnostics_Asimov1p0_PullsAndShapesWithUncs/fitDiagnostics_2017_fh.root"
    import argparse
    ##############################################################################################################
    ##############################################################################################################
    # Argument parser definitions:
    argumentparser = argparse.ArgumentParser(
        description='Description'
    )
    argumentparser.add_argument(
        "--loglevel",
        action = "store",
        type = int,
        default = 20,
        choices = [10,20,30,40,50],
        help = "Set the tag used for the filename",
    )
    argumentparser.add_argument(
        "--vars",
        action = "store",
        type = str,
        nargs = "+",
        required = True,
        help = "Variables to be plotted",
    )
    argumentparser.add_argument(
        "--path",
        action = "store",
        type = str,
        required = True,
        help = "input file ",
    )
    argumentparser.add_argument(
        "--cat",
        action = "store",
        type = str,
        required = True,
        choices = ["fh_j7_t3","fh_j8_t3","fh_j9_t3","fh_j7_t4","fh_j8_t4","fh_j9_t4"],
        help = "Scale for signal histogram in yield plot",
    )
    argumentparser.add_argument(
        "--pulls",
        action = "store_true",
        help = "If set ratio will contain pulls",
    )
    argumentparser.add_argument(
        "--saveHistos",
        action = "store_true",
        help = "All histograms and the canvas will be saved in a root file",
    )
    argumentparser.add_argument(
        "--saveMacro",
        action = "store_true",
        help = "All histograms and the canvas will be saved in a root file",
    )

    args = argumentparser.parse_args()
    ##############################################################################################################
    initLogging(args.loglevel)
    logging.info("Starting script")
    histos = {}

    invertedcat = args.cat.split("_")
    invertedcat = invertedcat[0]+"_"+invertedcat[1][1]+invertedcat[1][0]+"_"+invertedcat[2][1]+invertedcat[2][0]

    fitDiagnosticFiles = {}
    for f in glob(args.path+"/fitDiagnostics*"):
        if not (args.cat.replace("fh_","") in f or invertedcat.replace("fh_","") in f or invertedcat.replace("fh_","").replace("_","") in f):
            print f, args.cat.replace("fh_",""), invertedcat.replace("fh_","")
            continue
        for var in args.vars:
            if var in f:
                logging.info("Found file: %s", f)
                fitDiagnosticFiles[var] = f

    
    
    for var in args.vars:
        histos[var] = getFitDiagnosticsHistos(fitDiagnosticFiles[var], postfit = False, cat = args.cat, variable = var)
    postfix = "_prefit"+"_"+args.cat
    postfix += "pulls" if args.pulls else ""
    makeValidationPlots(histos, outPostfix = postfix, processCat = args.cat, variables = args.vars, drawPulls = args.pulls, saveROOTObjects = args.saveHistos, saveMacro = args.saveMacro)
    
    logging.info("Exiting script")
