#!/usr/bin/env python
import ROOT
import Helper

bgnorm_ddQCD = 1.05
systematics = Helper.systematics
#systematics += ["bgnorm_ddQCD"]
sdir = "Up"
selbins = 0

#print systematics
print ""
#infile = "/mnt/t3nfs01/data01/shome/koschwei/tth/gc/sparsev4_final_v3_noDetaj/merged_SR_ddQCD_bNormUncorr.root"
#infile = "/mnt/t3nfs01/data01/shome/koschwei/tth/gc/sparsev4_final_v3/merged_SR_ddQCD_bNormUncorr.root"

categories = ["fh_j7_t3", "fh_j8_t3", "fh_j9_t3", "fh_j7_t4", "fh_j8_t4", "fh_j9_t4"]
#categories = ["fh_presel"]

#backgrounds = ["qcd", "ttbarOther", "ttbarPlusCCbar", "ttbarPlusB", "ttbarPlus2B", "ttbarPlusBBbar", "singlet", "wjets", "zjets", "ttbarZ", "ttbarW", "total_background"]
#backgrounds = ["qcd", "ttbarOther", "ttbarPlusCCbar", "ttbarPlusB", "ttbarPlus2B", "ttbarPlusBBbar", "singlet", "vjets", "ttbarV", "diboson","total_background"]
backgrounds = ["ddQCD", "ttbarOther", "ttbarPlusCCbar", "ttbarPlusB", "ttbarPlus2B", "ttbarPlusBBbar", "singlet", "vjets", "ttbarV", "diboson","total_background"]
signals = ["ttH_hbb", "ttH_hcc", "ttH_hww", "ttH_hzz", "ttH_htt", "ttH_hgg", "ttH_hgluglu", "ttH_hzg", "total_signal" ]

#yieldorder = ["qcd", "ttbarOther", "ttbarPlusCCbar", "ttbarPlusB", "ttbarPlus2B", "ttbarPlusBBbar", "singlet", "wjets", "zjets", "ttbarZ", "ttbarW", "total_background", "total_signal",  "data"]
#yieldorder = ["qcd", "ttbarOther", "ttbarPlusCCbar", "ttbarPlusB", "ttbarPlus2B", "ttbarPlusBBbar", "singlet", "vjets", "ttbarV", "diboson", "total_background", "total_signal",  "data"]
yieldorder = ["ddQCD", "ttbarOther", "ttbarPlusCCbar", "ttbarPlusB", "ttbarPlus2B", "ttbarPlusBBbar", "singlet", "vjets", "ttbarV", "diboson", "total_background", "total_signal",  "data"]

sums = {
    "ttbarV" : ["ttbarZ", "ttbarW"],
    "vjets" : ["zjets", "wjets"],
}

sumMap = {}

def getYields(infile):
    f = ROOT.TFile.Open(infile)
    yields = {} #dict of cat, proc and yield
    errors2 = {} #dict of cat, proc and error squared
    print f
    for cat in categories:
        yields[cat] = {}
        errors2[cat] = {}
        yields[cat]["total_background"] = 0.0
        errors2[cat]["total_background"] = 0.0
        yields[cat]["total_signal"] = 0.0
        errors2[cat]["total_signal"] = 0.0
        if "t3" in cat:
            disc = "mem_FH_4w2h1t_p"
        elif cat == "fh_j9_t4":
            disc = "mem_FH_4w2h2t_p"
        elif "presel" in cat:
            disc = "nBDeepCSVM"
        else:
            disc = "mem_FH_3w2h2t_p"
        for proc in backgrounds+signals+["data"]:
            if proc=="total_background" or proc=="total_signal":
                continue
            errors2[cat][proc] = 0.0
            nom = proc+"__"+cat+"__"+disc
            print nom
            hist = f.Get(nom).Clone()
            nbins = hist.GetNbinsX()
            if selbins > 0:
                yields[cat][proc] = hist.Integral(selbins,nbins)
                for i in range(selbins,nbins+1):
                    errors2[cat][proc] += hist.GetBinError(i)**2
            else:
                yields[cat][proc] = hist.Integral()
                for i in range(nbins):
                    errors2[cat][proc] += hist.GetBinError(i+1)**2
            if proc in backgrounds:
                yields[cat]["total_background"] += yields[cat][proc]
                errors2[cat]["total_background"] += errors2[cat][proc]
            elif proc in signals:
                yields[cat]["total_signal"] += yields[cat][proc]
                errors2[cat]["total_signal"] += errors2[cat][proc]
            #if proc=="ttbarPlusB":
            #    print "nominal", yields[cat][proc]
        errors2[cat]["data"] = yields[cat]["data"]

        for sys in systematics:
            sys = "__"+sys+sdir
            Ybkg = 0.0
            Ysig = 0.0
            for proc in backgrounds+signals+["data"]:
                if proc=="total_background" or proc=="total_signal" or proc=="data":
                    continue
                name = proc+"__"+cat+"__"+disc+sys
                if "bgnorm_ddQCD" in sys and proc=="ddQCD":
                    yld = yields[cat][proc]*bgnorm_ddQCD
                elif f.GetListOfKeys().Contains(name):
                    hist = f.Get(name).Clone()
                    if cat=="fh_j8_t4" and proc=="wjets": #TEMP
                        if hist.GetBinContent(1)<4 or hist.GetBinContent(1)>10:
                            print "zerobin", name, hist.GetBinContent(1)
                    if selbins > 0:
                        nbins = hist.GetNbinsX()
                        yld = hist.Integral(selbins,nbins)
                    else:
                        yld = hist.Integral()
                else:
                    #print name, "not found"
                    yld = yields[cat][proc]
                diff = yld - yields[cat][proc]
                #if proc=="ttbarPlusB":
                #    print sys, yld, diff
                if ((abs(diff)/yields[cat][proc]) > 0.5):
                    print cat, proc, sys, yld, "nom", yields[cat][proc]
                errors2[cat][proc] += diff*diff
                if proc in backgrounds:
                    Ybkg += yld
                elif proc in signals:
                    Ysig += yld
            errors2[cat]["total_background"] += (Ybkg-yields[cat]["total_background"])**2
            errors2[cat]["total_signal"] += (Ysig-yields[cat]["total_signal"])**2

    """
    for cat in categories:
        print cat
        print "proc yield . err ."   
        for proc in yieldorder:
            if not proc:
                print ""
                continue
            print proc,
            yld = yields[cat][proc]
            err = errors2[cat][proc]**0.5
            print yld, ".", err, ".",
            print ""
        print ""
    """
    return yields, errors2

def printTexTable(infile):
    yields, errors2 = getYields(infile)
    for proc in yieldorder:
        line = proc + " &"
        for cat in categories:
            line +=  "{0:.0f}".format(yields[cat][proc]) +" & \hspace{-4.15mm} $\pm$"+" {0:.0f}".format(errors2[cat][proc]**0.5)+" & "
        line = line[0:-2]+" \\\\"
        print line



if __name__ == "__main__":
    #infile = "/mnt/t3nfs01/data01/shome/koschwei/tth/gc/sparsev4_final_v3_noDetaj/merged_SR_ddQCD_bNormUncorr.root"
    #infile = "/mnt/t3nfs01/data01/shome/koschwei/tth/gc/sparsev4_final_v3/merged_SR_ddQCD_bNormUncorr.root"
    #infile = "/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/combine/CMSSW_8_1_0/src/approval/alt/datacards/datacards/ttHbb_2017/FH_UZH_v05/Baseline/fh_presel__nBDeepCSVM_merged.root"
    infile = "/mnt/t3nfs01/data01/shome/koschwei/tth/gc/sparsev5_final_v1/merged_SR_ddQCD_merged.root"
    printTexTable(infile)
