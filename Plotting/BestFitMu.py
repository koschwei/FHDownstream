#!/usr/bin/env python
import numpy as np
#import CMS_lumi as CMS_lumi
import ROOT
ROOT.gStyle.SetOptStat(0)
from collections import OrderedDict
import Helper

savePlots = True
version = "sparsev5_final_v2_unblind_V5_L1Pre"

#categories and mu value [mu, minus, plus, statm, statp, sysm, sysp] in reverse order to plot
labels = OrderedDict()
labels["group_fh"] = [-1.67, -1.51, 1.42, 0, 0, 0, 0]
labels["group_fh_4b"] = [-1.53, -1.44, 1.4, 0, 0, 0, 0]
labels["group_fh_3b"] = [1.35, -5.35, 3.64, 0, 0, 0, 0]
labels["fh_j9_t4"] = [-1.36, -2.6, 2.51, 0, 0, 0, 0]
labels["fh_j8_t4"] = [-0.55, -2.05, 1.93, 0, 0, 0, 0]
labels["fh_j7_t4"] = [-2.42, -2.57, 2.60, 0, 0, 0, 0]
labels["fh_j9_t3"] = [-7.21, -6.66, 5.73, 0, 0, 0, 0]
labels["fh_j8_t3"] = [-0.91, -9.11, 6.96, 0, 0, 0, 0]
labels["fh_j7_t3"] = [1.11, -14.17, 10.11, 0, 0, 0, 0]

# labels["group_fh"] = [0.8, -1.5, 1.5, -0.7, 0.7, -1.3, 1.3] #same but rounded per PDG rounding
# labels["group_fh_4b"] = [1.4, -1.6, 1.6, -0.9, 0.9, -1.4, 1.4]
# labels["group_fh_3b"] = [-1.7, -5.4, 5.2, -1.4, 1.4, -5.2, 5.0]
# labels["fh_j9_t4"] = [-0.6, -2.2, 2.1, -1.3, 1.4, -1.7, 1.6]
# labels["fh_j8_t4"] = [-0.3, -2.9, 2.8, -1.5, 1.5, -2.5, 2.3]
# labels["fh_j7_t4"] = [5.3, -2.7, 2.9, -1.8, 1.8, -2.0, 2.2]
# labels["fh_j9_t3"] = [-3.5, -6.4, 5.8, -2.4, 2.4, -6.0, 5.3]
# labels["fh_j8_t3"] = [1.2, -6.3, 5.9, -2.3, 2.2, -5.9, 5.4]
# labels["fh_j7_t3"] = [1.7, -11.7, 9.6, -2.7, 2.7, -11.4, 9.2]

# labels["group_fh"] = [0.84, -1.48, 1.50, -0.73, 0.74, -1.28, 1.31] #rateParam3 (with 1stBinUnc for 3b) QCDBin->stat
# labels["group_fh_4b"] = [1.37, -1.62, 1.64, -0.87, 0.87, -1.37, 1.39]
# labels["group_fh_3b"] = [-1.71, -5.36, 5.17, -1.40, 1.40, -5.17, 4.98]
# labels["fh_j9_t4"] = [-0.56, -2.18, 2.06, -1.34, 1.35, -1.72, 1.55]
# labels["fh_j8_t4"] = [-0.25, -2.93, 2.76, -1.47, 1.48, -2.53, 2.33]
# labels["fh_j7_t4"] = [5.33, -2.69, 2.87, -1.77, 1.79, -2.02, 2.25]
# labels["fh_j9_t3"] = [-3.47, -6.41, 5.79, -2.38, 2.36, -5.95, 5.29]
# labels["fh_j8_t3"] = [1.17, -6.32, 5.85, -2.26, 2.25, -5.90, 5.40]
# labels["fh_j7_t3"] = [1.66, -11.75, 9.57, -2.71, 2.71, -11.43, 9.18]

# labels["group_fh"] = [1.03, -1.48, 1.55, -0.64, 0.64, -1.34, 1.41] #7j3bHTrewt rateParam
# labels["group_fh_4b"] = [1.37, -1.62, 1.64, -0.79, 0.80, -1.42, 1.43]
# labels["group_fh_3b"] = [-3.25, -5.56, 6.97, -1.11, 1.11, -5.44, 6.88]
# labels["fh_j9_t4"] = [-0.56, -2.18, 2.06, -1.22, 1.25, -1.81, 1.64]
# labels["fh_j8_t4"] = [-0.25, -2.93, 2.76, -1.33, 1.35, -2.61, 2.40]
# labels["fh_j7_t4"] = [5.33, -2.69, 2.87, -1.62, 1.65, -2.14, 2.35]
# labels["fh_j9_t3"] = [-3.22, -6.35, 5.60, -1.83, 1.84, -6.08, 5.29]
# labels["fh_j8_t3"] = [0.98, -6.21, 5.75, -1.80, 1.81, -5.94, 5.46]
# labels["fh_j7_t3"] = [-3.99, -11.88, 10.35, -2.20, 2.21, -11.67, 10.12]

# labels["group_fh"] = [1.35, -1.14, 1.14, -0.45, 0.46, -1.04, 1.04] #unblind 35% tt+hf norm
# labels["group_fh_4b"] = [1.51, -1.39, 1.45, -0.60, 0.61, -1.26, 1.32]
# labels["fh_j9_t4"] = [-0.08, -1.90, 1.73, -0.89, 0.91, -1.68, 1.47]
# labels["fh_j8_t4"] = [-0.46, -2.79, 2.48, -1.02, 1.04, -2.60, 2.26]
# labels["fh_j7_t4"] = [4.59, -2.43, 2.62, -1.29, 1.31, -2.06, 2.27]
# labels["group_fh_3b"] = [0.48, -3.07, 2.89, -0.70, 0.71, -2.99, 2.80]
# labels["fh_j9_t3"] = [-1.54, -4.47, 4.28, -1.10, 1.11, -4.33, 4.14]
# labels["fh_j8_t3"] = [1.10, -5.29, 5.11, -1.19, 1.19, -5.15, 4.97]
# labels["fh_j7_t3"] = [1.64, -9.77, 8.33, -1.43, 1.43, -9.66, 8.20]

# labels["group_fh"] = [1.47, -1.10, 1.11, -0.45, 0.46, -1.00, 1.01] #post-fit unblind 50% tt+hf norm
# labels["group_fh_4b"] = [1.63, -1.37, 1.42, -0.60, 0.61, -1.23, 1.29]
# labels["fh_j9_t4"] = [0.00, -1.95, 1.76, -0.89, 0.91, -1.74, 1.50]
# labels["fh_j8_t4"] = [-0.40, -2.89, 2.50, -1.02, 1.04, -2.70, 2.28]
# labels["fh_j7_t4"] = [4.64, -2.48, 2.70, -1.29, 1.31, -2.12, 2.36]
# labels["group_fh_3b"] = [0.50, -3.08, 2.92, -0.70, 0.71, -3.00, 2.84]
# labels["fh_j9_t3"] = [-1.52, -4.48, 4.41, -1.10, 1.11, -4.34, 4.27]
# labels["fh_j8_t3"] = [1.13, -5.30, 5.11, -1.19, 1.19, -5.17, 4.97]
# labels["fh_j7_t3"] = [1.64, -9.93, 8.36, -1.43, 1.43, -9.82, 8.23]

# labels["group_fh"] = [1.00, -0.94, 1.12, -0.46, 0.46, -0.82, 1.02] #pre-fit blind
# labels["group_fh_4b"] = [1.00, -1.23, 1.39, -0.60, 0.61, -1.07, 1.25]
# labels["fh_j9_t4"] = [1.00, -1.57, 1.79, -0.90, 0.92, -1.28, 1.54]
# labels["fh_j8_t4"] = [1.00, -1.97, 2.03, -1.04, 1.06, -1.67, 1.73]
# labels["fh_j7_t4"] = [1.00, -2.26, 2.42, -1.28, 1.31, -1.86, 2.03]
# labels["group_fh_3b"] = [1.00, -2.73, 3.00, -0.70, 0.71, -2.64, 2.91]
# labels["fh_j9_t3"] = [1.00, -4.53, 4.43, -1.10, 1.11, -4.40, 4.29]
# labels["fh_j8_t3"] = [1.00, -5.74, 5.45, -1.19, 1.19, -5.61, 5.31]
# labels["fh_j7_t3"] = [1.00, -8.43, 7.64, -1.43, 1.44, -8.31, 7.50]

label_dict = {'fh_j7_t3': "7j, 3b",
              'fh_j8_t3': "8j, 3b",
              'fh_j9_t3': "#geq9j, 3b",
              'group_fh_3b': "3b cats",
              'fh_j7_t4': "7j, #geq4b",
              'fh_j8_t4': "8j, #geq4b",
              'fh_j9_t4': "#geq9j, #geq4b",
              'group_fh_4b': "4b cats",
              'group_fh': "Combined"  }

index  = np.ndarray([0])
mu     = np.ndarray([0])
downT  = np.ndarray([0])
upT    = np.ndarray([0])
downSt = np.ndarray([0])
upSt   = np.ndarray([0])
downSy = np.ndarray([0])
upSy   = np.ndarray([0])
width  = np.ndarray([0])
zeros  = np.ndarray([0])

(low, high) = (1000, 0)
N = len(labels)
for i, label in enumerate(labels):
    values = [ v for v in labels[label] ]
    index   = np.append(index,  i+0.7)
    mu = np.append(mu, values[0])
    downT = np.append(downT, -1.0*values[1])
    upT = np.append(upT, values[2])
    downSt = np.append(downSt, -1.0*values[3])
    upSt = np.append(upSt, values[4])
    downSy = np.append(downSy, -1.0*values[5])
    upSy = np.append(upSy, values[6])
    width   = np.append(width,  0.38)
    zeros   = np.append(zeros,  0)
    if low > (values[0]-values[1]): low = (values[0]-values[1])
    if high < (values[0]+values[2]): high = (values[0]+values[2])

print "downT", downT
print "index", index

total = ROOT.TGraphAsymmErrors(N, mu, index, downT, upT, zeros, zeros)
stat = ROOT.TGraphAsymmErrors(N, mu, index, downSt, upSt, zeros, zeros)

width = 600
height = 500
top = 0.08
bottom = 0.14
left = 0.195
right = 0.02

c2  = ROOT.TCanvas("c2","c2",100,100,width,height) #topx, topy, width, height
c2.SetFillColor(0)
c2.SetBorderMode(0)
c2.SetFrameFillStyle(0)
c2.SetFrameBorderMode(0)
c2.SetTopMargin(top)
c2.SetBottomMargin(bottom)
c2.SetLeftMargin(left)
c2.SetRightMargin(right)
#c2.SetGrid(1,0) #verticle lines
c2.SetTicks(0,0)

total.SetLineWidth(1)
total.SetLineColor(ROOT.kBlack)
stat.SetLineWidth(1)
stat.SetLineColor(ROOT.kRed)
stat.SetMarkerStyle(21)
stat.SetMarkerSize(1)
stat.SetMarkerColor(ROOT.kBlack)

frame = ROOT.TH2F("frame","",200,-20,50, N+1,0,N+1)
frame.GetXaxis().SetTitleSize(0.055)
frame.GetYaxis().SetLabelSize(0)
frame.GetYaxis().SetTitleSize(0)
frame.GetXaxis().SetLabelSize(0.048)
frame.GetXaxis().SetTitleOffset(1.14)
frame.GetYaxis().SetNdivisions(1,0,0,False) # ndiv = N1 + 100*N2 + 10000*N3
xaxis_title = "Best fit #hat{#mu} = #sigma/#sigma_{SM} at #it{m}_{H} = 125 GeV"
frame.GetXaxis().SetTitle(xaxis_title)

rMin = -15.5
rMax = 20.5
frame.GetXaxis().SetRangeUser(rMin,rMax)
#frame.GetYaxis().SetTitleOffset(0.8)

#one = ROOT.TH1F("one","",1,0,2)
#one.SetBinContent(1,0.5*N)
#one.SetBinError(1,0.5*N)
yval = np.array([0.5*N])
zero = np.array([0]) 
one = ROOT.TGraphAsymmErrors(1,np.array([1.0]),yval,zero,zero,yval,yval-0.3)
one.SetLineColor(ROOT.kBlack)
one.SetLineStyle(2)

split1 = ROOT.TGraphAsymmErrors(1,np.array([0.0]),np.array([1.2]),np.array([50.]),np.array([50.]),zero,zero)
split2 = ROOT.TGraphAsymmErrors(1,np.array([0.0]),np.array([3.2]),np.array([50.]),np.array([50.]),zero,zero)
split1.SetLineColor(ROOT.kBlack)
split2.SetLineColor(ROOT.kBlack)


frame.Draw("AXIS")
one.Draw("E SAME")
split1.Draw("E SAME")
split2.Draw("E SAME")
total.Draw("E SAME")
stat.Draw("PE SAME")
#leg.Draw()

text = ROOT.TLatex()
text.SetTextSize(0.055)
text.SetTextAlign(31)
text.SetTextFont(42)
xtext = rMin+0.015*(rMax-rMin) #-1.85
xmu = rMin+0.83*(rMax-rMin) #5.1
xtot = rMin+0.93*(rMax-rMin) #6.4
xstat = rMin+0.83*(rMax-rMin) #7.8
xsys = rMin+0.93*(rMax-rMin) #9.2
xleft = rMin+0.78*(rMax-rMin) #7.8
xright = rMin+0.982*(rMax-rMin) #9.2
ycorr = 0.0
yhead = N+0.4 #N=len(labels)

txt_mu = ROOT.TLatex()
txt_mu.SetTextSize(0.05)
txt_mu.SetTextAlign(31) #right align
txt_mu.SetTextFont(42)
txt_mu.DrawLatex(xmu,yhead,"#mu ")

txt_tot = ROOT.TLatex()
txt_tot.SetTextSize(0.042)
txt_tot.SetTextColor(ROOT.kBlack)
txt_tot.SetTextAlign(21)
txt_tot.SetTextFont(42)
txt_tot.DrawLatex(xtot,yhead,"tot")
txt_tot.SetTextSize(0.032)

txt_stat = ROOT.TLatex()
txt_stat.SetTextSize(0.042)
txt_stat.SetTextColor(ROOT.kGreen)
txt_stat.SetTextAlign(21)
txt_stat.SetTextFont(42)
#txt_stat.DrawLatex(xstat*.98,yhead,"( stat")
txt_stat.SetTextSize(0.032)

txt_sys = ROOT.TLatex()
txt_sys.SetTextSize(0.042)
txt_sys.SetTextColor(ROOT.kCyan)
txt_sys.SetTextAlign(21)
txt_sys.SetTextFont(42)
#txt_sys.DrawLatex(xsys*1.02,yhead,"syst )")
txt_sys.SetTextSize(0.032)

txt_left = ROOT.TLatex()
txt_left.SetTextSize(0.05)
txt_left.SetTextColor(ROOT.kRed)
txt_left.SetTextAlign(21)
txt_left.SetTextFont(42)

txt_right = ROOT.TLatex()
txt_right.SetTextSize(0.05)
txt_right.SetTextColor(ROOT.kBlack)
txt_right.SetTextAlign(21)
txt_right.SetTextFont(42)

for i, label in enumerate(labels):
    bin = frame.GetYaxis().FindBin(i)
    ytext = frame.GetYaxis().GetBinCenter(bin)+ycorr
    print i, bin, (ytext-ycorr)
    text.DrawLatex(xtext,ytext,label_dict[label]+"  ")
    if labels[label][0]<0:
        txt_mu.DrawLatex(xmu,ytext,"#font[122]{\55}"+"{0:0.1f}".format(-labels[label][0]))
    else:
        txt_mu.DrawLatex(xmu,ytext,"{0:0.1f}".format(labels[label][0]))
    txt_tot.DrawLatex(xtot,ytext+0.26,"+{0:0.1f}".format(labels[label][2]))
    txt_tot.DrawLatex(xtot,ytext-0.16,"#font[122]{\55}"+"{0:0.1f}".format(-labels[label][1]))
    #txt_stat.DrawLatex(xstat,ytext+0.26,"+{0:0.1f}".format(labels[label][4]))
    #txt_stat.DrawLatex(xstat,ytext-0.16,"#font[122]{\55}"+"{0:0.1f}".format(-labels[label][3]))
    #txt_sys.DrawLatex(xsys,ytext+0.26,"+{0:0.1f}".format(labels[label][6]))
    #txt_sys.DrawLatex(xsys,ytext-0.16,"#font[122]{\55}"+"{0:0.1f}".format(-labels[label][5]))
    #txt_left.DrawLatex(xleft,ytext,"(")
    #txt_right.DrawLatex(xright,ytext,")") 

lumi = 41.5 #fb-1
box = Helper.create_paves(lumi, "Data", CMSposX=0.22, CMSposY=0.835, 
                          prelimPosX=0.33, prelimPosY=0.835,
                          lumiPosX=0.99, lumiPosY=0.88, alignRight=False)
box["lumi"].Draw()
box["CMS"].Draw()
box["label"].Draw()

c2.Update()

print " "
raw_input("press Enter to "+("save plot and " if savePlots else "")+"quit")
if savePlots:
    c2.SaveAs(version+"_bestFitMu.pdf")
    c2.Close()
