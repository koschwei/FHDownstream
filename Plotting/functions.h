#include<algorithm>

float get_TriggerSF3D(float ht, float pt6, int nB){
  // initialize in ROOT with TriggerSF* internalTriggerSF3D = new TriggerSF3D();
  float SF = internalTriggerSF3D->getTriggerWeight(ht,pt6,nB);
  //cout << SF << endl;
  return SF;
}


float QCDnJetWeight(int nJets){
  if (nJets == 6){
    return 0.907373905182;//+- 0.00864550945866
  }
  else if (nJets == 7){
    return 1.0223556757;// +- 0.0111939198432
  }
  else if (nJets == 8){
    return 1.11500406265;// +- 0.0183448039935
  }
  else if (nJets == 9){
    return 1.27151966095;// +- 0.0348287156877
  }
  else if (nJets == 10){
    return 1.49826455116;// +- 0.0717107143588
  }
  else if (nJets >= 11){
    return 1.51720297337;// +- 0.0659095385476
  }
  else {
    return 1.0;
  }
}

float HTWeight(float HT, int threeb){

  if(HT<500.0){ if(threeb == 3){return 1.12839255952;} else{return 1.31703485014;} }
  else if(HT<600.0){ if(threeb == 3){return 1.02091043945;} else{return 1.09419489123;} }
  else if(HT<700.0){ if(threeb == 3){return 0.981985075772;} else{return 1.01012853225;} }
  else if(HT<800.0){ if(threeb == 3){return 0.97105488008;} else{return 0.946841980024;} }
  else if(HT<900.0){ if(threeb == 3){return 0.968719047848;} else{return 0.857324375477;} }
  else if(HT<1000.0){ if(threeb == 3){return 0.95311308219;} else{return 0.839690682537;} }
  else if(HT<1100.0){ if(threeb == 3){return 0.902002818271;} else{return 0.677413601896;} }
  else if(HT<1200.0){ if(threeb == 3){return 0.897223939954;} else{return 0.676264641978;} }
  else if(HT<1300.0){ if(threeb == 3){return 0.864501512154;} else{return 0.50025215611;} }
  else if(HT<1400.0){ if(threeb == 3){return 0.822589924956;} else{return 0.654403074495;} }
  else { if(threeb == 3){return 0.894508345722;} else{return 0.806744147772;} }

}

/* float get_bTagNormalization(int SampleID, int nJets){ */
/*   /\* */
/*     1 - QCD300 */
/*     2 - QCD500 */
/*     3 - QCD700 */
/*     4 - QCD1000 */
/*     5 - QCD1500 */
/*     6 - QCD2000 */
/*     7 - st_s */
/*     8 - st_t */
/*     9 - st_tW */
/*     10 - stbar_t */
/*     11 - stbar_tW */
/*     12 - tt_SL */
/*     13 - tt_DL */
/*     14 - tt_FH */
/*     15 - ttW */
/*     16 - ttZ */
/*     17 - WJets_400 */
/*     18 - WJets_600 */
/*     19 - WJets_800 */
/*     20 - ZJets_400 */
/*     21 - ZJets_600 */
/*     22 - ZJets_800 */
/*     23 - ZZ */
/*     24 - WZ */
/*     25 - WW */
/*     26 - ttHbb */
/*     27 - ttHnonbb */
/*   *\/ */
/*   string sampleName = "NotSet"; */
/*   switch(SampleID){ */
/*   case 1 : sampleName = "QCD300"; break; */
/*   case 2 : sampleName = "QCD500"; break; */
/*   case 3 : sampleName = "QCD700"; break; */
/*   case 4 : sampleName = "QCD1000"; break; */
/*   case 5 : sampleName = "QCD1500"; break; */
/*   case 6 : sampleName = "QCD2000"; break; */
/*   case 7 : sampleName = "st_s"; break; */
/*   case 8 : sampleName = "st_t"; break; */
/*   case 9 : sampleName = "st_tw"; break; */
/*   case 10 : sampleName = "stbar_t"; break; */
/*   case 11 : sampleName = "stbar_tw"; break; */
/*   case 12 : sampleName = "TTbar_sl"; break; */
/*   case 13 : sampleName = "TTbar_dl"; break; */
/*   case 14 : sampleName = "TTbar_fh"; break; */
/*   case 15 : sampleName = "ttw_wqq"; break; */
/*   case 16 : sampleName = "ttz_zqq"; break; */
/*   case 17 : sampleName = "WjetstoQQ400"; break; */
/*   case 18 : sampleName = "WJetsToQQ600"; break; */
/*   case 19 : sampleName = "WJetsToQQ800"; break; */
/*   case 20 : sampleName = "ZJetsToQQ400"; break; */
/*   case 21 : sampleName = "ZJetsToQQ600"; break; */
/*   case 22 : sampleName = "ZJetsToQQ800"; break; */
/*   case 23 : sampleName = "zz"; break; */
/*   case 24 : sampleName = "wz"; break; */
/*   case 25 : sampleName = "ww"; break; */
/*   case 26 : sampleName = "ttH_hbb"; break; */
/*   case 27 : sampleName = "ttH_nonhbb"; break; */

/*   } */
/*   if (sampleName == "NotSet") {return 1.0;} */
/*   return internalbTagNormalization->getbTagNormalization(sampleName, nJets); */
/* } */

