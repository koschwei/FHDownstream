import ROOT
import copy
import logging
import math
import os

def moveOverUnderFlow(histo, moveOverFlow=True, moveUnderFlow=False):
    """
    Function for moving the overflow and (or) underflow bin to the first/last bin
    """
    nBins = histo.GetNbinsX()
    if moveUnderFlow:
        underflow = histo.GetBinContent(0)
        fistBinContent = histo.GetBinContent(1)
        err = math.sqrt(histo.GetBinError(0)*histo.GetBinError(0) + histo.GetBinError(1)*histo.GetBinError(1))
        histo.SetBinContent(1, fistBinContent+underflow)
        histo.SetBinContent(0, 0)
        histo.SetBinError(1,err)
        histo.SetBinError(0,0)
    if moveOverFlow:
        overflow = histo.GetBinContent(nBins+1)
        lastBinContent = histo.GetBinContent(nBins)
        err = math.sqrt(histo.GetBinError(nBins+1)*histo.GetBinError(nBins+1) + histo.GetBinError(nBins)*histo.GetBinError(nBins))
        histo.SetBinContent(nBins, lastBinContent+overflow)
        histo.SetBinContent(nBins+1, 0)
        histo.SetBinError(nBins,err)
        histo.SetBinError(nBins+1,0)
        
def saveCanvasListAsPDF(listofCanvases, outputfilename, foldername):
    logging.info("Writing outputfile %s.pdf",outputfilename)
    if not os.path.isdir(foldername):
        logging.warning("Creating dir %s", foldername)
        os.makedirs(foldername)
    if len(listofCanvases) > 1:
        for icanvas, canves in enumerate(listofCanvases):
            if icanvas == 0:
                canves.Print(foldername+"/"+outputfilename+".pdf[", "pdf")
                #canves.Print(foldername+"/"+outputfilename+".pdf", "pdf")
                canves.Print(foldername+"/"+outputfilename+".pdf", "pdf")
            elif icanvas == len(listofCanvases)-1:
                canves.Print(foldername+"/"+outputfilename+".pdf", "pdf")
                canves.Print(foldername+"/"+outputfilename+".pdf]", "pdf")
            else:
                canves.Print(foldername+"/"+outputfilename+".pdf", "pdf")
    else:
        listofCanvases[0].Print(foldername+"/"+outputfilename+".pdf", "pdf")

def makeDistribution(prefix, histos, Files, selection, mcWeight, color):
    retHistos = []
    for histo, bins, sBin, eBin in histos:
        logging.info("Processing plot: %s",histo)
        h = ROOT.TH1F(histo+"_"+prefix, histo+"_"+prefix, bins, sBin, eBin)
        h.SetTitle("")
        logging.info("Created histos %s", h)
        h.SetLineColor(color)
        for ifile, file_ in enumerate(Files):
            fileName, xsec, ngen = file_
            logging.info("processing file: %s", fileName.GetName().split("/")[-1])
            #logging.info("processing file: %s", fileName.split("/")[-1])
            #rfile = ROOT.TFile.Open(fileName)
            logging.debug(fileName)
            tree = fileName.Get("tree")
            SelTimesWeight = "({0}) * ({1} * {2})".format(selection, 100000 * (xsec/float(ngen)), mcWeight)
            logging.debug("Selection * weight: %s",SelTimesWeight)
            logging.debug("Variable: %s",histo)
            if ifile == 0:
                logging.debug("Projection initial histo %s", h.GetName())
                nProj = tree.Project(h.GetName(), histo, SelTimesWeight)
                logging.debug("Current histo integral: %s", h.Integral())
            else:
                tmpName = h.GetName()+"_tmp"+str(ifile)
                tmph = h.Clone(tmpName)
                nProj = tree.Project(tmpName, histo, SelTimesWeight)
                logging.debug("Current histo integral: %s", tmph.Integral())
                logging.debug("Adding projected histos")
                h.Add(tmph)
            logging.debug("Projection yielded %s events",nProj)
            logging.debug("Total histo integral: %s", h.Integral())
        moveOverUnderFlow(h)
        retHistos.append(h)

    return copy.deepcopy(retHistos)

def drawCMSLabel(addTest = "Preliminary", topMargin = 0.1, rightMargin = 0.1, scaleLumi = 1.0, scaleCMS = 1.0, moveCMS = 0, moveCMSTop = 0):
    """
    Drawing the CMS lumi plus logo somewhat tdr stylish

    Args:
    =====
    moveCMS(float) : Changes the x offset from the axis of the CMS logo part. Legative moves to left, postive moves to right
    """     
    t = topMargin
    r = rightMargin
    cmsText     = "CMS"
    cmsTextFont   = 61  

    logging.debug("Using scale %s, %s",scaleLumi, scaleCMS)
    
    writeExtraText = True
    extraText   = addTest
    extraTextFont = 52 
    
    lumiTextSize     = 0.6 * scaleLumi
    lumiTextOffset   = 0.23
    
    cmsTextSize      = 0.75 * scaleCMS
    cmsTextOffset    = 0.12
    relExtraDY = 0.8

    extraOverCmsTextSize  = 0.76

    extraTextSize = extraOverCmsTextSize*cmsTextSize
    lumiText = "41.5 fb^{-1} (13 TeV)"
    latex = ROOT.TLatex()
    latex.SetNDC()
    latex.SetTextAngle(0)
    latex.SetTextColor(ROOT.kBlack)

    latex.SetTextFont(42)
    latex.SetTextAlign(31) 
    latex.SetTextSize(lumiTextSize*t)    

    latex.DrawLatex(1-r,1-t+lumiTextOffset*t,lumiText)
    
    latex.SetTextFont(cmsTextFont)
    latex.SetTextSize(cmsTextSize*t)
    latex.DrawLatex(0.92+moveCMS, 0.86+moveCMSTop, cmsText)
    latex.SetTextFont(extraTextFont)
    latex.SetTextSize(extraTextSize*t)
    latex.DrawLatex(0.92+moveCMS, 0.86+moveCMSTop-relExtraDY*cmsTextSize*t, extraText)
    

    #returns the CMS and Extra textsize
    return cmsTextSize*t, extraTextSize*t
    
def makeCanvasOfHistos(name, Varname, histoList,width=600, height=540, legendText = None,
                       legendSize = (0.5,0.15,0.9,0.4), normalized = False, lineWidth = None,
                       colorList = None, drawAs = "HISTOE", drawAsList = None, addCMS = False,
                       addLabel = None, labelpos = None, legendColumns = 1, topScale = 1.1):
    canvas = ROOT.TCanvas("Generalcanvas_"+str(name),"Generalcanvas_"+str(name),width, height)
    canvas.SetTicks(1,1)
    canvas.SetFillStyle(0)
    canvas.SetFillColor(0)

    canvas.SetTopMargin(0.068)
    canvas.SetRightMargin(0.04)
    canvas.SetLeftMargin(0.12)
    canvas.SetBottomMargin(0.12)

    leg = ROOT.TLegend(legendSize[0], legendSize[1], legendSize[2], legendSize[3])
    leg.SetNColumns(legendColumns)
    leg.SetFillStyle(0)
    leg.SetBorderSize(0)
    leg.SetTextSize(0.03)
    

    
    if legendText is not None:
        assert isinstance(legendText, list)
        assert len(legendText) == len(histoList)
    if colorList is not None:
        assert isinstance(colorList, list)
        assert len(colorList) == len(histoList)
    canvas.cd()
        
    if normalized:
        for ihisto, histo in enumerate(histoList):
            normalizeHisto(histo)

    if drawAsList is None:
        drawAsList = len(histoList)*[drawAs]
        
    maximumVal = 0
    for ihisto, histo in enumerate(histoList):
        if histo.GetBinContent(histo.GetMaximumBin()) > maximumVal:
            maximumVal =  histo.GetBinContent(histo.GetMaximumBin())
            if maximumVal >= 30:
                print histo
    for ihisto, histo in enumerate(histoList):
        histo.SetTitle(" ")
        histo.GetXaxis().SetTitle(Varname)
        if colorList is not None:
            histo.SetLineColor(colorList[ihisto])
        if lineWidth is not None:
            histo.SetLineWidth(lineWidth)
        if maximumVal > 0:
            histo.GetYaxis().SetRangeUser(0,  maximumVal*topScale)
        if ihisto == 0:
            #histo.SetTitle(" ")
            if normalized:
                histo.GetYaxis().SetTitle("Normalized Units")
            histo.Draw(drawAsList[ihisto])
        else:
            histo.Draw(drawAsList[ihisto]+"same")
        if legendText is not None:
            leg.AddEntry(histo, legendText[ihisto], "L")

    CMSSize, ExtraSize = 0.6, 0.75
    if addCMS:
        CMSSize, ExtraSize = drawCMSLabel("Preliminary", canvas.GetTopMargin(), canvas.GetRightMargin() )
    if addLabel is not None:
        if labelpos is None:
            xPos, yPos = (0.27, 0.85)
        else:
            xPos, yPos = labelpos
        l = ROOT.TLatex();
        l.SetNDC();
        l.SetTextAlign(12);
        l.SetTextFont(42);
        l.SetTextSize(ExtraSize);
        l.DrawLatex(xPos, yPos, addLabel)

    if addCMS:
        leg.SetTextSize(ExtraSize)
    leg.Draw("same")
    canvas.Update()
    return copy.deepcopy(canvas)
            
def makeCanvas2DwCorrelation(histo, width=1280, height=1000, plotLogZ=False, xstart = 0.27, ystart = 0.95, addIdentifier = "", labelText = None, labelpos = None):
    name = histo.GetName()+addIdentifier
    canvas = ROOT.TCanvas("canvas_"+str(name),"canvas_"+str(name),width, height)
    canvas.SetLeftMargin(0.115)
    canvas.SetRightMargin(0.16)
    canvas.cd()
    if plotLogZ:
        canvas.SetLogz()
    histo.Draw("colz")
    t = ROOT.TLatex();
    t.SetNDC();
    t.SetTextAlign(22);
    t.SetTextFont(42);
    t.SetTextSizePixels(28);
    t.DrawLatex(xstart,ystart,"Correlation: {0:06.4f}".format(histo.GetCorrelationFactor()))
    if labelText is not None:
        if labelpos is None:
            xPos, yPos = (0.27, 0.85)
        else:
            xPos, yPos = labelpos
        l = ROOT.TLatex();
        l.SetNDC();
        l.SetTextAlign(23);
        l.SetTextFont(42);
        l.SetTextSizePixels(20);
        l.DrawLatex(xPos, yPos, labelText)
    
    return copy.deepcopy(canvas)


def normalizeHisto(histo):
    if histo.Integral() == 0:
        logging.warning("Histogrma integral 0. Will do nothing")
    else:
        histo.Scale(1/histo.Integral())
        
def initLogging(thisLevel):
    log_format = ('[%(asctime)s] %(funcName)-12s %(levelname)-8s %(message)s')
    if thisLevel == 20:
        thisLevel = logging.INFO
    elif thisLevel == 10:
        thisLevel = logging.DEBUG
    elif thisLevel == 30:
        thisLevel = logging.WARNING
    elif thisLevel == 40:
        thisLevel = logging.ERROR
    elif thisLevel == 50:
        thisLevel = logging.CRITICAL
    else:
        thisLevel = logging.NOTSET
        
    logging.basicConfig(
        format=log_format,
        level=thisLevel,
        datefmt="%H:%M:%S"
    )


def project(tree, hName, variable, selection, weight, ident = ""):
    """
    Wrapper for TTree::Project using the logging module
    """
    logging.info("Projecting %s %s", variable, ident)
    logging.debug("Selection: %s", selection)
    logging.debug("   Weight: %s", weight)
    logging.debug("    hName: %s", hName)
    tree.Project(hName, variable, "({0}) * ({1})".format(selection, weight))
