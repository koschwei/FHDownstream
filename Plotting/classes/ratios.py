"""
Module for making easy and fast ratio plots

Usage: 
1.) Initialize the RatioPlot class with a name (so the canvas has a unique name)
2.) Add the list of histograms that you want in the plot with the addHistos method.
    The fist will be used as the reference histogram (and the line in the ratio)
3.) Run the drawPlot method and pass a list of legend labels in the order of the
    histograms passed with the addHistos method. Passing a xTitle is optional. Otherwise
    the one of the leading histogram is used.
"""
import logging
from copy import deepcopy

import ROOT

import PlotHelpers

class RatioPlot(object):
    """
    Class for making nice and fast ratio plots
    """
    def __init__(self, name, width=600, height=540):
        logging.debug("Initializing class")
        self.canvas = ROOT.TCanvas("canvas_"+str(name),"canvas_"+str(name),width, height)
        self.canvas.Divide(1,2)
        self.canvas.cd(1).SetPad(0.,0.3-0.02,1.0,0.975)
        self.canvas.cd(2).SetPad(0.,0.0,1.0,0.3*(1-0.02))
        self.canvas.cd(1).SetBottomMargin(0.02)
        self.canvas.cd(2).SetTopMargin(0.00)
        self.canvas.cd(2).SetTicks(1,1)
        self.canvas.cd(1).SetTicks(1,1)
        self.canvas.cd(2).SetFillStyle(0)
        self.canvas.cd(1).SetFillStyle(0)
        self.canvas.cd(2).SetFillColor(0)
        self.canvas.cd(2).SetFrameFillStyle(4000)
        self.canvas.cd(1).SetTopMargin(0.068)
        self.canvas.cd(1).SetRightMargin(0.06)
        self.canvas.cd(1).SetLeftMargin(0.12)
        self.canvas.cd(2).SetRightMargin(0.06)
        self.canvas.cd(2).SetLeftMargin(0.12)
        self.canvas.cd(2).SetBottomMargin(0.3)


        # self.canvas.cd(1).SetTopMargin(0.01)
        # self.canvas.cd(1).SetRightMargin(0.04)
        # self.canvas.cd(1).SetLeftMargin(0.11)
        # self.canvas.cd(2).SetRightMargin(0.04)
        # self.canvas.cd(2).SetLeftMargin(0.11)
        # self.canvas.cd(2).SetBottomMargin(0.3)

        self.histos = None
        self.ratioRange = (0.2,1.8)
        self.ratioText = "#frac{Data}{MC}"
        self.legendSize = (0.7,0.6,0.9,0.8)
        self.legColumns = 1
        self.labels = None
        self.yTitle = "Events"
        self.labelScaleX = 1.2
        self.defaultColors = [ROOT.kBlue, ROOT.kRed, ROOT.kGreen-2, ROOT.kBlack, ROOT.kOrange, ROOT.kCyan]
        self.useDefaultColors = False

        self.yTitleOffset = 0.78
        self.xTitleOffsetScale = 1 
        self.yTitleOffsetRatio = 0.4
        self.invertRatio = False

        self.CMSscale = (1.0, 1.0)
        self.moveCMS = 0
        self.moveCMSTop = 0
        
        self._ROOTObjects = None
        self._ROOTObjectsSet = False

    def passHistos(self, ListOfHistos, normalize = False, appendHisto = False):
        if not appendHisto:
            self.histos = ListOfHistos
        else:
            if not isinstance(self.histos, list):
                self.histos =[ListOfHistos]
            else:
                self.histos.append(ListOfHistos)
        if normalize:
            self.yTitle = "Normalized Units"
            for h in self.histos:
                try:
                    1/h.Integral()
                except ZeroDivisionError:
                    logging.error("Zero Division in scaling")
                else:
                    h.Scale(1/h.Integral())

    def addLabel(self, labelText, xStart = 0.5, yStart = 0.92, scaleText=1):
        thisLabel = ROOT.TLatex(xStart , yStart , labelText)
        thisLabel.SetTextFont(42)
        thisLabel.SetTextSize(0.045*scaleText)
        thisLabel.SetNDC()
        if self.labels is None:
            self.labels = []
        self.labels.append(thisLabel)
        return True

    def addCMSLabel(self, text="Prelim"):
        if text not in ["Prelim", "Sim"]:
            logging.error("Unknown cms text requestion. Will not add label")
            return False
        if text == "Sim":
            labelText = '#scale['+str((1)+0.2)+']{#bf{CMS}} #scale['+str((1))+']{#it{simulation}}'
        if text == "Prelim":
            labelText = '#scale['+str((1)+0.2)+']{#bf{CMS}} #scale['+str((1))+']{#it{Preliminary}}'
        cms = ROOT.TLatex( 0.15 , 0.92 , labelText)
        cms.SetTextFont(42)
        cms.SetTextSize(0.045)
        cms.SetNDC()
        if self.labels is None:
            self.labels = []
        self.labels.append(cms)
        return True


    def drawPlot(self, legendText, xTitle = None, isDataMCwStack = False, stacksum = None, addHisto = None, plotTitle = None, noErr = False, errorband = None, drawCMS =False, histolegend = None, logscale = False, drawPulls = False, saveCanvas = False, floatingMax = 1.15 ):
        maximimus = []
        minimums = []
        for h in self.histos:
            maximimus.append(h.GetMaximum())

        leg = ROOT.TLegend(self.legendSize[0], self.legendSize[1], self.legendSize[2], self.legendSize[3])
        logging.debug("Setting NColumns to %s",self.legColumns)
        leg.SetNColumns(self.legColumns)
        leg.SetFillStyle(0)
        leg.SetBorderSize(0)
        leg.SetTextSize(0.05)

        if drawPulls:
            self.ratioText = "Pulls"
        
        if self.useDefaultColors:
            if len(self.defaultColors) < len(self.histos):
                raise RuntimeError("More histograms set than default color. Please disable useDefaultColors and set colors in your script")
            for ihisto, histo in enumerate(self.histos):
                histo.SetLineColor(self.defaultColors[ihisto])
                histo.SetFillStyle(0)
        ################################################
        # Making main plot
        self.canvas.cd(1)
        if logscale and isDataMCwStack:
            self.histos[1].Draw()
            self.histos[1].SetMinimum(1)
            self.canvas.cd(1).SetLogy()
             
        for ihisto, histo in enumerate(self.histos):
            if ihisto == 0:
                if isDataMCwStack:
                    histo.Draw("P")
                else:
                    if noErr:
                        histo.Draw("histo")
                    else:
                        histo.Draw("histoe")
                if logscale:
                    histo.SetMaximum(max(maximimus)*1000)
                else:
                    histo.SetMaximum(max(maximimus)*floatingMax)
                if logscale:
                    histo.SetMinimum(2)
                else:
                    histo.SetMinimum(0)
                if plotTitle is None:
                    histo.SetTitle("")
                else:
                    histo.SetTitle(plotTitle)
                    self.canvas.cd(1).SetTopMargin(0.1)
                histo.GetYaxis().SetTitle(self.yTitle)
                histo.GetXaxis().SetTitleSize(0)
                histo.GetXaxis().SetLabelSize(0)
                histo.GetYaxis().SetTitleOffset(self.yTitleOffset)
                histo.GetYaxis().SetTitleSize(0.06)
            else:
                add = ""
                #if isDataMCwStack:
                #    add = "axis"
                if noErr:
                    histo.Draw("histosame"+add)
                else:
                    histo.Draw("histoesame"+add)
                    
            if legendText is not None:
                leg.AddEntry(histo, legendText[ihisto], "PLE")
                logging.debug("Adding %s to legend for %s",legendText[ihisto], histo)
        if addHisto is not None:
            if not isinstance(addHisto, list):
                logging.error("addPlot variables is required to be of type list. Skipping add Plots")
            else:
                logging.debug("Additional histograms: %s",addHisto)
                for aHisto in addHisto:
                    if isinstance(aHisto, tuple):
                        h, drawStyle = aHisto
                        logging.debug("Drawing additional object %s with style %s", h, drawStyle)
                        h.Draw(drawStyle+"same")
                    else:
                        aHisto.Draw("histoesame")
        if errorband is not None:
            #for ibin in range(errorband.GetNbinsX()+2):
            #    print errorband.GetBinContent(ibin), errorband.GetBinError(ibin)
            errorband.Draw("E2same")

        if isDataMCwStack:
            leg.AddEntry(self.histos[0], "Data", "PLE")
            
        if histolegend is not None:
            for legendTuple in histolegend:
                if len(legendTuple) == 2:
                    histo, text = legendTuple
                    style = "F"
                if len(legendTuple) == 3:
                    histo, text, style = legendTuple
                leg.AddEntry(histo, text, style)
        if isDataMCwStack:
            self.histos[0].Draw("Psame")
            self.histos[0].Draw("sameaxis")
        leg.Draw("same")
        if self.labels is not None:
            for label in self.labels:
                label.Draw("same")
        ################################################
        # Making ratio plot
        self.canvas.cd(2)
        if stacksum is not None:
            ratios = self._makeAllRatios(stacksum, drawPulls)
        else:
            ratios = self._makeAllRatios(drawPulls = drawPulls )
        assert len(ratios) == len(self.histos)
        for iratio, ratio in enumerate(ratios):
            if iratio == 0:
                ratio.SetTitle("")
                ratio.GetXaxis().SetTitleOffset(0.8*self.xTitleOffsetScale)
                ratio.GetXaxis().SetTitleSize(0.125 )
                ratio.GetXaxis().SetLabelSize(ratio.GetYaxis().GetLabelSize()*self.labelScaleX)
                ratio.GetYaxis().SetTitleOffset(self.yTitleOffsetRatio)
                ratio.GetYaxis().SetTitleSize(0.1)
                ratio.GetYaxis().CenterTitle()
                if xTitle is not None:
                    ratio.GetXaxis().SetTitle(xTitle)
                ratio.GetYaxis().SetTitle(self.ratioText)
                if errorband is not None and stacksum is not None:
                    ratioErrorband = _getRatioErrorBand(stacksum, ratio)
                    if not drawPulls:
                        ratioErrorband.Draw("E2")
                    ratio.Draw("histoesame")
                elif errorband is not None and stacksum is None:
                    ratioErrorband = _getRatioErrorBand(self.histos[0], ratio)
                    if not drawPulls:
                        ratioErrorband.Draw("E2")
                    ratio.Draw("histoesame")
                else:
                    if noErr:
                        ratio.Draw("histo")
                    else:
                        ratio.Draw("histoe")
            else:
                if noErr:
                    for i in range(ratio.GetN()):
                        ratio.SetPointEYlow(i,0)
                        ratio.SetPointEYhigh(i,0)
                    ratio.Draw("sameP")
                else:
                    
                    ratio.Draw("sameP")
                    #ROOT.gStyle.SetErrorX(1)

        if drawCMS:
            scalelumi, scaleCMS = self.CMSscale
            PlotHelpers.drawCMSLabel("Preliminary", self.canvas.cd(1).GetTopMargin(),  self.canvas.cd(1).GetRightMargin(), scalelumi, scaleCMS, self.moveCMS, self.moveCMSTop)

        self._ROOTObjects = [
            ratio,
        ]
        if errorband is not None and stacksum is not None:
            self._ROOTObjects.append(ratioErrorband)
        if stacksum is not None:
            self._ROOTObjects.append(stacksum)

        self._ROOTObjectsSet = True
            
        return deepcopy(self.canvas)
                
    def _makeAllRatios(self, stacksum = None, drawPulls = False):
        ListOfRatios = []
        for ihisto, histo in enumerate(self.histos):
            logging.info("Make ratio for %s", histo.GetName())
            if ihisto == 0:
                logging.debug("Creating ratio line")
                if drawPulls:
                    ListOfRatios.append(self._makeRatioLine(histo, forPulls = True))
                else:
                    ListOfRatios.append(self._makeRatioLine(histo))
            else:
                logging.debug("Creation ratio")
                if stacksum is not None:
                    logging.info("Using stacksum instead of {0} histogram {1}".format(ihisto, histo))
                    if drawPulls:
                        ListOfRatios.append(self._makeRatio(stacksum, self.histos[0], forPulls = True)[0])
                    else:
                        ListOfRatios.append(self._makeRatio(stacksum, self.histos[0])[0])
                else:
                    ListOfRatios.append(self._makeRatio(histo, self.histos[0], forPulls = drawPulls)[0])
        return ListOfRatios

    def _makeRatioLine(self, histo, forPulls = False):
        l = histo.Clone()
        l.SetName("ratioline_"+l.GetName())
        l.SetTitle("")
        l.Divide(histo)
        l.SetLineColor(histo.GetLineColor())
        l.SetLineStyle(2)
        l.SetLineWidth(1)
        l.SetFillStyle(0)
        lowerbound, upperbound = self.ratioRange
        l.GetYaxis().SetRangeUser(lowerbound, upperbound)
        l.GetYaxis().SetTitle(self.ratioText)
        l.GetYaxis().SetTitleOffset(1.1)
        l.GetXaxis().SetTitleOffset(0.9)
        l.GetXaxis().SetLabelSize(histo.GetXaxis().GetLabelSize()*(1/0.4))
        l.GetYaxis().SetLabelSize(histo.GetYaxis().GetLabelSize()*(1/0.4))
        l.GetXaxis().SetTitleSize(histo.GetXaxis().GetTitleSize()*(1/0.4))
        l.GetYaxis().SetTitleSize(histo.GetYaxis().GetTitleSize()*(1/0.4))
        l.GetYaxis().SetNdivisions(505)
        l.GetXaxis().SetNdivisions(510)
        #print l.GetXaxis().GetLabelSize()
        for i in range(l.GetNbinsX()+1):
            if not forPulls:
                l.SetBinContent(i,1)
            else:
                l.SetBinContent(i,0)
            l.SetBinError(i,0)
        logging.debug("Ratio line generated: "+str(l))
        return deepcopy(l)

    def _makeRatio(self, h, href, forPulls = False):
        logging.debug("Making ratio for ratio plot from "+str(h))
        grref = ROOT.TGraphAsymmErrors(href)
        ratio = grref.Clone("ratio_"+h.GetName())
        ratio.SetMarkerColor(h.GetLineColor())
        ratio.SetLineColor(h.GetLineColor())
        x, y = ROOT.Double(0), ROOT.Double(0)
        mindiv = 9999.
        maxdiv = -9999.
        for i in range(0,grref.GetN()):
            grref.GetPoint(i, x, y)
            maxErros = max(grref.GetErrorYlow(i), grref.GetErrorYhigh(i))
            currentBin = h.FindBin(x)
            currentBinContent = h.GetBinContent(currentBin)
            currentBinError = h.GetBinError(currentBin)
            if currentBinContent > 0:
                if not forPulls:
                    if y != 0:
                        ratioval = currentBinContent/y if not self.invertRatio else y/currentBinContent
                    else:
                        ratioval = 0
                else:
                    if maxErros+currentBinError >= 0:
                        if not self.invertRatio:
                            ratioval = (currentBinContent-y)/(maxErros**2+currentBinError**2)**0.5
                        else:
                            ratioval = (y-currentBinContent)/(maxErros**2+currentBinError**2)**0.5
                        logging.debug("Pulls - %s | nh %s | eh %s | nRef %s | eRef %s | Pull %s", i, currentBinContent, currentBinError, y, maxErros,ratioval )
                    else:
                        ratioval = 0
                ratio.SetPoint(i, x, ratioval)
                if ratioval > maxdiv and ratioval > 0:
                    maxdiv = round(ratioval, 1)
                if ratioval < mindiv and ratioval > 0:
                    mindiv = round(ratioval, 1)
            else:
                ratio.SetPoint(i, x, -999)

            if y > 0 and not forPulls:
                if currentBinContent > 0:
                    ratio.SetPointEYlow(i, grref.GetErrorYlow(i)/currentBinContent)
                    ratio.SetPointEYhigh(i, grref.GetErrorYhigh(i)/currentBinContent)
                else:
                    ratio.SetPointEYlow(i, 1-(y-grref.GetErrorYlow(i))/y)
                    ratio.SetPointEYhigh(i, (y+grref.GetErrorYhigh(i))/y-1)
            elif forPulls:
                ratio.SetPointEYlow(i, 1)
                ratio.SetPointEYhigh(i, 1)
            else:
                ratio.SetPointEYlow(i, 0)
                ratio.SetPointEYhigh(i, 0)
            if forPulls:
                ratio.SetPointEXhigh(i, 0)
                ratio.SetPointEXlow(i, 0)
        return [deepcopy(ratio), mindiv, maxdiv]

    def getROOTObjects(self):
        if self._ROOTObjectsSet:
            return self.histos +  self._ROOTObjects 
        else:
            return None

    @staticmethod
    def formatErrorband(errorband):
        errorband.SetFillColor(ROOT.kBlack)
        errorband.SetLineColor(ROOT.kBlack)
        errorband.SetFillStyle(3645)
        

def _getRatioErrorBand(stacksum, line):
    ratioErrorBand = line.Clone("ratioErrorBand")
    ratioErrorBand.SetFillColor(ROOT.kBlack)
    ratioErrorBand.SetFillStyle(3645)
    ratioErrorBand.SetMarkerSize(0)
    for ibin in range(stacksum.GetNbinsX()+2):
        error = stacksum.GetBinError(ibin)
        content = stacksum.GetBinContent(ibin)
        errorratio = 0
        if content > 0:
            errorratio = error/float(content)

        logging.debug("Bin %s : error %s", ibin, errorratio)
            
        ratioErrorBand.SetBinError(ibin, errorratio)

    return ratioErrorBand


def _getErrorGraph(histo):
    errorGraph = ROOT.TGraphAsymmErrors(histo)
    errorGraph.SetFillStyle(3645)
    errorGraph.SetFillColor(ROOT.kBlack)
    return errorGraph


