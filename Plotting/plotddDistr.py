import ROOT
import copy
from classes.ratios import RatioPlot
from classes.PlotHelpers import moveOverUnderFlow, saveCanvasListAsPDF, makeDistribution
import Helper
#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(funcName)-18s %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.DEBUG,
)
#############################################################
#############################################################
ROOT.gStyle.SetOptStat(0)
ROOT.gROOT.SetBatch(1)

ROOT.gROOT.LoadMacro("functions/HTreweight.h+")
ROOT.gROOT.LoadMacro("functions/triggerWeightRound.h+")
ROOT.gROOT.LoadMacro("CRCorrections/btagCorrections.h+")


baseDir = "root://t3dcachedb.psi.ch//pnfs/psi.ch/cms/trivcat/store/user/koschwei/tth/projectSkim/v4/pre/v2"

files = {
    "data" : baseDir+"/data.root",
    # "QCD1000" : baseDir+"/QCD_HT1000to1500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
    # "QCD1500" : baseDir+"/QCD_HT1500to2000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
    # "QCD2000" : baseDir+"/QCD_HT2000toInf_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
    # "QCD300" : baseDir+"/QCD_HT300to500_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
    # "QCD500" : baseDir+"/QCD_HT500to700_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
    # "QCD700" : baseDir+"/QCD_HT700to1000_TuneCUETP8M1_13TeV-madgraphMLM-pythia8.root",
    "ttSL" : baseDir+"/TTToSemiLeptonic_TuneCP5_PSweights_13TeV-powheg-pythia8.root",
    "ttDL" : baseDir+"/TTTo2L2Nu_TuneCP5_PSweights_13TeV-powheg-pythia8.root",
    "ttFH" : baseDir+"/TTToHadronic_TuneCP5_PSweights_13TeV-powheg-pythia8.root",
#    "ttH" : baseDir+"/ttHTobb_M125_TuneCUETP8M2_ttHtranche3_13TeV-powheg-pythia8.root",
}

lumi = 35.920026
xSecBynGen = {
    "data" :  1,
    "QCD1000" : 1,
    "QCD1500" : 1,
    "QCD2000" : 1,
    "QCD300" : 1,
    "QCD500" : 1,
    "QCD700" : 1,
    "ttSL" : 365.46/107689840.0,
    "ttDL" : 88.34/54129016.0,
    "ttFH" : 377.96/58402380.0,
    "ttH" :  0.29533504/3737487.0,
    }


weight = "puWeight * (sign(genWeight)) * btagWeight_shape *  triggerFHWeight* qgWeight"
cuts = Helper.get_cuts()
categories = {
    "7J3TVR" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["ge7j"], cuts["3bVR"]),
    "8J3TVR" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["8j"], cuts["3bVR"]),
    "9J3TVR" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["9j"], cuts["3bVR"]),
    "7J4TVR" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["ge7j"], cuts["4bVR"]),
    "8J4TVR" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["8j"], cuts["4bVR"]),
    "9J4TVR" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["9j"], cuts["4bVR"]),
    "7J3TCR2" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["ge7j"], cuts["3bCR2"]),
    "8J3TCR2" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["8j"], cuts["3bCR2"]),
    "9J3TCR2" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["9j"], cuts["3bCR2"]),
    "7J4TCR2" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["ge7j"], cuts["4bCR2"]),
    "8J4TCR2" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["8j"], cuts["4bCR2"]),
    "9J4TCR2" : "((HLT_ttH_FH || HLT_BIT_HLT_PFHT1050) && ht30 > 500 && jets_pt[5] > 40 && {0} && {1})".format(cuts["9j"], cuts["4bCR2"]),
    }

hypothesis = {
    "7J3T" : "mem_tth_FH_4w2h1t_p/(mem_tth_FH_4w2h1t_p+0.080*mem_ttbb_FH_4w2h1t_p)",#4W2H1T
    "8J3T" : "mem_tth_FH_4w2h1t_p/(mem_tth_FH_4w2h1t_p+0.080*mem_ttbb_FH_4w2h1t_p)",#4W2H1T
    "9J3T" : "mem_tth_FH_4w2h1t_p/(mem_tth_FH_4w2h1t_p+0.080*mem_ttbb_FH_4w2h1t_p)",#4W2H1T
    "7J4T" : "mem_tth_FH_3w2h2t_p/(mem_tth_FH_3w2h2t_p+0.065*mem_ttbb_FH_3w2h2t_p)",#3W2H2T 
    "8J4T" : "mem_tth_FH_3w2h2t_p/(mem_tth_FH_3w2h2t_p+0.065*mem_ttbb_FH_3w2h2t_p)",#3W2H2T
    "9J4T" : "mem_tth_FH_4w2h2t_p/(mem_tth_FH_4w2h2t_p+0.065*mem_ttbb_FH_4w2h2t_p)",#4W2H2T
}


HTWeight = {
    "7J3T" : "1",#"HTrewt3b(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5])",
    "8J3T" : "1",#"HTrewt3b(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5])",
    "9J3T" : "1",#"HTrewt3b(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5])",
    "7J4T" : "1",#"HTrewt4b(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5])",
    "8J4T" : "1",#"HTrewt4b(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5])",
    "9J4T" : "1",#"HTrewt4b(jets_pt[0]+jets_pt[1]+jets_pt[2]+jets_pt[3]+jets_pt[4]+jets_pt[5])",
    }

baseHistos = [
    ( ROOT.TH1F("hHT", "hHT",16, 400, 2000), "ht30" ),
    #( ROOT.TH1F("hMEM", "hMEM",10, 0, 1), "MEM" )
]

trees = {}
rfiles = {}
for file_ in files:
    logging.info("Adding file %s",file_)
    rfiles[file_] = ROOT.TFile.Open(files[file_])
    trees[file_] = rfiles[file_].Get("tree")

bCorrData = "1"#"exp(Sum$(log(btagCorrData(jets_pt,jets_eta,jets_dRmin,jets_btagCSV))*jets_btagFlag))"
bCorrMC = "1"#"exp(Sum$(log(btagCorrMC(jets_pt,jets_eta,jets_dRmin,jets_btagCSV))*jets_btagFlag))"
#
gqNorm = Helper.get_QGNormaliziation()
ttConv = {"ttSL" : "TTbar_sl", "ttDL" : "TTbar_dl", "ttFH" : "TTbar_fh"}
for cat in hypothesis:
    finalOutput = []
    logging.info("PLotting cat: %s", cat)

    if not "7J" in cat:
        continue
    for baseHisto, Variable in baseHistos:
        rawVar = Variable
        stack = ROOT.THStack("bg"+cat+Variable,"")
        if Variable == "MEM":
            Variable = hypothesis[cat]
        logging.info("Precesisng plot %s", Variable)
        hData = baseHisto.Clone(baseHisto.GetName()+"_data_"+cat)
        hData.SetMarkerColor(ROOT.kBlack)
        hData.SetMarkerStyle(21)
        #hData.SetMarkerSize(1)
        httbar = baseHisto.Clone(baseHisto.GetName()+"_ttbar_"+cat)
        httbar.SetLineColor(ROOT.kBlack)
        httbar.SetFillColor(ROOT.kRed)
        httbar.SetFillStyle(1001)

        httbar1 = baseHisto.Clone(baseHisto.GetName()+"_ttbar1_"+cat)
        httbar2 = baseHisto.Clone(baseHisto.GetName()+"_ttbar2_"+cat)
        httbar3 = baseHisto.Clone(baseHisto.GetName()+"_ttbar3_"+cat)
        
        hddQCD = baseHisto.Clone(baseHisto.GetName()+"_ddQCD_"+cat)
        hddQCD.SetLineColor(ROOT.kBlack)
        hddQCD.SetFillColor(ROOT.kGreen)
        hddQCD.SetFillStyle(1001)
        hddtmp1 = baseHisto.Clone(baseHisto.GetName()+"_ddtmp1_"+cat)
        hddtmp2 = baseHisto.Clone(baseHisto.GetName()+"_ddtmp2_"+cat)
        hddtmp3 = baseHisto.Clone(baseHisto.GetName()+"_ddtmp3_"+cat)

        logging.debug("Selection: %s",categories["{0}VR".format(cat)])

        print "Data sel:",categories["{0}VR".format(cat)]
        trees["data"].Project(hData.GetName(), Variable, "({0}) * ({1})".format(categories["{0}VR".format(cat)], "1"))
        #logging.debug("tt xsec weight: %s", lumi*1000*xSecBynGen["tt"])

        if "7J" in cat:
            j = "7j"
        elif "8J" in cat:
            j = "8j"
        else:
            j = "9j"

        if "3T" in cat:
            b = "3t"
        else:
            b = "4t"
            
        trees["ttSL"].Project(httbar1.GetName(), Variable, "({0}) * ({1} * {2} * {3})".format(categories["{0}VR".format(cat)], str(gqNorm["TTbar_sl"][j][b]["VR"]*lumi*1000*xSecBynGen["ttSL"]), weight, HTWeight[cat]))
        trees["ttDL"].Project(httbar2.GetName(), Variable, "({0}) * ({1} * {2} * {3})".format(categories["{0}VR".format(cat)], str(gqNorm["TTbar_dl"][j][b]["VR"]*lumi*1000*xSecBynGen["ttDL"]), weight, HTWeight[cat]))
        trees["ttFH"].Project(httbar2.GetName(), Variable, "({0}) * ({1} * {2} * {3})".format(categories["{0}VR".format(cat)], str(gqNorm["TTbar_fh"][j][b]["VR"]*lumi*1000*xSecBynGen["ttFH"]), weight, HTWeight[cat]))
        httbar.Add(httbar1)
        httbar.Add(httbar2)
        httbar.Add(httbar3)
        print "httbar",httbar.Integral()
        print "hData",hData.Integral()
        
        trees["data"].Project(hddQCD.GetName(), Variable, "({0}) * ({1})".format(categories["{0}CR2".format(cat)], bCorrData))
        trees["ttSL"].Project(hddtmp1.GetName(), Variable, "({0}) * ({1} * {2} * {3} * {4})".format(categories["{0}CR2".format(cat)], str(gqNorm["TTbar_sl"][j][b]["CR2"]*lumi*1000*xSecBynGen["ttSL"]), weight, bCorrMC, HTWeight[cat]))
        trees["ttDL"].Project(hddtmp2.GetName(), Variable, "({0}) * ({1} * {2} * {3} * {4})".format(categories["{0}CR2".format(cat)], str(gqNorm["TTbar_dl"][j][b]["CR2"]*lumi*1000*xSecBynGen["ttDL"]), weight, bCorrMC, HTWeight[cat]))
        trees["ttFH"].Project(hddtmp3.GetName(), Variable, "({0}) * ({1} * {2} * {3} * {4})".format(categories["{0}CR2".format(cat)], str(gqNorm["TTbar_fh"][j][b]["CR2"]*lumi*1000*xSecBynGen["ttFH"]), weight, bCorrMC, HTWeight[cat]))
        hddQCD.Add(hddtmp1, -1)
        hddQCD.Add(hddtmp2, -1)
        hddQCD.Add(hddtmp3, -1)
        
        hddQCD.Scale( (hData.Integral()-httbar.Integral()) / hddQCD.Integral())

        hSum = httbar.Clone("bkgSum")
        hSum.Add(hddQCD)

        logging.debug("Yields: ttbar %s | ddQCD %s | data %s", httbar.Integral(), hddQCD.Integral(), hData.Integral())
        stack.Add(hddQCD)
        stack.Add(httbar)
        
        
        output = RatioPlot(name = str(cat))
        output.passHistos([hData, stack])
        finalOutput.append(
            output.drawPlot(None, rawVar, isDataMCwStack = True, stacksum=hSum)
        )
    
    saveCanvasListAsPDF(finalOutput, "VRdd_QGLRDef_distribution_"+cat, ".")
    
"""

for cat in hypothesis:
    finalOutput = []
    logging.info("Chape comparisons")
    logging.info("Plotting cat: %s", cat)
    for baseHisto, Variable in baseHistos:
        rawVar = Variable
        stack = ROOT.THStack("bg"+cat+Variable,"")
        if Variable == "MEM":
            Variable = hypothesis[cat]
        logging.info("Precesisng plot %s", Variable)

        httbar = baseHisto.Clone(baseHisto.GetName()+"_shape_ttbar_"+cat)
        httbar.SetLineColor(ROOT.kRed)

        httH = baseHisto.Clone(baseHisto.GetName()+"_shape_ttH_"+cat)
        httH.SetLineColor(ROOT.kBlue)

        hData = baseHisto.Clone(baseHisto.GetName()+"_shape_data_"+cat)
        hData.SetLineColor(ROOT.kBlack)

        trees["data"].Project(hData.GetName(), Variable, "({0}) * ({1})".format(categories["{0}SR".format(cat)], "1"))
        logging.debug("tt xsec weight: %s", lumi*1000*xSecBynGen["tt"])
        nSeltt = trees["tt"].Project(httbar.GetName(), Variable, "({0}) * ({1} * {2} * {3})".format(categories["{0}SR".format(cat)], str(Helper.qgWtFac["TTbar_inc"]*lumi*1000*xSecBynGen["tt"]), weight, HTWeight[cat]))
        nSelttH = trees["ttH"].Project(httH.GetName(), Variable, "({0}) * ({1} * {2} * {3})".format(categories["{0}SR".format(cat)], str(Helper.qgWtFac["ttH_hbb"]*lumi*100000*xSecBynGen["ttH"]), weight, HTWeight[cat]))

        output = RatioPlot(name = "Shape_"+str(cat))
        output.legendSize = (0.2,0.6,0.4,0.8)
        output.passHistos([httH, hData, httbar], True)
        finalOutput.append(
            output.drawPlot(["Data", "ttbar", "ttHbb"],rawVar)
        )

        saveCanvasListAsPDF(finalOutput, "SRCWoLa_QGLRDef_distribution_shape"+cat, ".")
D
"""
