#!/bin/bash
set -e
source common.sh

#go to work directory
cd $GC_SCRATCH

python ${CMSSW_BASE}/src/TTH/FHDownstream/gc/scripts/reCalcCount2.py out.root $FILE_NAMES
