#!/bin/bash
source common.sh
cd $GC_SCRATCH
export ANALYSIS_CONFIG=${CMSSW_BASE}/src/TTH/MEAnalysis/data/config_FH_VR.cfg
python ${CMSSW_BASE}/src/TTH/Plotting/python/joosep/sparsinator.py
