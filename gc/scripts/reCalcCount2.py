#!/usr/bin/env python
"""Recalculates the number fo weighted gen events from the nanoAOD tree"""
import ROOT
import sys
from TTH.MEAnalysis.samples_base import getSitePrefix
import logging
import os
import math

LOG_MODULE_NAME = logging.getLogger(__name__)

def main(filenames, ofname, debug=False):
    """
    Get the necessary weight from the nanoAOD file and calulates the the number of genevents with
    Sum of 1 * puWeight * sign(genWeight)
    """
    filenames_pref = map(getSitePrefix, filenames)
    if os.path.isfile(ofname):
        LOG_MODULE_NAME.info("opening existing file {0}".format(ofname))
        of = ROOT.TFile(ofname, "UPDATE")
    else:
        LOG_MODULE_NAME.info("creating new file {0}".format(ofname))
        of = ROOT.TFile(ofname, "RECREATE")
    tree_list = ROOT.TList()

    #keep tfiles open for the duration of the merge
    #tfiles = []
    
    hGen = ROOT.TH1F("hGen","hGen",1,1,2)
    hGenWeighted = ROOT.TH1F("hGenWeighted","hGenWeighted",1,1,2)
    hGenWeightednoPU = ROOT.TH1F("hGenWeightednoPU","hGenWeightednoPU",1,1,2)
    hGenWeightedonlyPU = ROOT.TH1F("hGenWeightedonlyPU","hGenWeightedonlyPU",1,1,2)
    fprocessed = 0
    for infn, lfn in zip(filenames_pref, filenames):
        LOG_MODULE_NAME.info("processing {0}".format(infn))
        tf = ROOT.TFile.Open(infn)
        #tfiles += [tf]

        #run_tree = tf.Get("Runs")
        #if not run_tree:
        #    run_tree = tf.Get("nanoAOD/Runs")

        evt_tree = tf.Get("Events")
        if not evt_tree:
            evt_tree = tf.Get("nanoAOD/Events")
            
        thishGen = hGen.Clone(hGen.GetName()+"_"+str(fprocessed))
        thishGenWeighted = hGenWeighted.Clone(hGenWeighted.GetName()+"_"+str(fprocessed))
        thishGenWeightednoPU = hGenWeightednoPU.Clone(hGenWeightednoPU.GetName()+"_"+str(fprocessed))
        thishGenWeightedonlyPU = hGenWeightedonlyPU.Clone(hGenWeightedonlyPU.GetName()+"_"+str(fprocessed))
        
        evt_tree.Project("hGen"+"_"+str(fprocessed),"1","1")
        evt_tree.Project("hGenWeighted"+"_"+str(fprocessed),"1","puWeight * sign(genWeight)")
        evt_tree.Project("hGenWeightednoPU"+"_"+str(fprocessed),"1","sign(genWeight)")
        evt_tree.Project("hGenWeightedonlyPU"+"_"+str(fprocessed),"1","puWeight")

        hGen.Add(thishGen)
        hGenWeighted.Add(thishGenWeighted)
        hGenWeightednoPU.Add(thishGenWeightednoPU)
        hGenWeightedonlyPU.Add(thishGenWeightedonlyPU)
        
        tf.Close()
        fprocessed += 1
    of.cd()
    #out_tree = ROOT.TTree.MergeTrees(tree_list)    
    LOG_MODULE_NAME.info("saving output")
    #out_tree.Write()
    hGen.Write()
    hGenWeighted.Write()
    hGenWeightednoPU.Write()
    hGenWeightedonlyPU.Write()
    if debug:
        LOG_MODULE_NAME.debug("Recalculated Values:")
        LOG_MODULE_NAME.debug("hGen : {0}".format(hGen.GetBinContent(1)))
        LOG_MODULE_NAME.debug("hGenWeighted : {0}".format(hGenWeighted.GetBinContent(1)))
        LOG_MODULE_NAME.debug("hGenWeightednoPU : {0}".format(hGenWeightednoPU.GetBinContent(1)))
        LOG_MODULE_NAME.debug("hGenWeightedonlyPU : {0}".format(hGenWeightedonlyPU.GetBinContent(1)))
        LOG_MODULE_NAME.debug("nanoAOD Values:")
        #countw = 0
        #countgen = 0
        #for ient in range(out_tree.GetEntries()):
        #    out_tree.GetEntry(ient)
        #    countw += out_tree.genEventSumw
        #    countgen += out_tree.genEventCount
        #LOG_MODULE_NAME.debug("genEventCount : {0}".format(countgen))
        #LOG_MODULE_NAME.debug("genEventCount : {0}".format(countw))
    
    
    of.Close()
    return ofname

if __name__ == "__main__":
    loglevel = 20
    logging.basicConfig(stream=sys.stdout, level=loglevel)
    filenames = sys.argv[2:]
    ofname = sys.argv[1]
    logging.info("Files to process: {0}".format(len(filenames)))
    logging.info("Outputfilename: "+ofname)
    debug = False
    if loglevel < 20:
        debug = True
    
    print main(filenames, ofname, debug)
