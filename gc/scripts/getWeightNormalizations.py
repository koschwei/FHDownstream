#!/usr/bin/env python
""" Script for getting normalization factors for b-tag reweighting  """
import sys
import logging
import os
import math
from copy import copy
from time import sleep
LOG_MODULE_NAME = logging.getLogger(__name__)

def btagNoramlization(filenames, ofname, debug=False):
    """
    Get Yield with and w/o b-tag reweighting from nanoAOD --> No selection applied 
    """
    import ROOT
    ROOT.TH1.SetDefaultSumw2(True)
    
    ROOT.gROOT.LoadMacro("/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p1/CMSSW_9_4_9/src/TTH/FHDownstream/gc/scripts/classes.h")
    ROOT.gInterpreter.ProcessLine('bTagSF* internalbTagSF = new bTagSF("/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p3/CMSSW_9_4_9/src/TTH/MEAnalysis/data/deepCSV_sfs_JECV32_v2.csv")')
    ROOT.gROOT.LoadMacro("/mnt/t3nfs01/data01/shome/koschwei/tth/2017Data/v3p1/CMSSW_9_4_9/src/TTH/FHDownstream/gc/scripts/functions.h") #inlcude functions that can be used in TTree::Draw


    
    from TTH.MEAnalysis.samples_base import getSitePrefix
    filenames_pref = map(getSitePrefix, filenames)
    if os.path.isfile(ofname):
        LOG_MODULE_NAME.info("opening existing file {0}".format(ofname))
        of = ROOT.TFile(ofname, "UPDATE")
    else:
        LOG_MODULE_NAME.info("creating new file {0}".format(ofname))
        of = ROOT.TFile(ofname, "RECREATE")
    tree_list = ROOT.TList()

    weightBase = "btagSF_shape"
    weightBaseFW = "btagWeight_shape"
    syst = [
        "central",
        "lf", "hf", "hfstats1", "hfstats2", "lfstats1", "lfstats2", "cferr1", "cferr2",
        "jesAbsoluteMPFBias",
        "jesAbsoluteScale",
        "jesAbsoluteStat",
        "jesFlavorQCD",
        "jesFragmentation",
        "jesPileUpDataMC",
        "jesPileUpPtBB",
        "jesPileUpPtEC1",
        "jesPileUpPtEC2",
        "jesPileUpPtHF",
        "jesPileUpPtRef",
        "jesRelativeBal",
        "jesRelativeFSR",
        "jesRelativeJEREC1",
        "jesRelativeJEREC2",
        "jesRelativeJERHF",
        "jesRelativePtBB",
        "jesRelativePtEC1",
        "jesRelativePtEC2",
        "jesRelativePtHF",
        "jesRelativeStatFSR",
        "jesRelativeStatEC",
        "jesRelativeStatHF",
        "jesSinglePionECAL",
        "jesSinglePionHCAL",
        "jesTimePtEta",
    ]
    IDmap = {
        "central" : 35,
        "lf" : 1,
        "hf" : 2,
        "hfstats1" : 3,
        "hfstats2" : 4,
        "lfstats1" : 5,
        "lfstats2" : 6,
        "cferr1" : 7,
        "cferr2" : 8,
        "jesAbsoluteMPFBias" : 9,
        "jesAbsoluteScale" : 10,
        "jesAbsoluteStat" : 11,
        "jesFlavorQCD" : 12,
        "jesFragmentation" : 13,
        "jesPileUpDataMC" : 14,
        "jesPileUpPtBB" : 15,
        "jesPileUpPtEC1" : 16,
        "jesPileUpPtEC2" : 17,
        "jesPileUpPtHF" : 18,
        "jesPileUpPtRef" : 19,
        "jesRelativeBal" : 20,
        "jesRelativeFSR" : 21,
        "jesRelativeJEREC1" : 22,
        "jesRelativeJEREC2" : 23,
        "jesRelativeJERHF" : 24,
        "jesRelativePtBB" : 25,
        "jesRelativePtEC1" : 26,
        "jesRelativePtEC2" : 27,
        "jesRelativePtHF" : 28,
        "jesRelativeStatFSR" : 29,
        "jesRelativeStatEC" : 30,
        "jesRelativeStatHF" : 31,
        "jesSinglePionECAL" : 32,
        "jesSinglePionHCAL" : 33,
        "jesTimePtEta" : 34,
        }

    IDMapDir = {
        "up" : 1,
        "down" : 2,
        "nom" : 0,
    }
    leadingUpper = lambda x: x[0].upper()+x[1:]
    
    weights = []
    hnames = {}
    weight4Draw = {}
    for sys in syst:
        if sys == "central":
            dirs = ["nom"]
        else:
            dirs = ["up", "down"]
        for i in dirs:
            weight = "{0}_{1}".format(i, sys)
            weight4Draw[weight] = "*exp(Sum$(log(get_bTagWeight({0}, {1}, Jet_pt, Jet_eta, Jet_btagDeepB, Jet_hadronFlavour)) * (Jet_pt > 30 && abs(Jet_eta) < 2.4)))".format(IDmap[sys], IDMapDir[i])
            weights.append(weight)
            hnames[weight] = "{0}_{1}".format(i, sys)
            #print weight4Draw[weight]
    EventsWeights = {}
    for weight in weights:
        EventsWeights[weight] = 1
    emptyEventWeights = copy(EventsWeights)
    
    hNum = ROOT.TH1F("hNum", "hNum", 13, 0 ,13)
    hBaseWeight = ROOT.TH1F("hBaseWeight","hBaseWeight",40,0,2)
    hDenom = {}
    hWeight = {}
    hWeightOnlyB = {}
    for weight in weights:
        hDenom[weight] = ROOT.TH1F("hDenom_"+hnames[weight], "hDenom"+hnames[weight], 13, 0 ,13)
        hWeight[weight] = ROOT.TH1F("hWeight_"+hnames[weight],"hWeight_"+hnames[weight],40,0,2)
        hWeightOnlyB[weight] = ROOT.TH1F("hWeightOnlyB_"+hnames[weight],"hWeightOnlyB_"+hnames[weight],40,0,2)
    iFile = 0
    for infn, lfn in zip(filenames_pref, filenames):
        LOG_MODULE_NAME.info("processing {0}".format(infn))
        tf = ROOT.TFile.Open(infn)
        #tfiles += [tf]

        #run_tree = tf.Get("Runs")
        #if not run_tree:
        #    run_tree = tf.Get("nanoAOD/Runs")

        evt_tree = tf.Get("Events")
        if not evt_tree:
            evt_tree = tf.Get("nanoAOD/Events")

        logging.debug("Creating temp histos for file %s", iFile)
        htmp = hNum.Clone(hNum.GetName()+"_"+str(iFile))
        htmpWeight = hBaseWeight.Clone(hBaseWeight.GetName()+"_"+str(iFile))
        htmpDenom = {}
        htmpWeightsDenom = {}
        htmpWeightsOnlyBDenom = {}
        for weight in weights:
            htmpDenom[weight] = hDenom[weight].Clone(hDenom[weight].GetName()+"_"+str(iFile))
            htmpWeightsDenom[weight] = hWeight[weight].Clone(hWeight[weight].GetName()+"_"+str(iFile))
            htmpWeightsOnlyBDenom[weight] = hWeightOnlyB[weight].Clone(hWeightOnlyB[weight].GetName()+"_"+str(iFile))
        #Fill histos for this file
        logging.debug("Projection unweighted histos")
        evt_tree.Project(htmp.GetName(), "Sum$(Jet_pt > 30 && abs(Jet_eta) < 2.4)", "sign(genWeight) * puWeight")
        evt_tree.Project(htmpWeight.GetName(), "sign(genWeight) * puWeight")
        for weight in weights:
            logging.debug("Starting with %s", weight)
            #sleep(10)
            evt_tree.Project(htmpDenom[weight].GetName(), "Sum$(Jet_pt > 30 && abs(Jet_eta) < 2.4)", "sign(genWeight) * puWeight"+weight4Draw[weight])
            evt_tree.Project(htmpWeightsDenom[weight].GetName(), "sign(genWeight) * puWeight"+weight4Draw[weight])
            evt_tree.Project(htmpWeightsOnlyBDenom[weight].GetName(), "1"+weight4Draw[weight])

        #Add histo from this file to total histos
        hNum.Add(htmp)
        hBaseWeight.Add(htmpWeight)
        for weight in weights:
            hDenom[weight].Add(htmpDenom[weight])
            hWeight[weight].Add(htmpWeightsDenom[weight])
            hWeightOnlyB[weight].Add(htmpWeightsOnlyBDenom[weight])

        """
        for iEv in range(evt_tree.GetEntries()):
            if iEv%10000 == 0 and iEv != 0:
                 LOG_MODULE_NAME.info("Processed events: {0}".format(iEv))
            evt_tree.GetEvent(iEv)
            LOG_MODULE_NAME.debug("Event %s| LS %s", evt_tree.event, evt_tree.luminosityBlock)
            nJets = 0
            baseEventWeight =  math.copysign(1.0, evt_tree.genWeight) * evt_tree.puWeight
            EventsWeights = copy(emptyEventWeights)
            for iJet in range(evt_tree.nJet):
                if evt_tree.Jet_pt[iJet] > 30 and abs(evt_tree.Jet_eta[iJet]) < 2.4:
                    nJets += 1
                    for weight in weights:
                       EventsWeights[weight] *= evt_tree.__getattr__("Jet_{0}".format(weight))[iJet]
            
            hNum.Fill(nJets, baseEventWeight)
            hBaseWeight.Fill(baseEventWeight)
            LOG_MODULE_NAME.debug("Event %s - Output", iEv)
            LOG_MODULE_NAME.debug("nJets: %s | Baseweight : %s",nJets, baseEventWeight)
            for weight in weights:
                hDenom[weight].Fill(nJets, baseEventWeight * EventsWeights[weight])
                hWeight[weight].Fill(EventsWeights[weight])
                LOG_MODULE_NAME.debug("Weight %s: %s", weight, EventsWeights[weight])
            #raw_input("Next event")
        #tree_list.Add(run_tree)
        """
        tf.Close()
        iFile += 1
    of.cd()        
    #out_tree = ROOT.TTree.MergeTrees(tree_list)    
    LOG_MODULE_NAME.info("saving output")
    
    #out_tree.Write()
    hNum.Write()
    hBaseWeight.Write()
    for weight in weights:
        hDenom[weight].Write()
        hWeight[weight].Write()
        hWeightOnlyB[weight].Write()
    of.Close()
    return ofname

if __name__ == "__main__":
    loglevel = 10
    logging.basicConfig(stream=sys.stdout, level=loglevel)
    filenames = sys.argv[2:]
    ofname = sys.argv[1]
    logging.info("Files to process: {0}".format(len(filenames)))
    logging.info("Outputfilename: "+ofname)
    debug = False
    if loglevel < 20:
        debug = True
    
    print btagNoramlization(filenames, ofname, debug)
