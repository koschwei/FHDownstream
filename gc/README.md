# Grid Control scripts and configs

## Setup
Grid Control is installed on the T3 at PSI to use on the local batch system.

```bash
ln -s /swshare/grid-control/go.py go.py
```
## Run
To run GC with

```bash
./go.py confs/configName.cfg
```

### Important config parameters
in `[global]`:

- `workdir` : Set where to workdirectory of CG is place. This will contain e.g. the error logs 

In `[local]`: 

- `queue` : Use `short.q`/`all.q`/`long.q`

In `[UserTask]`:

- `executable` : Specifies with script is executed. Located in `executables/`
- `files per job` : Number of files in dataset that are used for each job
- `dataset` : List of dataset for the job. Located in `datasets/`

In `[Storage]`:

- `se path` : There are basically two possible ways to output
  - On the SE: `se path = srm://t3se01.psi.ch:8443/srm/managerv2?SFN=/pnfs/psi.ch/cms/trivcat/store/user/$USER/${GC_TASK_ID}/${DATASETPATH}/` 
  - In user fs: `dir://$HOME/tth/gc//${GC_TASK_ID}/${DATASETPATH}/`
  - Feel free to add subfolders :D


## Structure
This folder contains the following objects:

| Folder | Description |
| ------ | ----------- |
|`datasets` | collects list of processed nTuples |
|`confs` | collects configs used for running GC |
|`executables` | collects executables specified in the GC conf |
|`scripts` | collects all scripts that are *only* used for GC jobs |
|`postprocessing` | Scripts used for postprocessing (merging, etc.) of GC output |
