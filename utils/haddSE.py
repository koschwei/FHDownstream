"""haddSE.py

Script for adding root files on the SE  (Default setting for T3_CH_PSI). Will use a folder,
set with the --tmpOut flag, where the file is initially hadded. It is then copied to the SE
with xrootd and deletes the tmp file. The default setting is the T3_CH_PSI scratch folder of
the user.
In default mode, the script will copy the output to the root directory set in the --dir flag.
If a dufferent folder is desired it can be set with the --altOutput flag.
"""
from glob import glob
import sys
import os
import subprocess

#############################################################
############### Configure Logging
import logging
log_format = (
    '[%(asctime)s] %(levelname)-8s %(message)s')
logging.basicConfig(
    format=log_format,
    level=logging.INFO,
)
#############################################################
#############################################################


def runSingleHadd(directory, SEprefix, tmpFile, altOutput, local, outPostfix):
    print altOutput
    if directory.endswith("/"):
        directory = directory[0:-1]
    if altOutput is not None:
        if altOutput.endswith("/"):
            altOutput = altOutput[0:-1]
    logging.info("Running of single directory %s", directory)
    fileList = os.listdir(directory)
    inputfiles = ""
    for file_ in fileList:
        logging.debug("Found file %s", SEprefix+directory+"/"+file_)
        inputfiles += SEprefix+directory+"/"+file_+" "
    haddCommand = "hadd {0}/tmpout{2}.root {1}".format(tmpFile, inputfiles, outPostfix)
    logging.debug("Command: %s", haddCommand)
    logging.info("hadd to output file %s/tmpout%s.root", tmpFile, outPostfix)
    process = subprocess.Popen(haddCommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = process.communicate()
    if error != "":
        print len(error)
        for line in error.split("\n"):
            if line != "":
                logging.error(line)
        logging.info("Exiting......")
        exit()
    else:
        pass
    if not local:
        datasetName = directory.split("/")[-1]
        if altOutput is None:
            logging.info("Transferring tmpout to SE")
            transfercommand = "xrdcp --force {0}/tmpout{3}.root {1}{2}.root".format(tmpFile, SEprefix, directory, outPostfix)
        else:
            logging.info("Transferring tmpout to SE with user set destination")
            transfercommand = "xrdcp --force {0}/tmpout{4}.root {1}{2}/{3}.root".format(tmpFile, SEprefix, altOutput, datasetName, outPostfix)
        logging.debug(transfercommand)
        process = subprocess.Popen(transfercommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = process.communicate()
        # if error != "":
        #     for line in error.split("\n"):
        #         if line != "":
        #             logging.error(line)
        #     logging.info("Exiting......")
        #     exit()
        logging.info("Transfer complete to %s.root", directory)
        process = subprocess.Popen("rm {0}/tmpout{1}.root".format(tmpFile, outPostfix), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        logging.info("Removed tmpfile")
    else:
        logging.info("Local mode! Will rename outputfile")
        datasetName = directory.split("/")[-1]
        renameCommand = "mv {0}/tmpout{2}.root {0}/{1}.root".format(tmpFile, datasetName, outPostfix)
        process = subprocess.Popen(renameCommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if error != "":
            for line in error.split("\n"):
                if line != "":
                    logging.error(line)
            logging.info("Exiting......")
            exit()
        logging.info("Renamed to %s.root",datasetName)

def runSingleHaddwSplit(directory, SEprefix, tmpFile, altOutput, local, splitting, outPostfix):
    print altOutput
    if directory.endswith("/"):
        directory = directory[0:-1]
    if altOutput is not None:
        if altOutput.endswith("/"):
            altOutput = altOutput[0:-1]
    logging.info("Running of single directory %s w/ splitting", directory)
    fileList = os.listdir(directory)
    inputfiles = ""

    lists = [[]]
    nList = 0
    for file_ in fileList:
        if len(lists[nList]) == splitting:
           lists.append([])
           nList += 1
        lists[nList].append(file_)
    logging.info("Split the files into %s parts", len(lists))
    for iList, fileList in enumerate(lists):
        logging.debug("List %s: %s", iList, fileList)
    tempFiles = []
    for iList, fileList in enumerate(lists):
        inputfiles = ""
        for file_ in fileList:
            logging.debug("Found file %s", SEprefix+directory+"/"+file_)
            inputfiles += SEprefix+directory+"/"+file_+" "
        haddCommand = "hadd {0}/tmpout{3}_{2}.root {1}".format(tmpFile, inputfiles, iList, outPostfix)
        tempFiles.append("{0}/tmpout{2}_{1}.root".format(tmpFile, iList, outPostfix))
        logging.debug("Command: %s", haddCommand)
        logging.info("hadd to output file %s/tmpout%s_%s.root", tmpFile, outPostfix, iList)
        process = subprocess.Popen(haddCommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = process.communicate()
        if error != "":
            print len(error)
            for line in error.split("\n"):
                if line != "":
                    logging.error(line)
            logging.info("Exiting......")
            exit()
        else:
            pass
    inputfiles = ""
    for file_ in tempFiles:
        logging.debug("Found file %s", SEprefix+directory+"/"+file_)
        inputfiles += file_+" "
    haddCommand = "hadd {0}/tmpout{2}.root {1}".format(tmpFile, inputfiles, outPostfix)
    logging.debug("Command: %s", haddCommand)
    logging.info("hadd to output file %s/tmpout%s.root", tmpFile, outPostfix)
    process = subprocess.Popen(haddCommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    output, error = process.communicate()
    if error != "":
        print len(error)
        for line in error.split("\n"):
            if line != "":
                logging.error(line)
        logging.info("Exiting......")
        exit()
    else:
        pass
    for file_ in tempFiles:
        process = subprocess.Popen("rm {0}".format(file_), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        logging.info("Removed %s",file_)
    
    if not local:
        datasetName = directory.split("/")[-1]
        if altOutput is None:
            logging.info("Transferring tmpout to SE")
            transfercommand = "xrdcp --force {0}/tmpout{3}.root {1}{2}.root".format(tmpFile, SEprefix, directory, outPostfix)
        else:
            logging.info("Transferring tmpout to SE with user set destination")
            transfercommand = "xrdcp --force {0}/tmpout{4}.root {1}{2}/{3}.root".format(tmpFile, SEprefix, altOutput, datasetName, outPostfix)
        logging.debug(transfercommand)
        process = subprocess.Popen(transfercommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        output, error = process.communicate()
        # if error != "":
        #     for line in error.split("\n"):
        #         if line != "":
        #             logging.error(line)
        #     logging.info("Exiting......")
        #     exit()
        logging.info("Transfer complete to %s.root", directory)
        process = subprocess.Popen("rm {0}/tmpout{1}.root".format(tmpFile, outPostfix), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        logging.info("Removed tmpfile")
    else:
        logging.info("Local mode! Will rename outputfile")
        datasetName = directory.split("/")[-1]
        renameCommand = "mv {0}/tmpout{2}.root {0}/{1}.root".format(tmpFile, datasetName, outPostfix)
        process = subprocess.Popen(renameCommand, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        if error != "":
            for line in error.split("\n"):
                if line != "":
                    logging.error(line)
            logging.info("Exiting......")
            exit()
        logging.info("Renamed to %s.root",datasetName)
        
def runMultHadd(startdirectory, SEprefix, tmpFile, altOutput, local, skip, outPostfix, checkExistance):
    if startdirectory.endswith("/"):
        startdirectory = startdirectory[0:-1]
    dirList = os.listdir(startdirectory)
    for dir_ in dirList:
        skipThis = False
        if skip is not None:
            for name in skip:
                if name in dir_:
                    logging.warning("Skipping %s (matches to %s)", dir_, name)
                    skipThis = True
        if checkExistance:
            altDirFiles = [x.split("/")[-1] for x in glob(altOutput+"/*.root")]
            if dir_+".root" in altDirFiles:
                skipThis = True
                logging.warning("Skipping %s because already in output dir", dir_)
        if not skipThis:
            runSingleHadd(startdirectory+"/"+dir_, SEprefix, tmpFile, altOutput, local, outPostfix)



if __name__ == "__main__":    
    userName = os.environ['USER']
    import argparse
    parser = argparse.ArgumentParser(description='Handling crab jobs')
    parser.add_argument('--type', action="store", required=True, help="Run on single dataset or muliple", choices=["Single","Multi"], type=str)
    parser.add_argument('--dir', action="store", required=True, help="Start directory", type=str)
    parser.add_argument("--prefix", default="root://t3dcachedb.psi.ch/", help="server prefix to add to rootfiles", type=str)
    parser.add_argument("--tmpOut", default="/scratch/{0}".format(userName), help="Output of the tmp file", type=str)
    parser.add_argument("--logging", default = "INFO", help="Set log level", type=str, choices=["INFO","DEBUG"])
    parser.add_argument("--altOutput", default=None, help="Alternative output for the transfer to the SE")
    parser.add_argument("--noSETransfer", action = "store_true", help="If set, the output will be renamed instead of copied to the SE")
    parser.add_argument("--split", action = "store_true", help="If set, the hadd will be made in chunks set with --splitting")
    parser.add_argument("--splitting", default=100, help="Splitting for hadd in chunks")
    parser.add_argument("--skip", default=None,nargs="+", help="Set substrings that will be used to skip datasets")
    parser.add_argument("--tempPostfix", default="", help="Add some string to the tempOut on scratch in case you want to run the script mulitple times simultaneously")
    parser.add_argument("--checkExistance", action = "store_true", help="If set, the script will skip all folders that have a root file in the altOutput")
    args = parser.parse_args()
    logging.getLogger().setLevel(args.logging)
    
    if args.type == "Single":
        if args.split:
            runSingleHaddwSplit(args.dir, args.prefix, args.tmpOut, args.altOutput, args.noSETransfer, args.splitting, args.tempPostfix)
        else:
            runSingleHadd(args.dir, args.prefix, args.tmpOut, args.altOutput, args.noSETransfer, args.tempPostfix)
    else:
        runMultHadd(args.dir, args.prefix, args.tmpOut, args.altOutput, args.noSETransfer, args.skip, args.tempPostfix, args.checkExistance)
