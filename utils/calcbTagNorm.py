import sys, os
import ROOT
from glob import glob
from copy import deepcopy

def calcWeights(path2File, folder):
    print "Processing:",path2File
    rFile = ROOT.TFile(path2File, "READ")
    rFile.cd()
    allDenomHistos = filter(lambda x: x.GetName().startswith("hDenom"), list(rFile.GetListOfKeys()))
    allDenomHistos = [h.GetName() for h in allDenomHistos]
    hNum = rFile.Get("hNum")
    hDenoms = []
    for name in allDenomHistos:
        hDenoms.append(rFile.Get(name))

    hWeight = []
    for hDenom in hDenoms:
       htmp = hNum.Clone("hWeight"+hDenom.GetName()[len("hDenom"):])
       htmp.Divide(hDenom)
       hWeight.append(htmp)

    infileName = path2File.split("/")[-1]
    outfileName = "{0}/bTagNormWeights_{1}".format(folder, infileName) 
    outFile = ROOT.TFile(outfileName, "RECREATE")
    outFile.cd()
    for h in hWeight:
        h.Write()
    outFile.Close()


def getAllWeights(path, folder = "."):
    if path.endswith("/"):
        path = path[:-1]

    print "Using path:",path
    for file_ in sorted(glob(path+"/*.root")):
        calcWeights(file_, folder)

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description="Script for calcuating the btagNormalization for a given set of file from the getWeightNormalizations GC script")
    parser.add_argument("--inputPath", required=True, help="Path to inputfile for this script", type=str)
    parser.add_argument("--folder", default = ".", help=" Folder where the output will be written to")
    args = parser.parse_args()
    if not os.path.exists(args.folder):
        print "Creating folder: {0}".format(args.folder)
        os.makedirs(args.folder)

    
    getAllWeights(args.inputPath, args.folder)
    
