import ROOT
from glob import glob
import sys

markdown = True

folder = sys.argv[1]
if markdown:
    print "| Dataset | nGen (weighted) | nGen |"
    print "|---------|-----------------|------|"
for file_ in sorted(glob(folder+"/*.root")):
    rFile = ROOT.TFile.Open(file_)
    hGenWeight = rFile.Get("hGenWeighted")
    hGen = rFile.Get("hGen")
    nGenW = hGenWeight.GetBinContent(1)
    nGen = hGen.GetBinContent(1)
    if markdown:
        print "|","`",file_.split("/")[-1],"`","|", nGenW,"|", nGen,"|"
    else:
        print file_.split("/")[-1], nGenW, nGen
