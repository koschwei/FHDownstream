#!/bin/bash

root=$1

for folder in $root*; do
    folderName=`basename "$folder"`
    hadd $folder.root $folder/*.root
done
