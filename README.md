# FHDownstream

Collection of downstream scripts and configurations for the tt+X UZH group

## Setup
This repository guild on the Code in [tt+X Framework of the Zurich CMS groups](https://gitlab.cern.ch/Zurich_ttH/tthbb13). First, follow the setup instructions there.
```bash
cd $CMSSW_BASE/src/TTH
git clone ssh://git@gitlab.cern.ch:7999/Zurich_ttH/FHDownstream.git

# Setup script for futher scripts and functionalities
source setup.sh
```

